import sbt.Keys.{baseDirectory, name}

ThisBuild / organization := "noo-programming-language"
ThisBuild / version := "0.1.0"
ThisBuild / scalaVersion := "2.13.10"

lazy val root : Project = (project in file("."))
  .aggregate(`macro`, compiler, test, library)
  .settings(
    name := "Noo Programming Language",
    javaOptions ++= Seq(
      "-Dconsole.encoding=UTF-8",
      "-Dfile.encoding=UTF-8"
    )
  )

lazy val library : Project = (project in file("library"))
  .settings(
    name := "Noo Programming Language - Library",
    libraryDependencies ++= Seq(),
    Compile / resourceDirectory := baseDirectory.value / "library" / "src"
  )

lazy val compiler : Project = (project in file("compiler"))
  .settings(
    name := "Noo Programming Language - Compiler",
    libraryDependencies ++= Seq(
      "org.scala-lang.modules" %% "scala-parser-combinators" % "2.3.0"
    ),
    Compile / sourceDirectory := baseDirectory.value / "compiler" / "src"
  ) dependsOn library

lazy val `macro` : Project = (project in file("macro"))
  .settings(
    name := "Noo Programming Language - Macro",
    libraryDependencies ++= Seq(
      scalaOrganization.value % "scala-reflect" % scalaVersion.value
    ),
    Compile / sourceDirectory := baseDirectory.value / "macro" / "src"
  ) dependsOn compiler

lazy val test : Project = (project in file("test"))
  .settings(
    name := "Noo Programming Language - Test",
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.19" % "test"
    ),
    Test / sourceDirectory := baseDirectory.value / "test" / "src"
  ) dependsOn `macro`
