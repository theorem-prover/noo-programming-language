package noo.compiler

import noo.compiler.syntax.surface.{Declaration, Interpolation}

import scala.language.experimental.macros

package object common {

  implicit final class StringContextExtension(val context: StringContext) extends AnyVal {

    def noo(arguments: Any*): Declaration = macro Interpolation.declaration

  }

}
