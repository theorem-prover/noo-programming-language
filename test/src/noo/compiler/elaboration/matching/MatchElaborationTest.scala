package noo.compiler.elaboration.matching

import noo.compiler.CompilerTest
import noo.compiler.common.StringContextExtension

class MatchElaborationTest extends CompilerTest {

  val Nat = noo"""
     family Nat : Type where
       case Zero : Nat
       case successor : Nat -> Nat
  """

  val add = noo"""
     define add : Nat -> Nat -> Nat where
       case add Nat.Zero          b => b
       case add (Nat.successor a) b => Nat.successor (add a b)
  """

  s"The definition of addition" should "elaborate to a case tree." in {
    assert(elaborate(Nat, add).isRight)
  }

}
