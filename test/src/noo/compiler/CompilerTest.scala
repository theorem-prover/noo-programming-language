package noo.compiler

import noo.compiler.elaboration.context.Context
import noo.compiler.elaboration.elaborator.Elaborator
import noo.compiler.syntax.surface.Declaration
import noo.compiler.error.Error
import org.scalatest.flatspec.AnyFlatSpec

abstract class CompilerTest extends AnyFlatSpec {

  def elaborate(declarations: Declaration*): Either[Error, Unit] =
    Elaborator.elaborate(declarations.toList, noo.compiler.syntax.core.Declaration.Module.root)(Context.empty)

}
