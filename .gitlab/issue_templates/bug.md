## Bug Report

The impossible happened and the compiler crashed. 

Please answer the following questions as precisely as possible: 
- *What were the exact steps that led to the error?*
- *Which operating system (name and version) do you use?*
- *Which version of `noo` do you use?*
