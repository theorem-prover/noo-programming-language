
module Test where
/*
  family Nat : Type where
    case Z : Nat
    case S : Nat -> Nat

  define add : Nat -> Nat -> Nat where
    case add Nat.Z     m => m
    case add (Nat.S n) m => Nat.S (add n m)

  family Vec (A : Type) : (length : Nat) -> Type where
    case nil  : Vec A Nat.Z
    case cons : forall k, A -> Vec A k -> Vec A (Nat.S k)

  define sum : forall k, Vec Nat (Nat.S k) -> Nat where
    case sum (Vec.cons x Vec.nil) => x
    case sum (Vec.cons x (Vec.cons _ _))  => x
*/
  family Eq { A : Type } (x : A) : (y : A) -> Type where
    case refl : Eq x x

  define symmetry : forall A, forall x y : A, Eq x y -> Eq y x where
    case symmetry { A } { x } { y } Eq.refl => Eq.refl { A } { x }

  define transitivity : forall A, forall x y z : A, Eq x y -> Eq y z -> Eq x z where
    case transitivity Eq.refl Eq.refl => Eq.refl

  define congruence : forall A B, forall x y, (f : A -> B) -> Eq x y -> Eq (f x) (f y) where
    case congruence _ Eq.refl => Eq.refl

  define substitute : forall A, forall x y, forall P : A -> Type, Eq x y -> P x -> P y where
    case substitute Eq.refl Eq.refl => Eq.refl

  define uniqueness : forall A, forall x : A, (proof : Eq x x) -> Eq proof Eq.refl where
    case uniqueness Eq.refl => Eq.refl

end
