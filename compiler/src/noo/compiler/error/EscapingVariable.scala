package noo.compiler.error

import noo.compiler.input.SourceSpan

case class EscapingVariable(name: String)(val span: SourceSpan) extends Error {

  override def kind: String = "variable error"

  override val message: String = s"The variable `$name` escapes its scope"

}
