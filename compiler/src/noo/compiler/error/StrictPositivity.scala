package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.common.Qualifier

case class StrictPositivity(family: Qualifier, constructor: String, where: String)(val span: SourceSpan) extends Error {

  override def kind: String = "declaration error"

  override val message: String = s"The signature of constructor `$constructor` is not strictly positive (this is because the positive family `$family` occurs $where)"

}
