package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.Namespace.Entry
import noo.compiler.syntax.core.{Declaration, Namespace}

case class DuplicateDeclaration(entry: Entry[Declaration], previous: Entry[Declaration], namespace: Declaration with Namespace)(val span: SourceSpan) extends Error {

  override def kind: String = "declaration error"

  override val message: String = s"Duplicate declaration of ${previous.declaration.kind} `${previous.name}`${if (entry.declaration.kind != previous.declaration.kind) s" as ${entry.declaration.kind}" else ""} in ${namespace.identifier}"

}
