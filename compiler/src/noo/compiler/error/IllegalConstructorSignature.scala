package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.common.Qualifier

case class IllegalConstructorSignature(family: Qualifier, constructor: String, actualReturnType: String)(val span: SourceSpan) extends Error {

  override def kind: String = "declaration error"

  override val message: String = s"The signature of the constructor `$constructor` is ill-formed. It must return an instance of its corresponding family `$family`, but it actually returns type `$actualReturnType`."

}
