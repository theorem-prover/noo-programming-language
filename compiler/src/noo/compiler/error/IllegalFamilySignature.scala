package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.modifier.Polarity
import noo.compiler.syntax.common.Qualifier

case class IllegalFamilySignature(family: Qualifier, polarity: Polarity, actualReturnType: String)(val span: SourceSpan) extends Error {

  override def kind: String = "declaration error"

  override val message: String = s"The signature of the $polarity family `$family` is ill-formed (it must return an instance of type `Type`, but actually returns type `$actualReturnType`)"

}
