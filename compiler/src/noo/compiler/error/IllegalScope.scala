package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.{Declaration, Namespace}

case class IllegalScope(scope: Declaration with Namespace, namespace: Declaration with Namespace)(val span: SourceSpan) extends Error {

  override def kind: String = "declaration error"

  override val message: String = s"Illegal visibility scope `${scope.qualifier}` in ${namespace.identifier}"

}
