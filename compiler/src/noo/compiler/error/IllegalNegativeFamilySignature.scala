package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.common.Qualifier

case class IllegalNegativeFamilySignature(family: Qualifier)(val span: SourceSpan) extends Error {

  override def kind: String = "declaration error"

  override val message: String = s"The signature of the negative family `$family` is ill-formed.\nNegative families may not have indices for now. Please consider using the identity type to constrain a family parameter instead."

}
