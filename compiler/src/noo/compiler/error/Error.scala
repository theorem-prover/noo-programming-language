package noo.compiler.error

import noo.compiler.common.StringExtension
import noo.compiler.common.Unicode.A
import noo.compiler.input.SourceSpan

abstract class Error {

  def span: SourceSpan

  def kind: String

  def message: String

  final lazy val format: String = {
    val result = new StringBuilder()
    result ++= s"[error] ${A(kind)} occurred in ${span.source.description} at line ${span.position.line} and character ${span.position.character}:\n\n"

    span.underline match {
      case Some(code) =>
        result ++= code.indent(4)
        result += '\n'
        result ++= message.indent(2)
        result += '\n'
      case None =>
        result ++= message.indent(2)
    }

    result.toString()
  }

  final def report(): Unit = System.err.println(format)

}
