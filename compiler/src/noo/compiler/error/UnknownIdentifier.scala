package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.{Declaration, Namespace}

case class UnknownIdentifier(name: String, namespace: Declaration with Namespace)(val span: SourceSpan) extends Error {

  override def kind: String = "name error"

  override val message: String = s"Unknown identifier `$name` in ${namespace.identifier}"

}
