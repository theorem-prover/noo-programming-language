package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.{Declaration, Signature}

case class NonExhaustivePatternMatch(definition: Declaration with Signature, missing: List[String])(val span: SourceSpan) extends Error {

  override def kind: String = "exhaustiveness error"

  override val message: String = s"The match for ${definition.identifier} is not exhaustive.${
    if (missing.isEmpty) ""
    else if (missing.length == 1) s" It would fail in case of ${missing.head}."
    else s" It would fail in case of ${missing.init.mkString(", ")} and ${missing.last}."
  }"

}
