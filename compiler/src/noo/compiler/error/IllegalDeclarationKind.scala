package noo.compiler.error

import noo.compiler.common.Unicode.a
import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.Declaration
import noo.compiler.syntax.core.Namespace.Entry

case class IllegalDeclarationKind(entry: Entry[Declaration], declarationKind: String)(val span: SourceSpan) extends Error {

  override def kind: String = "illegal access"

  override val message: String = s"${entry.declaration.identifier.capitalize} is not ${a(declarationKind)}"

}
