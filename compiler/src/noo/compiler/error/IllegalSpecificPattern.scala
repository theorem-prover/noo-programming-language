package noo.compiler.error

import noo.compiler.input.SourceSpan

case class IllegalSpecificPattern(actual: String, expected: String)(val span: SourceSpan) extends Error {

  override def kind: String = "syntax error"

  override val message: String = s"Illegal specific pattern $actual when $expected was the next expected argument"

}
