package noo.compiler.error

import noo.compiler.input.SourceSpan

case class SyntaxError(details: String)(val span: SourceSpan) extends Error {

  val kind = "syntax error"

  val message: String = details.capitalize

}
