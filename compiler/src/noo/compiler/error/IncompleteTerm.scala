package noo.compiler.error

import noo.compiler.elaboration.context.MetaVariable
import noo.compiler.input.SourceSpan

case class IncompleteTerm(variable: MetaVariable)(val span: SourceSpan) extends Error {

  override def kind: String = "hole error"

  override val message: String = s"Incomplete term (cannot solve meta-variable `${variable.toString}`)"

}
