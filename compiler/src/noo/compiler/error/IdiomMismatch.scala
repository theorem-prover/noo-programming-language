package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.modifier.Idiom

case class IdiomMismatch(expected: Idiom, actual: Idiom)(val span: SourceSpan) extends Error {

  override def kind: String = "type error"

  override val message: String = s"Expected an $expected argument, but got an $actual one"

}
