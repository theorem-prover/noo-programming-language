package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.{Declaration, Signature}

case class TooManyPatterns(number: Int, declaration: Declaration with Signature)(val span: SourceSpan) extends Error {

  override def kind: String = "pattern error"

  override val message: String = s"Too many patterns provided for ${declaration.identifier} (expected ${declaration.names.size}, but got $number)"

}
