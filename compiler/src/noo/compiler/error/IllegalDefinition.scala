package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.Declaration

case class IllegalDefinition(declaration: Declaration, related: Declaration)(val span: SourceSpan) extends Error {

  override def kind: String = "pattern error"

  override val message: String = s"Can not define ${declaration.identifier} within ${related.identifier}"

}
