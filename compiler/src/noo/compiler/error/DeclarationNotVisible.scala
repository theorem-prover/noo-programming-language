package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.Namespace.Entry
import noo.compiler.syntax.core.{Declaration, Namespace}

case class DeclarationNotVisible(entry: Entry[Declaration], from: Declaration with Namespace)(val span: SourceSpan) extends Error {

  override def kind: String = "visibility error"

  override val message: String = s"${entry.declaration.identifier.capitalize} is not visible from ${from.identifier} (visibility is restricted to ${entry.scope.identifier})"

}
