package noo.compiler.error

import noo.compiler.input.SourceSpan

case class RecursiveSolution(what: String)(val span: SourceSpan) extends Error {

  override def kind: String = "type error"

  override val message: String = s"Recursive solution for $what found"

}
