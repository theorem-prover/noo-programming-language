package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.surface.{Expression, PrettyPrinter}

case class IllegalApplicationPattern(what: String, pattern: Expression)(val span: SourceSpan) extends Error {

  override def kind: String = "pattern error"

  override val message: String = s"Illegal application of $what to pattern `${PrettyPrinter.print(pattern)}`"

}
