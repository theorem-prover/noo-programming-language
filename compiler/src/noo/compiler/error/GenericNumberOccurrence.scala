package noo.compiler.error

import noo.compiler.input.SourceSpan

import java.math.BigInteger

case class GenericNumberOccurrence(value: BigInteger)(val span: SourceSpan) extends Error {

  override def kind: String = "type error"

  override val message: String = s"Insufficient instantiation of the generic number literal `$value` (please provide a type annotation)"

}
