package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.common.Qualifier

case class IllegalProjectionSignature(family: Qualifier, projection: String)(val span: SourceSpan) extends Error {

  override def kind: String = "declaration error"

  override val message: String = s"The signature of the projection `$projection` is ill-formed. It must accept an instance of its corresponding family `$family`."

}
