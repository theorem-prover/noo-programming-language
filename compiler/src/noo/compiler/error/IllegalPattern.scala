package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.modifier.Polarity

case class IllegalPattern(actual: String, polarity: Polarity)(val span: SourceSpan) extends Error {

  override def kind: String = "pattern error"

  override val message: String = s"Illegal use of $actual in $polarity pattern position"

}
