package noo.compiler.error

import noo.compiler.input.SourceSpan

case class TypeMismatch(expected: String, actual: String, justification: Option[String])(val span: SourceSpan) extends Error {

  override def kind: String = "type error"

  override val message: String = s"Expected the type\n  `$expected`${justification.fold("")(justification => s" (because of $justification)")}\nbut got the type\n  `$actual`"

}
