package noo.compiler.error

import noo.compiler.input.SourceSpan

case class UnreachableClause(firstMatch: SourceSpan)(val span: SourceSpan) extends Error {

  override def kind: String = "pattern error"

  override val message: String = s"The pattern is not reachable" + firstMatch.underline.map { code =>
    s", because it would already match here:\n\n${code.indent(2)}"
  }.getOrElse("")

}
