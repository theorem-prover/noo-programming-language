package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.Declaration

case class UnexpectedConstructor(clause: Declaration.Clause, related: Declaration)(val span: SourceSpan) extends Error {

  override def kind: String = "pattern error"

  override val message: String = s"${clause.identifier} is not allowed in copattern position"

}
