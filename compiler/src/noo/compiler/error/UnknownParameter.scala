package noo.compiler.error

import noo.compiler.input.SourceSpan

case class UnknownParameter(parameter: String, parameters: List[String])(val span: SourceSpan) extends Error {

  override def kind: String = "name error"

  override val message: String = parameters match {
    case Nil => s"Unknown parameter `$parameter`"
    case parameter :: Nil => s"Unknown parameter `${this.parameter}` (did you mean `$parameter`?)"
    case parameters =>
      val initial = parameters.init.map("`" + _ + "`").mkString(", ")
      val last = "`" + parameters.last + "`"
      s"Unknown parameter `$parameter` (did you mean $initial or $last?)"
  }

}
