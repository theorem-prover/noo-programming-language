package noo.compiler.error

import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.Declaration.Clause.Projection

case class IncompleteProjectionPattern(projection: Projection)(val span: SourceSpan) extends Error {

  override def kind: String = "pattern error"

  override val message: String = s"The projector pattern for ${projection.identifier} is missing"

}
