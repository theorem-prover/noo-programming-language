package noo.compiler.error

import noo.compiler.input.SourceSpan

// TODO: Improve this error message. It would be nice if we can provide already handled clauses or clauses relevant for this error message.
case class PatternArityMismatch(illegalClausesCount: Int)(val span: SourceSpan) extends Error {

  override def kind: String = "syntax error"

  override val message: String = s"There exist $illegalClausesCount clauses that are not fully applied while others are. All clauses must be applied to the same amount of arguments."

}
