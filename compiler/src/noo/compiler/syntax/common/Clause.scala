package noo.compiler.syntax.common

import noo.compiler.elaboration.Elaboration

case class Clause[+A, +B](head: A, body: B) {

  def map[C](f: A => C): Clause[C, B] =
    Clause(f(head), body)

  def mapM[C](f: A => Elaboration[C]): Elaboration[Clause[C, B]] =
    f(head).map(Clause(_, body))

  def mapBody[C](f: B => C): Clause[A, C] =
    Clause(head, f(body))

  def mapBodyM[C](f: B => Elaboration[C]): Elaboration[Clause[A, C]] =
    f(body).map(Clause(head, _))

}

object Clause {

  type ClauseGroup[+A, +B] = List[Clause[A, B]]

}
