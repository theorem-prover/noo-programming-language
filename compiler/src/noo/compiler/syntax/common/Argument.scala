package noo.compiler.syntax.common

import noo.compiler.elaboration.Elaboration
import noo.compiler.modifier.Idiom

sealed trait Argument[@specialized +A] {

  def argument: A

  def idiom: Idiom

  def isSpecific: Boolean

  def asSpecific: Option[Argument.Specific[A]]

  def map[@specialized B](f: A => B): Argument[B]

  def mapM[@specialized B](f: A => Elaboration[B]): Elaboration[Argument[B]]

}

object Argument {

  def unapply[A](argument: Argument[A]): Option[(A, Idiom)] =
    argument match {
      case Default(argument, idiom) => Some((argument, idiom))
      case Specific(_, argument) => Some((argument, Idiom.Implicit))
    }

  case class Default[@specialized +A](argument: A, idiom: Idiom) extends Argument[A] {

    override val isSpecific: Boolean = false

    override val asSpecific: Option[Specific[A]] = None

    override def map[@specialized B](f: A => B): Argument[B] = Default(f(argument), idiom)

    override def mapM[@specialized B](f: A => Elaboration[B]): Elaboration[Argument[B]] =
      f(argument).map(Default(_, idiom))

  }

  case class Specific[@specialized +A](name: String, argument: A) extends Argument[A] {

    override val idiom: Idiom = Idiom.Implicit

    override val isSpecific: Boolean = false

    override val asSpecific: Option[Argument.Specific[A]] = Some(this)

    override def map[@specialized B](f: A => B): Argument[B] = Specific(name, f(argument))

    override def mapM[@specialized B](f: A => Elaboration[B]): Elaboration[Argument[B]] =
      f(argument).map(Specific(name, _))

  }

}
