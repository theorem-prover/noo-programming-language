package noo.compiler.syntax.common

import noo.compiler.syntax.core.Declaration

/**
 * An elimination is either a projection or an application. We use these to generalize between co-patterns and patterns.
 * That is, when we hit an application such as `head (tail (const n) eq))` in the surface language it is internally
 * converted to the application `const n .tail eq .head` where `n .tail eq .head` is the sequence of eliminations.
 */
sealed trait Elimination[+A] {

  def isProjection: Boolean

  def isApplication: Boolean

  def asProjection: Option[Elimination.Projection]

  def asApplication: Option[Elimination.Application[A]]

}

object Elimination {

  case class Projection(projection: Declaration.Clause.Projection/*, parameters: Spine[Value]*/) extends Elimination[Nothing] {

    override val isProjection: Boolean = true

    override val isApplication: Boolean = false

    override val asProjection: Option[Projection] = Some(this)

    override val asApplication: Option[Elimination.Application[Nothing]] = None

  }

  case class Application[+A](argument: Argument[A]) extends Elimination[A] {

    override val isProjection: Boolean = false

    override val isApplication: Boolean = true

    override val asProjection: Option[Projection] = None

    override val asApplication: Option[Elimination.Application[A]] = Some(this)

  }

}
