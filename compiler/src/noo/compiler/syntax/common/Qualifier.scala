package noo.compiler.syntax.common

sealed trait Qualifier {

  def length: Int

  def elements: List[String]

  def toString: String

  def last: Option[String]

  def initial: Option[Qualifier]

  def isEmpty: Boolean

  def startsWith(qualifier: Qualifier): Boolean

  final def +(member: String): Qualifier = Qualifier.Selection(this, member)

  final def ++(suffix: Qualifier): Qualifier = suffix match {
    case Qualifier.Empty => this
    case Qualifier.Selection(qualifier, name) => (this ++ qualifier) + name
  }

}

object Qualifier {

  def apply(elements: String*): Qualifier = elements.foldLeft[Qualifier](Empty)(Selection)

  def unapplySeq(qualifier: Qualifier): Option[List[String]] = Some(qualifier.elements)

  case object Empty extends Qualifier {

    override val length: Int = 0

    override val elements: List[String] = Nil

    override val toString: String = ""

    override val last: Option[String] = None

    override val initial: Option[Qualifier] = None

    override val isEmpty: Boolean = true

    override def startsWith(qualifier: Qualifier): Boolean = qualifier.isEmpty

  }

  case class Selection(base: Qualifier, member: String) extends Qualifier {

    override val length: Int = base.length + 1

    override lazy val elements: List[String] = base.elements :+ member

    override lazy val toString: String = base match {
      case Empty => member
      case qualifier: Selection => qualifier.toString + "." + member
    }

    override val last: Option[String] = Some(member)

    override val initial: Option[Qualifier] = Some(base)

    override val isEmpty: Boolean = false

    override def startsWith(qualifier: Qualifier): Boolean =
      elements.startsWith(qualifier.elements)

  }

}
