package noo.compiler.syntax.common

import noo.compiler.elaboration.Elaboration

sealed trait Spine[@specialized +T] extends Product with Serializable {

  def last: Argument[T]

  def initial: Spine[T]

  /**
   * Append an argument to this `Spine`.
   * @param argument The argument to append.
   * @return The new `Spine` which consists of this `Spine` and the specified argument.
   */
  def :+[U >: T](argument: Argument[U]): Spine[U]

  def ++[U >: T](iterable: Iterable[Argument[U]]): Spine[U]

  def ++[U >: T](spine: Spine[U]): Spine[U]

  def isEmpty: Boolean

  /**
   * Convert this `Spine` to a list, where the natural order the arguments is preserved (i.e., the last argument of this
   * `Spine` is the last element of the returned list).
   * @return This `Spine` as a list.
   */
  def toList: List[Argument[T]]

  /**
   * Interpret this `Spine` as its underlying "snoc" list (i.e., the last argument of this `Spine` is the head element
   * of the returned list).
   * @return The list interpretation of this `Spine`.
   */
  def asList: List[Argument[T]]

  def map[@specialized U](f: T => U): Spine[U]

  def mapM[@specialized U](f: T => Elaboration[U]): Elaboration[Spine[U]]

  def foldLeft[@specialized U](z: U)(f: (U, Argument[T]) => U): U

  def foldLeftM[@specialized U](z: Elaboration[U])(f: (U, Argument[T]) => Elaboration[U]): Elaboration[U]

  def size: Int

  def reverse: Spine[T]

  def exists(p: T => Boolean): Boolean

  def forall(p: T => Boolean): Boolean

  def take(n: Int): Spine[T]

  def drop(n: Int): Spine[T]

  def split(n: Int): (Spine[T], Spine[T])

}

object Spine {

  val empty: Spine[Nothing] = new ListSpine[Nothing](Nil)

  def unapplySeq[@specialized T](spine: Spine[T]): Option[Seq[Argument[T]]] =
    Some(spine.toList)

  def of[@specialized T](arguments: Argument[T]*): Spine[T] = ListSpine(arguments.toList.reverse)

  object :+ {

    def unapply[@specialized T](spine: Spine[T]): Option[(Spine[T], Argument[T])] =
      if (spine.isEmpty) None
      else Some((spine.initial, spine.last))

  }

  private case class ListSpine[@specialized +T](override val asList: List[Argument[T]]) extends Spine[T] {

    lazy val last: Argument[T] = asList.head

    lazy val initial: Spine[T] = ListSpine(asList.tail)

    override def take(n: Int): Spine[T] = ListSpine(asList.drop(size - n))

    override def drop(n: Int): Spine[T] = ListSpine(asList.take(size - n))

    override def split(n: Int): (Spine[T], Spine[T]) = {
      val (suffix, prefix) = asList.splitAt(size - n)
      (ListSpine(prefix), ListSpine(suffix))
    }

    override def map[@specialized U](f: T => U): Spine[U] = ListSpine(asList.map(_.map(f)))

    override def mapM[@specialized U](f: T => Elaboration[U]): Elaboration[Spine[U]] = asList.map(_.mapM(f)).sequence.map(ListSpine(_))

    override def :+[U >: T](argument: Argument[U]): Spine[U] = ListSpine[U](argument :: asList)

    override def ++[U >: T](iterable: Iterable[Argument[U]]): Spine[U] = ListSpine(iterable.toList.reverse ++ asList)

    override def ++[U >: T](spine: Spine[U]): Spine[U] = ListSpine(spine.asList ++ asList)

    override val isEmpty: Boolean = asList.isEmpty

    override lazy val toList: List[Argument[T]] = asList.reverse

    override def foldLeft[@specialized U](z: U)(f: (U, Argument[T]) => U): U =
      toList.foldLeft(z)(f)

    override def foldLeftM[@specialized U](z: Elaboration[U])(f: (U, Argument[T]) => Elaboration[U]): Elaboration[U] =
      toList.foldLeft(z) { case (result, argument) => result.flatMap(f(_, argument)) }

    val size: Int = asList.length

    lazy val reverse: Spine[T] = ListSpine(toList)

    override def exists(p: T => Boolean): Boolean = asList.exists(argument => p(argument.argument))

    override def forall(p: T => Boolean): Boolean = asList.forall(argument => p(argument.argument))

    override def equals(any: Any): Boolean = any match {
      case spine: Spine[_] => asList.equals(spine.asList)
      case _ => false
    }

    override val hashCode: Int = asList.hashCode()

    override def toString: String = s"Spine(${toList.mkString(", ")})"

  }

}
