package noo.compiler.syntax.surface.parser

import noo.compiler.input.SourceSpan
import noo.compiler.modifier.{Idiom, Modifier, Polarity}
import noo.compiler.syntax.common.{Argument, Clause}
import noo.compiler.syntax.surface.{Declaration, Expression}

trait DeclarationParser { this: Parser =>

  lazy val declarations: Parser[List[Declaration]] = rep(declaration)

  lazy val declaration: Parser[Declaration] = {
    val declaration = module | `import` | definition | axiom | family
    for {
      modifiers <- modifiers
      f <- declaration
    } yield f(modifiers)
  }

  private lazy val module: Parser[List[Modifier] => Declaration] =
    "module" ~>! withSourceSpan(qualifier.map(qualifier => (qualifier, _ : SourceSpan))) ~ ("where" ~>! declarations <~ "end") ^^ {
      case (qualifier, span) ~ declarations => Declaration.Module(_, qualifier, span, declarations)
    }

  private lazy val `import`: Parser[List[Modifier] => Declaration] =
    "import" ~>! withSourceSpan(pattern.map(pattern => (pattern, _ : SourceSpan))) ^^ {
      case (pattern, span) => Declaration.Import(_, pattern, span)
    }

  private lazy val pattern: Parser[Declaration.Import.Pattern] =
    "_" ^^^ Declaration.Import.Pattern.Wildcard |
    "(" ~>! repsep(pattern, ",") <~ ")" ^^ Declaration.Import.Pattern.Group |
    qualifier ~ ("." ~>! pattern ^^ (pattern => Declaration.Import.Pattern.Qualified(_, pattern)) | opt("as" ~>! name) ^^ {
      case Some(synonym) => Declaration.Import.Pattern.Synonym(_, synonym)
      case None => qualifier => Declaration.Import.Pattern.Synonym(qualifier, qualifier.last.get)
    }) ^^ {
      case qualifier ~ f => f(qualifier)
    }

  private lazy val axiom: Parser[List[Modifier] => Declaration] =
    "axiom" ~>! withSourceSpan(name.map(name => (name, _ : SourceSpan))) ~ (":" ~>! expression) ^^ {
      case (name, span) ~ signature => Declaration.Axiom(_, name, span, signature)
    }

  private lazy val definition: Parser[List[Modifier] => Declaration] =
    "define" ~>! withSourceSpan(name.map(name => (name, _ : SourceSpan))) ~ (":" ~>! expression) ~ ("=" ~>! expression.map(Left(_)) | "where" ~>! rep(definitionClause).map(Right(_))) ^^ {
      case (name, span)  ~ signature ~ body => Declaration.Definition(_, name, span, signature, body)
    }

  private lazy val definitionClause: Parser[Clause[Expression, Expression]] =
    "case" ~>! application ~ ("=>" ~>! expression) ^^ {
      case head ~ body => Clause(head, body)
    }

  private lazy val family: Parser[List[Modifier] => Declaration] = {
    (opt("positive" ^^^ Polarity.Positive | "negative" ^^^ Polarity.Negative).map(_.getOrElse(Polarity.Positive)) <~ "family") ~! withSourceSpan(name.map(name => (name, _ : SourceSpan))) ~ parameters ~ (":" ~>! expression) ~ ("where" ~>! rep(familyClause)) ^^ {
      case polarity ~ ((name, span)) ~ parameters ~ signature ~ clauses => modifiers => Declaration.Family(polarity :: modifiers, name, span, parameters, signature, clauses)
    }
  }

  private lazy val parameters: Parser[List[(String, Argument[Expression])]] =
    rep(parameter)

  private lazy val parameter: Parser[(String, Argument[Expression])] =
    "(" ~>! name ~ (":" ~>! expression) <~ ")" ^^ {
      case name ~ signature => (name, Argument.Default(signature, Idiom.Explicit))
    } |
    "{" ~>! name ~ (":" ~>! expression) <~ "}" ^^ {
      case name ~ signature => (name, Argument.Default(signature, Idiom.Implicit))
    }

  private lazy val familyClause: Parser[Declaration.Family.Clause] =
    modifiers ~ ("case" ~>! withSourceSpan(name.map(name => (name, _ : SourceSpan)))) ~ (":" ~>! expression) ^^ {
      case modifiers ~ ((name, span)) ~ signature => Declaration.Family.Clause(modifiers, name, span, signature)
    }

}
