package noo.compiler.syntax.surface.parser

import noo.compiler.modifier.{Modifier, Polarity, Visibility}
import noo.compiler.syntax.common.Qualifier

trait ModifierParser { this: Parser =>

  protected lazy val modifiers: Parser[List[Modifier]] = rep(modifier)

  private lazy val modifier: Parser[Modifier] = visibility

  private lazy val visibility: Parser[Modifier] =
    "private" ~>! opt("(" ~> opt(qualifier) <~ ")") ^^ {
      qualifier => Visibility.Private(qualifier.flatten.getOrElse(Qualifier.Empty))
    } | "public" ^^^ Visibility.Public

}
