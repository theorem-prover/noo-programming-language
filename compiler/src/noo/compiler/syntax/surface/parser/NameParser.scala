package noo.compiler.syntax.surface.parser

import noo.compiler.syntax.common.Qualifier

import scala.util.matching.Regex

trait NameParser { this: Parser =>

  private val keywords: Set[String] = Set(
    "define",
    "axiom",
    "module",
    "family",
    "import",
    "private",
    "public",
    "positive",
    "negative",
    "type",
    "forall",
    "end",
    "as",
    "where",
    "case",
    "let",
    "in"
  )

  private val symbols: Set[String] = Set(":", ",", "=>", "->", "(", ")", "{", "}", "_", "=", ".")

  protected lazy val name: Parser[String] = {
    val alphabeticalName: Regex = new Regex("((\\p{L}|\\p{No}|\\p{Nl}|\\p{Pc})\\p{M}*+)+\\??")
    val symbolicName: Regex = new Regex("((\\p{S}|\\p{No}|\\p{Nl}|\\p{Pc}|\\p{Pd}|[\\p{Po}&&[^\\.\",\\?]])\\p{M}*+)+\\??")
    alphabeticalName ^? ({
      case name if !keywords.contains(name) && !symbols.contains(name) => name
    }, name => s"'$name' is a reserved keyword") |
    symbolicName ^? ({
      case name if !keywords.contains(name) && !symbols.contains(name) => name
    }, name => s"'$name' is a reserved symbol") |
    "\'[^']+\'".r
  }

  protected lazy val qualifier: Parser[Qualifier] =
    rep1sep(name, ".").map(Qualifier.apply(_ : _*))

}
