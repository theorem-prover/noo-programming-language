package noo.compiler.syntax.surface.parser

import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.Argument
import noo.compiler.syntax.surface.Expression
import noo.compiler.syntax.surface.Expression.Position

trait ExpressionParser { this: Parser =>

  lazy val expression: Parser[Expression] = let | abstraction | annotation

  private lazy val let: Parser[Expression] = withPosition {
    "let" ~>! name ~ opt(":" ~>! expression) ~ ("=" ~>! expression) ~ ("in" ~>! expression) ^^ {
      case name ~ annotation ~ right ~ body => Expression.Let(name, annotation.getOrElse(Expression.Hole), right, body)
    }
  }

  private lazy val abstraction: Parser[Expression] = withPosition {
    ("(" ~> name ~ opt(":" ~> expression) <~ ")" ^^ {
      case parameter ~ head => (Argument.Default(parameter, Idiom.Explicit), head.getOrElse(Expression.Hole))
    } | "{" ~> name ~ ("as" ~>! name) <~ "}" ^^ {
      case signatureName ~ parameterName => (Argument.Specific(signatureName, parameterName), Expression.Hole)
    } | "{" ~> name ~ opt(":" ~> expression) <~ "}" ^^ {
      case parameter ~ Some(head) => (Argument.Default(parameter, Idiom.Implicit), head)
      case parameter ~ None => (Argument.Specific(parameter, parameter), Expression.Hole)
    } | name ~ opt(":" ~> application) ^^ {
      case parameter ~ head => (Argument.Default(parameter, Idiom.Explicit), head.getOrElse(Expression.Hole))
    }) ~ ("=>" ~>! expression) ^^ {
      case (parameter, head) ~ body => Expression.Lambda(parameter, head, body)
    } |
    ("(" ~> rep1(name) ~ (":" ~> expression) <~ ")" ^^ {
      case parameters ~ head => (parameters, Idiom.Explicit, head)
    } | "(" ~> expression <~ ")" ^^ {
      head => (Nil, Idiom.Explicit, head)
    } | "{" ~> rep1(name) ~ (":" ~>! expression) <~ "}" ^^ {
      case parameters ~ head => (parameters, Idiom.Implicit, head)
    } | "{" ~> rep1(name) <~ "}" ^^ {
      parameters => (parameters, Idiom.Implicit, Expression.Hole)
    } | application ^^ {
      head => (Nil, Idiom.Explicit, head)
    }) ~ ("->" ~>! expression ) ^^ {
      case (Nil, idiom, head) ~ body =>
        Expression.Pi(None, head, body, idiom)
      case (parameters, idiom, head) ~ body =>
        parameters.init.foldRight(Expression.Pi(Some(parameters.last), head, body, idiom)) {
          case (parameter, body) => Expression.Pi(Some(parameter), head, body, idiom)
        }
    } | "forall" ~>! rep1(name) ~ opt(":" ~>! expression) ~ ("," ~>! expression) ^^ {
      case parameters ~ head ~ body =>
        parameters.init.foldRight(Expression.Pi(Some(parameters.last), head.getOrElse(Expression.Hole), body, Idiom.Implicit)) {
          case (parameter, body) => Expression.Pi(Some(parameter), head.getOrElse(Expression.Hole), body, Idiom.Implicit)
        }
    }
  }

  private lazy val annotation: Parser[Expression] = withPosition {
    application ~ opt(":" ~>! application) ^^ {
      case expression ~ Some(annotation) => Expression.Annotation(expression, annotation)
      case expression ~ None => expression
    }
  }

  protected lazy val application: Parser[Expression] = withPosition {
    atom ~ rep(argument) ^^ { case function ~ arguments => arguments.foldLeft(function)(Expression.Application) }
  }

  private lazy val argument: Parser[Argument[Expression]] =
    "{" ~>! opt(name <~ "=") ~! expression <~ "}" ^^ {
      case Some(identifier) ~ argument => Argument.Specific(identifier, argument)
      case None ~ argument => Argument.Default(argument, Idiom.Implicit)
    } | atom ^^ (Argument.Default(_, Idiom.Explicit))

  private lazy val atom: Parser[Expression] =
    withPosition(qualifier ^^ Expression.Variable) | withPosition("_" ^^^ Expression.Hole) | "(" ~>! withPosition(expression) <~ ")"

  private def withPosition(parser: Parser[Expression]): Parser[Expression] =
    withSourceSpan(parser.map(expression => Position(expression, _)))

}
