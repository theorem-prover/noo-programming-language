package noo.compiler.syntax.surface.parser

import noo.compiler.elaboration.Elaboration
import noo.compiler.error.SyntaxError
import noo.compiler.input.{InputSource, SourcePosition, SourceSpan}
import noo.compiler.syntax.surface.Declaration

import scala.util.matching.Regex
import scala.util.parsing.combinator.{ImplicitConversions, RegexParsers}

final class Parser(source: InputSource)
  extends RegexParsers
  with ImplicitConversions
  with NameParser
  with ModifierParser
  with ExpressionParser
  with DeclarationParser
{

  override protected val whiteSpace: Regex = "(//[^\n]*|\\s|/\\*([^*]*|\\*+[^/])*\\*+/)+".r

  def everything: Elaboration[List[Declaration]] = parse(declarations)

  def parse[A](parser: this.Parser[A]): Elaboration[A] = parseAll(parser, source.contents) match {
    case Success(result, _) => Elaboration.pure(result)
    case NoSuccess(error, rest) =>
      val start = SourcePosition.Location(rest.pos.line, rest.pos.column, source, rest.offset)
      val end = SourcePosition.Location(rest.pos.line, rest.pos.column, source, rest.offset)
      Elaboration.fail(SyntaxError(error)(SourceSpan.Between(start, end)))
  }

  protected def withSourceSpan[A](parser: Parser[SourceSpan => A]): Parser[A] = "(//[^\n]*|\\s|/\\*([^*]*|\\*+[^/])*\\*+/)*".r ~> Parser { input =>
    val position = SourcePosition.Location(input.pos.line, input.pos.column, source, input.offset)
    parser(input) match {
      case Success(f, input) =>
        val boundary = SourcePosition.Location(input.pos.line, input.pos.column, source, input.offset)
        Success(f(SourceSpan.Between(position, boundary)), input)
      case Failure(error, input) => Failure(error, input)
      case Error(error, input) => Error(error, input)
    }
  }

}
