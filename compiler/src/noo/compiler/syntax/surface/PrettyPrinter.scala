package noo.compiler.syntax.surface

import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.Argument
import noo.compiler.syntax.surface.Expression._

object PrettyPrinter {

  def print(expression: Expression): String = expression match {
    case Hole =>
      "_"

    case Position(expression, _) =>
      print(expression)

    case Variable(qualifier) =>
      qualifier.toString

    case Application(function, argument) =>
      val left = function match {
        case Expression(_: Let | _: Annotation | _: Pi | _: Lambda) =>
          s"(${print(function)})"
        case _ =>
          print(function)
      }

      val right = argument match {
        case Argument.Specific(name, _) =>
          s"{ $name = ${print(argument.argument)} }"
        case Argument.Default(_, Idiom.Implicit) =>
          s"{ ${print(argument.argument)} }"
        case Argument(Expression(_: Let | _: Annotation | _: Pi | _: Lambda | _: Application), _) =>
          s"(${print(argument.argument)})"
        case _ =>
          print(argument.argument)
      }

      s"$left $right"

    case Annotation(expression, annotation) =>
      val left = expression match {
        case Expression(_: Let | _: Annotation | _: Pi | _: Lambda) =>
          s"(${print(expression)})"
        case _ =>
          print(expression)
      }

      val right = annotation match {
        case Expression(_: Let | _: Pi | _: Lambda) =>
          s"(${print(annotation)})"
        case _ =>
          print(annotation)
      }

      s"$left : $right"

    case Let(name, signature, right, body) =>
      s"let $name : ${print(signature)} = ${print(right)} in ${print(body)}"

    case Lambda(parameter, head, body) =>
      val left = parameter match {
        case Argument.Default(argument, Idiom.Explicit) =>
          s"($argument : ${print(head)})"

        case Argument.Default(argument, Idiom.Implicit) =>
          s"{ $argument : ${print(head)} }"

        case Argument.Specific(name, argument) =>
          s"{ $name as $argument : ${print(head)} }"
      }

      s"$left => ${print(body)}"

    case Pi(parameter, head, body, idiom) =>
      val left = parameter match {
        case Some(parameter) if idiom.isExplicit =>
          s"($parameter : ${print(head)})"
        case None if idiom.isExplicit =>
          s"(${print(head)})"
        case Some(parameter) =>
          s"{ $parameter : ${print(head)} }"
        case None =>
          s"{ ${print(head)} }"
      }

      s"$left -> ${print(body)}"
  }

}
