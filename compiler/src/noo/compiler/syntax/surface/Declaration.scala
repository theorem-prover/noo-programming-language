package noo.compiler.syntax.surface

import noo.compiler.input.SourceSpan
import noo.compiler.modifier.Modifier
import noo.compiler.syntax.common.Clause.ClauseGroup
import noo.compiler.syntax.common.{Argument, Qualifier}
import noo.compiler.syntax.core

sealed trait Declaration extends Product with Serializable {

  def modifiers: List[Modifier]

  var instance: Option[core.Declaration] = None

  def span: SourceSpan

}

object Declaration {

  case class Module(
    modifiers: List[Modifier],
    qualifier: Qualifier,
    span: SourceSpan,
    declarations: List[Declaration]
  ) extends Declaration

  case class Axiom(
    modifiers: List[Modifier],
    name: String,
    span: SourceSpan,
    signature: Expression
  ) extends Declaration

  case class Definition(
    modifiers: List[Modifier],
    name: String,
    span: SourceSpan,
    signature: Expression,
    body: Either[Expression, ClauseGroup[Expression, Expression]]
  ) extends Declaration

  case class Family(
    modifiers: List[Modifier],
    name: String,
    span: SourceSpan,
    parameters: List[(String, Argument[Expression])],
    signature: Expression,
    clauses: List[Family.Clause],
  ) extends Declaration

  object Family {

    case class Clause(
      modifiers: List[Modifier],
      name: String,
      span: SourceSpan,
      signature: Expression
    ) extends Declaration

  }

  case class Import(
    modifiers: List[Modifier],
    pattern: Import.Pattern,
    span: SourceSpan
  ) extends Declaration

  object Import {

    sealed trait Pattern extends Product with Serializable

    object Pattern {

      case object Wildcard extends Pattern

      case class Group(patterns: List[Pattern]) extends Pattern

      case class Qualified(base: Qualifier, pattern: Pattern) extends Pattern

      case class Synonym(qualifier: Qualifier, synonym: String) extends Pattern

    }

  }

}
