package noo.compiler.syntax.surface

import noo.compiler.input.SourceSpan
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.{Argument, Qualifier}

import scala.annotation.tailrec

sealed trait Expression {

  def kind: String

  lazy val getSourceSpan: Option[SourceSpan] = this match {
    case Expression.Position(expression, span) =>
      Some(expression.getSourceSpan.getOrElse(span))
    case _ =>
      None
  }

}

object Expression {

  case object Hole extends Expression {

    override val kind: String = "hole"

  }

  case class Position(expression: Expression, span: SourceSpan) extends Expression {

    override val kind: String = expression.kind

  }

  case class Variable(qualifier: Qualifier) extends Expression {

    override val kind: String = "variable"

  }

  case class Application(function: Expression, argument: Argument[Expression]) extends Expression {

    override val kind: String = "application"

  }

  case class Annotation(expression: Expression, annotation: Expression) extends Expression {

    override val kind: String = "annotation"

  }

  case class Let(name: String, signature: Expression, right: Expression, body: Expression) extends Expression {

    override val kind: String = "let"

  }

  case class Lambda(parameter: Argument[String], head: Expression, body: Expression) extends Expression {

    override val kind: String = "lambda"

  }

  case class Pi(parameter: Option[String], head: Expression, body: Expression, idiom: Idiom) extends Expression {

    override val kind: String = "dependent function space"

  }

  @tailrec
  def unapply(expression: Expression): Option[Expression] = expression match {
    case Expression.Position(expression, _) => unapply(expression)
    case _ => Some(expression)
  }

}
