package noo.compiler.syntax.core

import noo.compiler.elaboration.context.MetaVariable
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.{Argument, Qualifier, Spine}

sealed trait Term extends Product with Serializable {

  def free: Set[Qualifier]

}

object Term {

  case object Type extends Term {

    val free: Set[Qualifier] = Set.empty

  }

  sealed trait Primitive extends Term {

    val free: Set[Qualifier] = Set.empty

  }

  object Primitive {

    case class Byte(value: java.lang.Byte) extends Primitive

  }

  sealed trait Variable extends Term

  object Variable {

    case class Local(index: Index) extends Variable {

      val free: Set[Qualifier] = Set.empty

    }

    case class Global(declaration: Declaration) extends Variable {

      val free: Set[Qualifier] = Set(declaration.qualifier)

      override val toString: String = s"Free(${declaration.qualifier})"

      override val hashCode: Int = declaration.qualifier.hashCode()

    }

    case class Flexible(variable: MetaVariable) extends Variable {

      val free: Set[Qualifier] = Set.empty

    }

    case class Pruning(variable: MetaVariable, mask: Spine[Boolean]) extends Variable {

      val free: Set[Qualifier] = Set.empty

    }

  }

  case class Application(function: Term, argument: Argument[Term]) extends Term {

    lazy val free: Set[Qualifier] = function.free ++ argument.argument.free

  }

  case class Lambda(parameter: String, domain: Term, body: Term, idiom: Idiom) extends Term {

    lazy val free: Set[Qualifier] = body.free

  }

  case class Pi(parameter: String, head: Term, body: Term, idiom: Idiom) extends Term {

    lazy val free: Set[Qualifier] = head.free ++ body.free

  }

  case class Let(name: String, domain: Term, definition: Term, body: Term) extends Term {

    lazy val free: Set[Qualifier] = definition.free ++ body.free

  }

  def parameters(term: Term): List[String] = term match {
    case Pi(parameter, _, body, _) =>
      parameter :: parameters(body)
    case _ =>
      Nil
  }

}