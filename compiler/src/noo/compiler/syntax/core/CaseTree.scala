package noo.compiler.syntax.core

import noo.compiler.elaboration.matching.Matcher
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.Clause
import noo.compiler.syntax.core.Declaration.Clause.Projection

sealed trait CaseTree {

}

object CaseTree {

  case class Body(body: Term) extends CaseTree

  case class Introduction(name: String, domain: Term, body: CaseTree, idiom: Idiom) extends CaseTree

  case class Record(clauses: List[Clause[Projection, CaseTree]]) extends CaseTree

  case class Match(index: Index, clauses: List[Clause[Matcher, CaseTree]]) extends CaseTree

}
