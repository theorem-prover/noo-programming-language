package noo.compiler.syntax.core

import noo.compiler.evaluation.Conversion.Type
import noo.compiler.syntax.common.Argument

trait Signature { this: Declaration =>

  def signature: Type

  def names: List[Argument[String]]

  lazy val arity: Int = names.size

}
