package noo.compiler.syntax.core

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Context
import noo.compiler.error.{DeclarationNotVisible, DuplicateDeclaration, IllegalDeclarationKind, UnknownIdentifier}
import noo.compiler.syntax.common.Qualifier
import noo.compiler.syntax.core.Namespace.Entry

import java.util.Objects
import scala.collection.mutable
import scala.reflect.ClassTag

trait Namespace extends Iterable[Entry[Declaration]] { this: Declaration =>

  private[Namespace] val entries: mutable.Map[String, Entry[Declaration]] = mutable.LinkedHashMap.empty
  private[Namespace] var dirty: Boolean = false

  def children: List[Declaration] = entries.values.map(_.declaration).filter(_.isChildOf(this)).toList

  override def iterator: Iterator[Entry[Declaration]] = entries.valuesIterator

  override lazy val toString: String = identifier

  def find(name: String): Option[Entry[Declaration]] = get(name).orElse(parent.flatMap(_.find(name)))

  def isDirty: Boolean = dirty

  def reset(): Unit = dirty = false

  def mark(): Unit = {
    var current = this
    while (current ne null) {
      current.dirty = true
      current = current.parent.orNull
    }
  }

  def get(name: String): Option[Entry[Declaration]] = entries.get(name)

  def set(name: String, entry: Entry[Declaration]): Option[Entry[Declaration]] = {
    val previous = entries.get(name)
    if (!previous.contains(entry)) mark()
    entries(name) = entry
    previous
  }

  def contains(name: String): Boolean = entries.contains(name)

  def relativize(declaration: Declaration): Qualifier =
    if (declaration.qualifier.startsWith(this.qualifier)) {
      Qualifier(declaration.qualifier.elements.drop(Math.min(this.qualifier.length, declaration.qualifier.elements.size - 1)): _*)
    } else entries.find(_._2.declaration == declaration) match {
      case Some((name, _)) =>
        // The declaration is defined here
        Qualifier(name)
      case None =>
        // The declaration is not directly defined here, so let's check if its parent is defined here
        declaration.parent match {
          case Some(parent) =>
            this.relativize(parent) + declaration.name
          case None =>
            // There is no parent, so this must be the root module
            declaration.qualifier
        }
    }

  def resolve(qualifier: Qualifier): Elaboration[Option[Entry[Declaration]]] = (context: Context) =>
    resolveOrFail(qualifier)(context) match {
      case Left(_) => Right(None)
      case Right(entry) => Right(Some(entry))
    }

  def resolveOrFail(qualifier: Qualifier): Elaboration[Entry[Declaration]] =
    qualifier match {
      case Qualifier.Empty =>
        Elaboration.pure(this.self)
      case Qualifier.Selection(base, member) =>
        for {
          nameSpaceEntry <- resolveOrFail(base).flatMap(_.as[Declaration with Namespace]("namespace"))
          optionalEntry = base match {
            case Qualifier.Empty =>
              // do a real lookup
              nameSpaceEntry.declaration.find(member)
            case _ =>
              // just resolve the member
              nameSpaceEntry.declaration.entries.get(member)
          }

          entry <- optionalEntry match {
            case Some(entry) =>
              if (entry.isVisibleFrom(this)) {
                Elaboration.pure(entry)
              } else {
                Elaboration.fail(DeclarationNotVisible(entry, this) _)
              }

            case None =>
              Elaboration.fail(UnknownIdentifier(member, nameSpaceEntry.declaration) _)
          }
        } yield entry
    }

  def declare[D <: Declaration](entry: Entry[D]): Elaboration[Entry[D]] =
    for {
      _ <- get(entry.declaration.name) match {
        case Some(other) if other != entry =>
          Elaboration.fail(DuplicateDeclaration(entry, other, this) _)
        case previous =>
          if (!previous.contains(entry)) mark()
          this.entries.put(entry.name, entry)
          Elaboration.pure(entry)
      }
    } yield entry

}

object Namespace {

  sealed trait Entry[+D <: Declaration] extends Product with Serializable {

    def name: String

    def declaration: D

    def scope: Declaration with Namespace

    def source: Entry[D]

    def as[T <: Declaration](kind: String)(implicit tag: ClassTag[T]): Elaboration[Entry[T]] =
      if (tag.runtimeClass.isAssignableFrom(declaration.getClass)) {
        Elaboration.pure(this.asInstanceOf[Entry[T]])
      } else {
        Elaboration.fail(IllegalDeclarationKind(this, kind) _)
      }

    def link(name: String, scope: Declaration with Namespace): Entry[D] = Entry.Link(name, this, scope)

    def isVisibleFrom(namespace: Declaration with Namespace): Boolean = namespace.isChildOf(scope)

    override def equals(any: Any): Boolean = any match {
      case entry: Entry[_] if name == entry.name && scope == entry.scope => declaration eq entry.declaration
      case _ => false
    }

    override lazy val hashCode: Int = Objects.hashCode(name, scope, System.identityHashCode(declaration))

  }

  object Entry {

    def apply[D <: Declaration](declaration: D, scope: Declaration with Namespace) : Entry[D] = Identity(declaration, scope)

    def unapply[D <: Declaration](entry: Entry[D]): Option[D] = Some(entry.declaration)

    private case class Link[D <: Declaration](name: String, entry: Entry[D], scope: Declaration with Namespace) extends Entry[D] {

      override val declaration: D = entry.declaration

      override val source: Entry[D] = entry.source

    }

    private case class Identity[D <: Declaration](declaration: D, scope: Declaration with Namespace) extends Entry[D] {

      override val name: String = declaration.name

      override val source: Entry[D] = this

    }

  }

}
