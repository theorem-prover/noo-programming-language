package noo.compiler.syntax.core

import noo.compiler.evaluation.Value
import noo.compiler.modifier.Polarity
import noo.compiler.syntax.common.{Argument, Qualifier}
import noo.compiler.syntax.core.Namespace.Entry

import scala.language.reflectiveCalls

sealed trait Declaration extends Product with Serializable {

  def name: String

  def kind: String

  def parent: Option[Declaration with Namespace]

  val qualifier: Qualifier = parent.map(_.qualifier).getOrElse(Qualifier.Empty) + name

  final lazy val self: Entry[this.type] = Entry[this.type](declaration = this, parent.getOrElse(Declaration.Module.root))

  lazy val identifier: String = s"$kind `$qualifier`"

  def isChildOf(namespace: Declaration with Namespace): Boolean =
    qualifier.startsWith(namespace.qualifier)

  override def equals(any: Any): Boolean = any match {
    case declaration: Declaration => declaration.qualifier == qualifier
    case _ => false
  }

  override lazy val toString: String = identifier

}

object Declaration {

  case class Axiom(name: String, signature: Value, names: List[Argument[String]], parent: Option[Declaration with Namespace]) extends Declaration with Signature {

    override val kind: String = "axiom"

  }

  case class Definition(name: String, signature: Value, names: List[Argument[String]], parent: Option[Declaration with Namespace]) extends Declaration with Signature {

    /**
     * This is the [[CaseTree]] that is associated with this definition. When this field is `None`, this means that the
     * case tree is not yet fully elaborated and the definition has to be considered as abstract (i.e., it cannot be unfolded).
     */
    var tree: Option[CaseTree] = None

    override val kind: String = "definition"

  }

  case class Family(name: String, parameters: List[Argument[String]], indices: List[Argument[String]], signature: Value, polarity: Polarity, parent: Option[Declaration with Namespace]) extends Declaration with Namespace with Signature {

    override val names: List[Argument[String]] = parameters ++ indices

    override val kind: String = "family"

    def clauses: List[Clause] = this.collect {
      case Entry(clause: Clause) => clause
    }.toList

  }

  sealed trait Clause extends Declaration with Signature {

    override def parent: Option[Family]

    def definition: Family = parent.get

    def isProjection: Boolean

    def isConstructor: Boolean

  }

  object Clause {

    case class Constructor(name: String, signature: Value, names: List[Argument[String]], parent: Option[Family]) extends Clause {

      override val kind: String = "constructor"

      override val isProjection: Boolean = false

      override val isConstructor: Boolean = true

    }

    case class Projection(name: String, signature: Value, names: List[Argument[String]], parent: Option[Family]) extends Clause {

      override val kind: String = "projection"

      override val isProjection: Boolean = true

      override val isConstructor: Boolean = false

    }

  }

  case class Module(name: String, parent: Option[Declaration with Namespace]) extends Declaration with Namespace {

    override val kind: String = "module"

  }

  object Module {

    val root: Module with ({

      def Type: Declaration.Definition

    }) = new Module("root", None) {

      override val qualifier: Qualifier = Qualifier.Empty

      override lazy val identifier: String = "the root module"

      val Type: Declaration.Definition = Declaration.Definition("Type", Value.Type, List(), Some(this))
      Type.tree = Some(CaseTree.Body(Term.Type))

    }

    root.declare(root.Type.self.link("Type", Module.root))

  }

}
