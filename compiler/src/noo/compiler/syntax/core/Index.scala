package noo.compiler.syntax.core

import noo.compiler.evaluation.Level

import scala.language.implicitConversions

case class Index(value: Int) extends AnyVal {

  def toLevel(level: Level): Level = Level(level.value - (value + 1))

  def asLevel: Level = Level(value)

  def +(offset: Int): Index = Index(this.value + offset)

  def -(offset: Int): Index = Index(this.value - offset)

  override def toString: String = value.toString

}

object Index {

  implicit def toIndex(value: Int): Index = Index(value)
  
}
