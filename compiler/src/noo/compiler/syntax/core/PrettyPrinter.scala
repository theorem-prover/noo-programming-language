package noo.compiler.syntax.core

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.Argument

object PrettyPrinter {

  def print(value: Value): Elaboration[String] =
    Conversion.reify(value, unfold = false).flatMap(print)

  def print(term: Term): Elaboration[String] =
    term match {
      case Term.Type =>
        Elaboration.pure("Type")

      case Term.Primitive.Byte(value) =>
        Elaboration.pure("" + value)

      case Term.Variable.Local(index) =>
        Elaboration.environment.map { environment =>
          if (index.value < environment.size && index.value >= 0) {
            environment.getByIndex(index).name
          } else {
            index.toString
          }
        }

      case Term.Variable.Global(declaration) =>
        Elaboration.namespace.map { namespace =>
          namespace.relativize(declaration).toString
        }

      case Term.Variable.Flexible(variable) =>
        Elaboration.pure(variable.toString)

      case Term.Application(function, argument) =>
        for {
          left <- function match {
            case _: Term.Lambda | _: Term.Pi | _: Term.Let =>
              print(function).map("(" + _ + ")")
            case _ =>
              print(function)
          }
          right <- print(argument)
        } yield s"$left $right"

      case Term.Lambda(parameter, domain, body, idiom) =>
        for {
          domainValue <- Conversion.evaluate(domain)
          domain <- print(domain)
          body <- Elaboration.bind(parameter, domainValue, idiom, Visible) {
            _ => print(body)
          }
        } yield idiom match {
          case Idiom.Explicit => s"($parameter : $domain) => $body"
          case Idiom.Implicit => s"{ $parameter : $domain } => $body"
        }

      case Term.Pi(parameter, head, body, idiom) =>
        for {
          headValue <- Conversion.evaluate(head)
          head <- print(head)
          body <- Elaboration.bind(parameter, headValue, idiom, Visible) {
            _ => print(body)
          }
        } yield idiom match {
          case Idiom.Explicit => s"($parameter : $head) -> $body"
          case Idiom.Implicit => s"{ $parameter : $head } -> $body"
        }

      case Term.Let(name, domain, definition, body) =>
        for {
          domainValue <- Conversion.evaluate(domain)
          domain <- print(domain)
          definition <- print(definition)
          body <- Elaboration.bind(name, domainValue, Idiom.Explicit, Visible) {
            _ => print(body)
          }
        } yield s"let $name : $domain = $definition in $body"
    }

  def print(argument: Argument[Term]): Elaboration[String] = argument match {
    case Argument.Default(_: Term.Lambda | _: Term.Pi | _: Term.Let | _: Term.Application, Idiom.Explicit) =>
      print(argument.argument).map("(" + _ + ")")

    case Argument.Default(argument, Idiom.Explicit) =>
      print(argument)

    case Argument.Default(argument, Idiom.Implicit) =>
      print(argument: Term).map("{ " + _ + " }")

    case Argument.Specific(name, argument) =>
      print(argument: Term).map("{ " + name + " = " + _ + " }")
  }

}
