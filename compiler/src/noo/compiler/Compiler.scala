package noo.compiler

import noo.compiler.elaboration.context.Context
import noo.compiler.elaboration.elaborator.Elaborator
import noo.compiler.input.InputSource
import noo.compiler.syntax.surface.parser.Parser

import java.nio.charset.StandardCharsets
import java.nio.file.Path
import scala.collection.mutable.ArrayBuffer

object Compiler {

  def main(arguments: Array[String]): Unit = {
    val sources = ArrayBuffer.empty[InputSource]

    val iterator = arguments.iterator
    while (iterator.hasNext) {
      iterator.next() match {
        case "-h" | "-?" | "--help" =>
          ???

        case "-v" | "--version" =>
          ???

        case file =>
          sources += InputSource.from(Path.of(file), StandardCharsets.UTF_8, None)
      }
    }

    val elaboration = for {
      declarations <- sources.toList.map(new Parser(_).everything).sequence.map(_.flatten)
      _ <- Elaborator.elaborate(declarations, noo.compiler.syntax.core.Declaration.Module.root)
    } yield ()

    elaboration(Context.empty) match {
      case Left(error) =>
        error.report()
      case Right(_) =>
        ()
    }
  }

}
