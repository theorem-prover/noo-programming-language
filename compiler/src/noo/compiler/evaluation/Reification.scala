package noo.compiler.evaluation

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.MetaEntry
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.syntax.common.{Elimination, Spine}
import noo.compiler.syntax.core.Term

trait Reification { this: Conversion.type =>

  def reify(value: Value, unfold: Boolean): Elaboration[Term] = force(value, unfold).flatMap {
    case _: Value.Thunk => impossible

    case Value.Type =>
      Elaboration.pure(Term.Type)

    case Value.Primitive.Byte(value) =>
      Elaboration.pure(Term.Primitive.Byte(value))

    case Value.Application.Local(level, spine) =>
      Elaboration.context.flatMap { context =>
        reify(Term.Variable.Local(level.toIndex(context.environment.level)), spine, unfold)
      }

    case application: Value.Application.Global if unfold =>
      Conversion.unfold(application).flatMap {
        case Some(value) =>
          reify(value, unfold)
        case None =>
          reify(Term.Variable.Global(application.declaration), application.arguments, unfold)
      }

    case application: Value.Application.Global =>
      reify(Term.Variable.Global(application.declaration), application.arguments, unfold)

    case Value.Application.Flexible(variable, spine) =>
      Elaboration.context.flatMap(_.get(variable) match {
        case MetaEntry.Solved(_, value, _) => apply(value, spine).flatMap(reify(_, unfold))
        case _ => reify(Term.Variable.Flexible(variable), spine, unfold)
      })

    case Value.Lambda(parameter, domain, body, idiom) =>
      for {
        body <- Elaboration.bind(parameter, domain, idiom, Visible) {
          binding => body(binding.value).flatMap(reify(_, unfold))
        }
        domain <- reify(domain, unfold)
      } yield Term.Lambda(parameter, domain, body, idiom)

    case Value.Pi(parameter, head, body, idiom) =>
      for {
        body <- Elaboration.bind(parameter, head, idiom, Visible) {
          binding => body(binding.value).flatMap(reify(_, unfold))
        }
        head <- reify(head, unfold)
      } yield Term.Pi(parameter, head, body, idiom)
  }

  private def reify(function: Term, spine: Spine[Value], unfold: Boolean): Elaboration[Term] =
    if (spine.isEmpty) Elaboration.pure(function)
    else for {
      term <- reify(function, spine.initial, unfold)
      argument <- spine.last.mapM(reify(_, unfold))
    } yield Term.Application(term, argument)

  private def reify(function: Term, eliminations: List[Elimination[Value]], unfold: Boolean): Elaboration[Term] = {
    val index = eliminations.indexWhere(_.isProjection)
    if (index >= 0) {
      val (prefix, Elimination.Projection(projection) :: suffix) = eliminations.splitAt(index)
      for {
        term <- reify(function, Spine.of(prefix.map(_.asApplication.get.argument): _*), unfold)
        term <- reify(Term.Application(Term.Variable.Global(projection), projection.names.head.map(_ => term)), suffix, unfold)
      } yield term
    } else {
      reify(function, Spine.of(eliminations.map(_.asApplication.get.argument): _*), unfold)
    }
  }

}
