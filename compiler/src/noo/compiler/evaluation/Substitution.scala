package noo.compiler.evaluation

import noo.compiler.elaboration.Elaboration
import noo.compiler.syntax.common.Spine

import scala.collection.immutable.IntMap

final class Substitution private(private val entries: IntMap[Value]) extends (PartialFunction[Level, Value]) {

  def this() = this(IntMap.empty)

  def domain: Set[Level] = entries.keySet.map(Level.apply)

  def isEmpty: Boolean = entries.isEmpty

  def -(level: Level): Substitution =
    new Substitution(entries - level.value)

  def +(entry: (Level, Value)): Substitution = {
    val (level, value) = entry
    new Substitution(entries + (level.value, value))
  }

  def compose(substitution: Substitution): Elaboration[Substitution] =
    for {
      prefix <- entries.map { case (level, value) =>
        substitution(value).map((level, _))
      }.sequence
      suffix = substitution.entries.filterNot {
        case (level, _) => entries.contains(level)
      }
      composition = new Substitution(suffix ++ prefix)
    } yield composition

  override def isDefinedAt(level: Level): Boolean =
    entries.contains(level.value)

  override def apply(level: Level): Value =
    entries(level.value)

  def apply(value: Value): Elaboration[Value] = Conversion.force(value, unfold = false).flatMap {
    case Value.Type =>
      Elaboration.pure(Value.Type)

    case Value.Application.Local(level, spine) =>
      for {
        spine <- apply(spine)
        result <- entries.get(level.value) match {
          case Some(value) =>
            Conversion.apply(value, spine)
          case None =>
            Elaboration.pure(Value.Application.Local(level.value, spine))
        }
      } yield result

    case Value.Application.Global.Definition(declaration, arguments) =>
      for {
        arguments <- apply(arguments)
        result <- Conversion.unfold(Value.Application.Global.Definition(declaration, arguments)).map {
          case Some(value) =>
            // It is important that we do not apply this substitution on the result of the unfolding. We already
            // applied the substitution on the spine, so no "dangling" reference should escape into the top-level
            // definition we face here.
            value
          case None =>
            Value.Application.Global.Definition(declaration, arguments)
        }
      } yield result

    case Value.Application.Global.Declaration(declaration, arguments) =>
      for {
        arguments <- apply(arguments)
        result <- Conversion.unfold(Value.Application.Global.Declaration(declaration, arguments)).map {
          case Some(value) =>
            // It is important that we do not apply this substitution on the result of the unfolding. We already
            // applied the substitution on the spine, so no "dangling" reference should escape into the top-level
            // definition we face here.
            value
          case None =>
            Value.Application.Global.Declaration(declaration, arguments)
        }
      } yield result

    case Value.Application.Flexible(variable, spine) =>
      for {
        spine <- apply(spine)
      } yield Value.Application.Flexible(variable, spine)

    case Value.Lambda(parameter, domain, body, idiom) =>
      for {
        domain <- apply(domain)
      } yield Value.Lambda(parameter, domain, argument => for {
        body <- body(argument)
        body <- apply(body)
      } yield body, idiom)

    case Value.Pi(parameter, head, body, idiom) =>
      for {
        head <- apply(head)
      } yield Value.Pi(parameter, head, argument => for {
        body <- body(argument)
        body <- apply(body)
      } yield body, idiom)
  }

  def apply(spine: Spine[Value]): Elaboration[Spine[Value]] =
    spine.mapM(apply)

}

object Substitution {

  val empty: Substitution =
    new Substitution(IntMap.empty)

  def identity(levels: Level*): Substitution =
    apply(levels.map(level => (level, Value.Application.Local(level, Spine.empty))): _*)

  def apply(entries: (Level, Value)*): Substitution =
    new Substitution(IntMap(entries.map {
      case (level, value) => (level.value, value)
    }: _*))

}
