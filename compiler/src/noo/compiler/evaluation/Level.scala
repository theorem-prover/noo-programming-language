package noo.compiler.evaluation

import noo.compiler.syntax.core.Index

import scala.language.implicitConversions

case class Level(value: Int) extends AnyVal {

  def toIndex(level: Level): Index = Index(level.value - (value + 1))

  def asIndex: Index = Index(value)

  def +(offset: Int): Level = Level(this.value + offset)

  def -(offset: Int): Level = Level(this.value - offset)

  override def toString: String = value.toString

  def until(level: Level): Range = value until level.value

  def to(level: Level): Range = value to level.value

}

object Level {

  implicit def toLevel(value: Int): Level = Level(value)

}
