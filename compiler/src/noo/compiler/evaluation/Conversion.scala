package noo.compiler.evaluation

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.{Binding, MetaEntry}
import noo.compiler.syntax.common.Spine.:+
import noo.compiler.syntax.common.{Argument, Spine}
import noo.compiler.syntax.core.Term

object Conversion extends Evaluation with Reification {

  final type Type = Value

  def normalize(term: Term): Elaboration[Term] =
    evaluate(term).flatMap(reify(_, unfold = false))

  def apply(function: Value, argument: Argument[Value]): Elaboration[Value] = function match {
    case thunk: Value.Thunk =>
      thunk.value.flatMap(apply(_, argument))

    case Value.Lambda(_, _, function, _) =>
      function(argument.argument)

    case Value.Application.Local(level, spine) =>
      Elaboration.pure(Value.Application.Local(level, spine :+ argument))

    case Value.Application.Global.Definition(definition, arguments) =>
      Elaboration.pure(Value.Application.Global.Definition(definition, arguments :+ argument))

    case Value.Application.Global.Declaration(declaration, spine) =>
      Elaboration.pure(Value.Application.Global.Declaration(declaration, spine :+ argument))

    case Value.Application.Flexible(variable, spine) =>
      Elaboration.pure(Value.Application.Flexible(variable, spine :+ argument))

    case _ =>
      impossible
  }

  def apply(function: Value, spine: Spine[Value]): Elaboration[Value] =
    if (spine.isEmpty) Elaboration.pure(function)
    else apply(function, spine.initial).flatMap(apply(_, spine.last))

  /**
   * Forces the head of the provided value. This also unfolds any definition in head position.
   * @param value The value whose head should be forced.
   * @return The value with a forced head.
   */
  def force(value: Value, unfold: Boolean): Elaboration[Value] = value match {
    case Value.Application.Flexible(variable, spine) =>
      Elaboration.context.flatMap(_.get(variable) match {
        case MetaEntry.Solved(_, value, _) =>
          for {
            value <- apply(value, spine)
            value <- force(value, unfold)
          } yield value
        case _ =>
          Elaboration.pure(value)
      })

    case thunk: Value.Thunk =>
      thunk.value.flatMap(force(_, unfold))

    case application: Value.Application.Global if unfold =>
      Conversion.unfold(application).flatMap {
        case Some(value) => force(value, unfold)
        case None => Elaboration.pure(application)
      }

    case application: Value.Application.Local if unfold =>
      for {
        environment <- Elaboration.environment
        result <- environment.get(application.level) match {
          case Binding.Definition(_, _, _, value, _) =>
            Conversion.apply(value, application.arguments).flatMap(force(_, unfold))

          case _ =>
            Elaboration.pure(application)
        }
      } yield result

    case value =>
      Elaboration.pure(value)
  }

  def instantiate(signature: Type, argument: Argument[Value]): Elaboration[Type] =
    instantiate(signature, Spine.of(argument))

  def instantiate(signature: Type, spine: Spine[Value]): Elaboration[Type] = spine match {
    case Spine.empty =>
      Elaboration.pure(signature)

    case spine :+ Argument(argument, _) =>
      for {
        signature <- instantiate(signature, spine)
        signature <- force(signature, unfold = true).flatMap {
          case Value.Pi(_, _, body, _) =>
            body(argument)
          case _ =>
            impossible
        }
      } yield signature
  }

}
