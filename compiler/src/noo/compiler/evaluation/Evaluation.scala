package noo.compiler.evaluation

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.Elaboration.OptionExtension2
import noo.compiler.elaboration.context.MetaEntry
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.syntax.common.{Argument, Clause, Elimination, Spine}
import noo.compiler.syntax.core.Declaration.Definition
import noo.compiler.syntax.core.{CaseTree, Declaration, Term}

trait Evaluation { this: Conversion.type =>

  def evaluate(term: Term): Elaboration[Value] =
    term match {
      case Term.Type =>
        Elaboration.pure(Value.Type)

      case Term.Primitive.Byte(value) =>
        Elaboration.pure(Value.Primitive.Byte(value))

      case Term.Variable.Local(index) =>
        Elaboration.environment.map { environment =>
          environment.value(index)
        }

      case Term.Variable.Global(definition: Definition) if definition.tree.isDefined =>
        // The case tree is not stuck so we can evaluate its value
        Elaboration.pure(Value.Application.Global.Definition(definition, Spine.empty))

      case Term.Variable.Global(declaration) =>
        Elaboration.pure(Value.Application.Global.Declaration(declaration, Spine.empty))

      case Term.Variable.Flexible(variable) =>
        Elaboration.context.map(_.get(variable) match {
          case MetaEntry.Solved(_, value, _) => value
          case _ => Value.Application.Flexible(variable, Spine.empty)
        })

      case Term.Variable.Pruning(variable, mask) =>
        Elaboration.context.flatMap(_.get(variable) match {
          case MetaEntry.Solved(_, value, _) => apply(value, mask)
          case _ => apply(Value.Application.Flexible(variable, Spine.empty), mask)
        })

      case Term.Application(function, argument) =>
        for {
          context <- Elaboration.context
          function <- evaluate(function)
          result <- apply(function, argument.map(argument => new Value.Thunk(evaluate(argument), context)))
        } yield result

      case Term.Lambda(parameter, domain, body, idiom) =>
        for {
          domain <- evaluate(domain)
          static <- Elaboration.context
        } yield Value.Lambda(parameter, domain, { argument =>
          // We use the *static* context of this lambda here to evaluate the body.
          Elaboration.withContext(static) {
            Elaboration.define(parameter, domain, argument, Visible) {
              _ => evaluate(body)
            }
          }
        }, idiom)

      case Term.Pi(parameter, head, body, idiom) =>
        for {
          head <- evaluate(head)
          static <- Elaboration.context
        } yield Value.Pi(parameter, head, { argument =>
          // We use the *static* context of this pi here to evaluate the body.
          Elaboration.withContext(static) {
            Elaboration.define(parameter, head, argument, Visible) {
              _ => evaluate(body)
            }
          }
        }, idiom)

      case Term.Let(name, domain, definition, body) =>
        for {
          context <- Elaboration.context
          domain <- evaluate(domain)
          result <- Elaboration.define(name, domain, new Value.Thunk(evaluate(definition), context), Visible) {
            _ => evaluate(body)
          }
        } yield result
    }

  def unfold(application: Value.Application.Global): Elaboration[Option[Value]] = {
    def apply(tree: CaseTree, eliminations: Vector[Elimination[Value]]): Elaboration[Option[Value]] = tree match {
      case CaseTree.Body(body) =>
        for {
          value <- evaluate(body)
          result <- if (eliminations.isEmpty) Elaboration.pure(Some(value)) else Conversion.force(value, unfold = false).flatMap {
            case application: Value.Application.Global =>
              unfold(application, eliminations)
            case value =>
              if (eliminations.forall(_.isApplication)) {
                // TODO: This is only a quick fix for most situations (when all eliminations are ordinary applications)
                Conversion.apply(value, Spine.of(eliminations.map(_.asApplication.get.argument): _*)).map(Some(_))
              } else {
                // TODO: In this case we have to reconstruct the projection eliminations, for which we need the parameters
                //       of the projection's family.
                impossible
              }
          }
        } yield result

      case _: CaseTree.Introduction if eliminations.isEmpty =>
        Elaboration.pure(None)

      case CaseTree.Introduction(name, domain, body, _) =>
        for {
          domain <- evaluate(domain)
          // Since we are already well-typed here, the first elimination must be an application in order to unfold this introduction.
          argument = eliminations.head.asApplication.get.argument
          result <- Elaboration.define(name, domain, argument.argument, Visible) {
            _ => apply(body, eliminations.tail)
          }
        } yield result

      case _: CaseTree.Record if eliminations.isEmpty =>
        Elaboration.pure(None)

      case CaseTree.Record(clauses) =>
        // Since we are already well-typed here, the first elimination must be a projection in order to unfold this record.
        val projection = eliminations.head.asProjection.get.projection
        clauses.find(_.head == projection) match {
          case Some(Clause(_, body)) =>
            apply(body, eliminations.tail)

          case None =>
            // The record is not defined for this projection, so we cannot unfold it
            Elaboration.pure(None)
        }

      case CaseTree.Match(index, clauses) =>
        for {
          environment <- Elaboration.environment
          // Now, we can check if the scrutinee is bound to a constructor application in this context. If an `Intro` was
          // called with such an application, it is passed down to this point via a definition binding.
          value <- Conversion.force(environment.value(index), unfold = true)
          result <- value match {
            case Value.Application.Global.Declaration(constructor: Declaration.Clause.Constructor, constructorArguments) =>
              // The value is a constructor application. This means, we can advance the evaluation of this match by
              // selecting the corresponding clause, evaluating its body.
              val Clause(matcher, body) = clauses.find(_.head.constructor == constructor).get
              val values = constructorArguments.toList.toVector.map(_.argument)
              matcher.define(index, values) {
                apply(body, eliminations)
              }

            case _ =>
              Elaboration.pure(None)
          }
        } yield result
    }

    def unfold(application: Value.Application.Global, eliminations: Vector[Elimination[Value]]): Elaboration[Option[Value]] = {
      application.declaration match {
        case projection: Declaration.Clause.Projection =>
          // This is a projection that is applied at least to its projector. Here, we first drop the family parameters (as
          // these cannot be matched anyway) and then try to unfold the projector using this projection with its remaining
          // arguments as additional eliminations.
          application.arguments.toList.drop(projection.definition.parameters.size) match {
            case Argument(projector, _) :: arguments =>
              Conversion.force(projector, unfold = true).flatMap {
                case application: Value.Application.Global =>
                  unfold(application, (Elimination.Projection(projection) :: arguments.map(Elimination.Application(_))).toVector ++ eliminations)
                case _ =>
                  Elaboration.pure(None)
              }
            case Nil =>
              Elaboration.pure(None)
          }

        case definition: Declaration.Definition =>
          definition.tree.mapM(apply(_, application.arguments.toList.toVector.map(Elimination.Application(_)) ++ eliminations)).map(_.flatten)

        case _ =>
          Elaboration.pure(None)
      }
    }

    unfold(application, Vector.empty)
  }

  private def apply(function: Value, mask: Spine[Boolean]): Elaboration[Value] =
    Elaboration.context.flatMap { context =>
      context.environment.values.zip(mask.asList).foldRight(Elaboration.pure[Value](function)) {
        case ((argument, mask), function) if mask.argument =>
          function.flatMap(apply(_, mask.map(_ => argument)))
        case ((_, _), function) =>
          function
      }
    }

}
