package noo.compiler.evaluation

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.elaboration.context.{Context, MetaVariable}
import noo.compiler.error.Error
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.{Elimination, Spine}
import noo.compiler.syntax.core

sealed trait Value

object Value {

  case object Type extends Value

  sealed trait Primitive extends Value

  object Primitive {

    case class Byte(value: java.lang.Byte) extends Primitive

  }

  sealed trait Application extends Value {

    def arguments: Spine[Value]

  }

  object Application {

    case class Local(level: Level, arguments: Spine[Value]) extends Application

    sealed trait Global extends Application {

      def declaration: core.Declaration

    }

    object Global {

      case class Definition(override val declaration: core.Declaration.Definition, arguments: Spine[Value]) extends Global {

        override def toString: String = s"Free.Definition(${declaration.qualifier}, $arguments)"

      }

      case class Declaration(override val declaration: core.Declaration, arguments: Spine[Value]) extends Global {

        override def toString: String = s"Free.Declaration(${declaration.qualifier}, $arguments)"

      }

    }

    case class Flexible(variable: MetaVariable, arguments: Spine[Value]) extends Application

  }

  final class Thunk(elaboration: => Elaboration[Value], context: Context) extends Value {

    val value: Elaboration[Value] = {
      var result: Either[Error, Value] = null
      (_: Context) => {
        if (result eq null) {
          result = elaboration(context)
        }
        result
      }
    }

  }

  case class Lambda(parameter: String, domain: Value, body: Value => Elaboration[Value], idiom: Idiom) extends Value

  case class Pi(parameter: String, head: Value, body: Value => Elaboration[Value], idiom: Idiom) extends Value

  def free(value: Value): Elaboration[Set[Level]] = Conversion.force(value, unfold = false).flatMap {
    case Value.Type =>
      Elaboration.pure(Set.empty)

    case Value.Application.Global.Definition(_, spine) =>
      free(spine)

    case Value.Application.Global.Declaration(_, spine) =>
      free(spine)

    case Value.Application.Flexible(_, spine) =>
      free(spine)

    case Value.Application.Local(level, spine) =>
      free(spine).map(_ + level)

    case Value.Lambda(parameter, domain, body, idiom) =>
      for {
        domainFree <- free(domain)
        bodyFree <- Elaboration.bind(parameter, domain, idiom, Visible) {
          binding => body(binding.value).flatMap(free).map(_ - binding.level)
        }
      } yield domainFree ++ bodyFree

    case Value.Pi(parameter, head, body, idiom) =>
      for {
        headFree <- free(head)
        bodyFree <- Elaboration.bind(parameter, head, idiom, Visible) {
          binding => body(binding.value).flatMap(free).map(_ - binding.level)
        }
      } yield headFree ++ bodyFree
  }

  def free(eliminations: List[Elimination[Value]]): Elaboration[Set[Level]] =
    eliminations.map {
      case _: Elimination.Projection => Elaboration.pure(Set.empty)
      case Elimination.Application(argument) => free(argument.argument)
    }.sequence.map(_.toSet.flatten)

  def free(spine: Spine[Value]): Elaboration[Set[Level]] =
    spine.mapM(free).map(_.asList.flatMap(_.argument).toSet)

}


