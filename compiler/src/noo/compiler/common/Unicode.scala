package noo.compiler.common

import scala.annotation.switch

object Unicode {

  type CodePoint = Int

  def isCombiningCodePoint(codePoint: CodePoint): Boolean =
    (Character.getType(codePoint): @switch) match {
      case Character.ENCLOSING_MARK => true
      case Character.COMBINING_SPACING_MARK => true
      case Character.NON_SPACING_MARK => true
      case _ => false
    }

  def isSymbolCodePoint(codePoint: CodePoint): Boolean =
    (codePoint: @switch) match {
      case '\"' => false
      case '\'' => false
      case ',' => false
      case '.' => false
      case _ =>
        (Character.getType(codePoint): @switch) match {
          case Character.DASH_PUNCTUATION => true
          case Character.OTHER_PUNCTUATION => true
          case Character.CURRENCY_SYMBOL => true
          case Character.MODIFIER_SYMBOL => true
          case Character.MATH_SYMBOL => true
          case Character.OTHER_SYMBOL => true
          case _ => false
        }
    }

  def isSymbolOrCombiningCodePoint(codePoint: CodePoint): Boolean =
    isCombiningCodePoint(codePoint) || isSymbolCodePoint(codePoint)

  def isNumericCodePoint(codePoint: CodePoint): Boolean =
    (Character.getType(codePoint): @switch) match {
      case Character.DECIMAL_DIGIT_NUMBER => true
      case Character.OTHER_NUMBER => true
      case Character.LETTER_NUMBER => true
      case _ => false
    }

  def isAlphabeticalCodePoint(codePoint: CodePoint): Boolean =
    (Character.getType(codePoint): @switch) match {
      case Character.LOWERCASE_LETTER => true
      case Character.MODIFIER_LETTER => true
      case Character.OTHER_LETTER => true
      case Character.UPPERCASE_LETTER => true
      case Character.TITLECASE_LETTER => true
      case Character.CONNECTOR_PUNCTUATION => true
      case _ => false
    }

  def isAlphaNumericCodePoint(codePoint: CodePoint): Boolean =
    isNumericCodePoint(codePoint) || isAlphabeticalCodePoint(codePoint)

  def isAlphaNumericOrCombiningCodePoint(codePoint: CodePoint): Boolean =
    isCombiningCodePoint(codePoint) || isAlphaNumericCodePoint(codePoint)

  def isVowel(character: Char): Boolean = {
    // gratefully taken from https://stackoverflow.com/questions/26557604/whats-the-best-way-to-check-if-a-character-is-a-vowel-in-java
    (character: @switch) match {
      case 65 => true
      case 69 => true
      case 73 => true
      case 79 => true
      case 85 => true
      case 89 => true
      case 97 => true
      case 101 => true
      case 105 => true
      case 111 => true
      case 117 => true
      case 121 => true
      case 192 => true
      case 193 => true
      case 194 => true
      case 195 => true
      case 196 => true
      case 197 => true
      case 198 => true
      case 200 => true
      case 201 => true
      case 202 => true
      case 203 => true
      case 204 => true
      case 205 => true
      case 206 => true
      case 207 => true
      case 210 => true
      case 211 => true
      case 212 => true
      case 213 => true
      case 214 => true
      case 216 => true
      case 217 => true
      case 218 => true
      case 219 => true
      case 220 => true
      case 221 => true
      case 224 => true
      case 225 => true
      case 226 => true
      case 227 => true
      case 228 => true
      case 229 => true
      case 230 => true
      case 232 => true
      case 233 => true
      case 234 => true
      case 235 => true
      case 236 => true
      case 237 => true
      case 238 => true
      case 239 => true
      case 242 => true
      case 243 => true
      case 244 => true
      case 245 => true
      case 246 => true
      case 248 => true
      case 249 => true
      case 250 => true
      case 251 => true
      case 252 => true
      case 253 => true
      case 255 => true
      case 256 => true
      case 257 => true
      case 258 => true
      case 259 => true
      case 260 => true
      case 261 => true
      case 274 => true
      case 275 => true
      case 276 => true
      case 277 => true
      case 278 => true
      case 279 => true
      case 280 => true
      case 281 => true
      case 282 => true
      case 283 => true
      case 296 => true
      case 297 => true
      case 298 => true
      case 299 => true
      case 300 => true
      case 301 => true
      case 302 => true
      case 303 => true
      case 304 => true
      case 305 => true
      case 306 => true
      case 307 => true
      case 332 => true
      case 333 => true
      case 334 => true
      case 335 => true
      case 336 => true
      case 337 => true
      case 338 => true
      case 339 => true
      case 360 => true
      case 361 => true
      case 362 => true
      case 363 => true
      case 364 => true
      case 365 => true
      case 366 => true
      case 367 => true
      case 368 => true
      case 369 => true
      case 370 => true
      case 371 => true
      case 374 => true
      case 375 => true
      case 376 => true
      case 506 => true
      case 507 => true
      case 508 => true
      case 509 => true
      case 510 => true
      case 511 => true
      case 512 => true
      case 513 => true
      case 514 => true
      case 515 => true
      case 516 => true
      case 517 => true
      case 518 => true
      case 519 => true
      case 520 => true
      case 521 => true
      case 522 => true
      case 523 => true
      case 524 => true
      case 525 => true
      case 526 => true
      case 527 => true
      case 532 => true
      case 533 => true
      case 534 => true
      case 535 => true
      case _ =>
        (character: @switch) match {
          case 7680 => true
          case 7681 => true
          case 7700 => true
          case 7701 => true
          case 7702 => true
          case 7703 => true
          case 7704 => true
          case 7705 => true
          case 7706 => true
          case 7707 => true
          case 7708 => true
          case 7709 => true
          case 7724 => true
          case 7725 => true
          case 7726 => true
          case 7727 => true
          case 7756 => true
          case 7757 => true
          case 7758 => true
          case 7759 => true
          case 7760 => true
          case 7761 => true
          case 7762 => true
          case 7763 => true
          case 7794 => true
          case 7795 => true
          case 7796 => true
          case 7797 => true
          case 7798 => true
          case 7799 => true
          case 7800 => true
          case 7801 => true
          case 7802 => true
          case 7803 => true
          case 7833 => true
          case 7840 => true
          case 7841 => true
          case 7842 => true
          case 7843 => true
          case 7844 => true
          case 7845 => true
          case 7846 => true
          case 7847 => true
          case 7848 => true
          case 7849 => true
          case 7850 => true
          case 7851 => true
          case 7852 => true
          case 7853 => true
          case 7854 => true
          case 7855 => true
          case 7856 => true
          case 7857 => true
          case 7858 => true
          case 7859 => true
          case 7860 => true
          case 7861 => true
          case 7862 => true
          case 7863 => true
          case 7864 => true
          case 7865 => true
          case 7866 => true
          case 7867 => true
          case 7868 => true
          case 7869 => true
          case 7870 => true
          case 7871 => true
          case 7872 => true
          case 7873 => true
          case 7874 => true
          case 7875 => true
          case 7876 => true
          case 7877 => true
          case 7878 => true
          case 7879 => true
          case 7880 => true
          case 7881 => true
          case 7882 => true
          case 7883 => true
          case 7884 => true
          case 7885 => true
          case 7886 => true
          case 7887 => true
          case 7888 => true
          case 7889 => true
          case 7890 => true
          case 7891 => true
          case 7892 => true
          case 7893 => true
          case 7894 => true
          case 7895 => true
          case 7896 => true
          case 7897 => true
          case 7898 => true
          case 7899 => true
          case 7900 => true
          case 7901 => true
          case 7902 => true
          case 7903 => true
          case 7904 => true
          case 7905 => true
          case 7906 => true
          case 7907 => true
          case 7908 => true
          case 7909 => true
          case 7910 => true
          case 7911 => true
          case 7912 => true
          case 7913 => true
          case 7914 => true
          case 7915 => true
          case 7916 => true
          case 7917 => true
          case 7918 => true
          case 7919 => true
          case 7920 => true
          case 7921 => true
          case 7922 => true
          case 7923 => true
          case 7924 => true
          case 7925 => true
          case 7926 => true
          case 7927 => true
          case 7928 => true
          case 7929 => true
          case _ => false
        }
    }
  }

  def A(string: String): String =
    if (string.headOption.exists(isVowel)) s"An $string"
    else s"A $string"

  def a(string: String): String =
    if (string.headOption.exists(isVowel)) s"an $string"
    else s"a $string"

}

