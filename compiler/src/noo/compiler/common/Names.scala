package noo.compiler.common

object Names {

  val smallGreekLetters: Array[Char] = ('\u03B1' to '\u03C9').toArray

  val capitalGreekLetters: Array[Char] = ('\u0391' to '\u03A9').toArray

  val smallLatinLetters: Array[Char] = ('a' to 'z').toArray

  val capitalLatinLetters: Array[Char] = ('A' to 'Z').toArray

  val smallGreekNames: LazyList[String] = names(smallGreekLetters)

  val capitalGreekNames: LazyList[String] = names(capitalGreekLetters)

  val smallLatinNames: LazyList[String] = names(smallLatinLetters)

  val capitalLatinNames: LazyList[String] = names(capitalLatinLetters)

  def names(letters: Array[Char]): LazyList[String] = {
    val base = LazyList.from(letters).map(_.toString)

    def go(n: Int): LazyList[String] =
      base.map(_ + n).lazyAppendedAll(go(n + 1))

    base.lazyAppendedAll(go(0))
  }

  def distance(a: String, b: String): Int = {
    val distances = new Array[Int]((a.length + 1) * (b.length + 1))

    @inline
    def get(i: Int, j: Int): Int = distances(i * (b.length + 1) + j)

    @inline
    def set(i: Int, j: Int, value: Int): Unit = distances(i * (b.length + 1) + j) = value

    var i: Int = 1
    while (i < a.length + 1) {
      set(i, 0, i)
      i = i + 1
    }

    var j: Int = 1
    while (j < b.length + 1) {
      set(0, j, j)
      j = j + 1
    }

    j = 1
    while (j < b.length + 1) {
      i = 1
      while (i < a.length + 1) {
        val cost = if (a.charAt(i - 1) == b.charAt(j - 1)) 0 else 1
        set(i, j, (get(i - 1, j) + 1) min (get(i, j - 1) + 1) min (get(i - 1, j - 1) + cost))
        i = i + 1
      }
      j = j + 1
    }

    get(a.length, b.length)
  }

}
