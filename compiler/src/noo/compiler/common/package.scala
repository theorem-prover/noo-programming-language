package noo.compiler

import scala.collection.Factory
import scala.language.experimental.macros

package object common {

  def impossible(reason: String): Nothing = throw new RuntimeException(s"this should not have happened because $reason (please report this as a bug)")

  lazy val impossible: Nothing = throw new RuntimeException("this should not have happened (please report this as a bug)")

  implicit final class Tuple2Extension[+A, +B](val tuple: (A, B)) extends AnyVal {

    def bimap[C, D](f: A => C)(g: B => D): (C, D) =
      tuple match {
        case (a, b) => (f(a), g(b))
      }

  }

  implicit final class IterableExtension[+A, I[+X] <: Iterable[X]](val iterable: I[A]) extends AnyVal {

    def splitWhere[B >: A](f: A => Boolean)(implicit factory: Factory[B, I[B]]): (I[B], I[B]) = {
      val a = factory.newBuilder
      val b = factory.newBuilder

      var split = false
      for (element <- iterable) {
        split = split || f(element)
        (if (split) a else b) += element
      }

      (a.result(), b.result())
    }

  }

  implicit final class StringExtension(val string: String) extends AnyVal {

    def indentation(n: Int): String = {
      val indentation = " " * n
      string.split('\n').map(indentation + _).mkString("\n")
    }

  }

  trait ~>[F[_], G[_]] {

    def apply[A](x: F[A]): G[A]

  }

  object ~> {

    type Id[A] = A

  }

}
