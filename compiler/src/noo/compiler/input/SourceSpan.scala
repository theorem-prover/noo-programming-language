package noo.compiler.input

import noo.compiler.common.Unicode
import noo.compiler.common.Unicode.CodePoint

import scala.collection.mutable.ArrayBuffer

sealed trait SourceSpan {

  def position: SourcePosition

  def boundary: SourcePosition

  def source: InputSource

  def underline: Option[String]

}

object SourceSpan {

  case object Undefined extends SourceSpan {

    override val position: SourcePosition = SourcePosition.Undefined

    override val boundary: SourcePosition = SourcePosition.Undefined

    override val source: InputSource = InputSource.unknown

    val underline: Option[String] = None

  }

  case class Between(override val position: SourcePosition, override val boundary: SourcePosition) extends SourceSpan {

    override val source: InputSource = position.source

    def underline: Option[String] = Some {
      // 1. Find the beginning of the line
      var lineStartOffset = position.offset
      var startOfLineFound = false
      var nextIsCombiningCharacter = if (position.offset >= source.contents.length()) false else Unicode.isCombiningCodePoint(Character.codePointAt(source.contents, position.offset))

      while (!startOfLineFound && lineStartOffset > 0) {
        lineStartOffset -= 1

        if (Character.isLowSurrogate(source.contents.charAt(lineStartOffset)) && lineStartOffset > 0 && Character.isHighSurrogate(source.contents.charAt(lineStartOffset - 1))) {
          lineStartOffset -= 1
        }

        val codePoint: CodePoint = Character.codePointAt(source.contents, lineStartOffset)
        if (codePoint == '\n' && !nextIsCombiningCharacter) {
          startOfLineFound = true
          lineStartOffset += Character.charCount(codePoint)
        }

        nextIsCombiningCharacter = Unicode.isCombiningCodePoint(codePoint)
      }

      // 2. Find the end of the line
      var lineEndOffset = boundary.offset
      var endOfLineFound = false
      while (!endOfLineFound && lineEndOffset < source.contents.length()) {
        val codePoint: CodePoint = Character.codePointAt(source.contents, lineEndOffset)
        val charCount = Character.charCount(codePoint)
        lineEndOffset += charCount

        if (codePoint == '\n' && (lineEndOffset >= source.contents.length() || !Unicode.isCombiningCodePoint(Character.codePointAt(source.contents, lineEndOffset)))) {
          lineEndOffset -= charCount
          endOfLineFound = true
        }
      }

      // 3. Get the source text slice for this range
      val text = source.contents.subSequence(lineStartOffset, lineEndOffset)

      // 4. Get all lines with their corresponding underline
      val lines = ArrayBuffer(new StringBuilder)
      val underlines = ArrayBuffer(new StringBuilder)

      var i = 0
      var underline = false
      while (i < text.length()) {
        val cp = Character.codePointAt(text, i)
        i += Character.charCount(cp)

        if (cp == '\n' && (i >= text.length() || !Unicode.isCombiningCodePoint(Character.codePointAt(text, i)))) {
          // Line break
          lines += new StringBuilder
          underlines += new StringBuilder
          underline = false
        } else {
          lines.last ++= Character.toString(cp)
          if (i < position.offset - lineStartOffset) {
            if (Character.isWhitespace(cp) && (i >= text.length() || !Unicode.isCombiningCodePoint(Character.codePointAt(text, i))) && cp == '\t') {
              underlines.last += '\t'
            } else {
              underlines.last += ' '
            }
          } else if (i > boundary.offset - lineStartOffset) {
            underline = false
          } else if (Character.isWhitespace(cp) && (i >= text.length() || !Unicode.isCombiningCodePoint(Character.codePointAt(text, i)))) {
            // Should we underline this whitespace?
            if (underline && cp != '\t') {
              underlines.last += '~'
            } else {
              underlines.last ++= Character.toString(cp)
            }
          } else {
            underlines.last += '~'
            underline = true
          }
        }
      }

      // 5. Compute a source code view from all lines and underlines
      val result = new StringBuilder
      val maxLineNumberWidth = (position.line + (lines.length - 1)).toString.length

      i = 0
      while (lines.nonEmpty) {
        val indentation = String.format("%" + maxLineNumberWidth + "d | ", position.line + i)
        result ++= indentation
        result ++= lines.remove(0)
        result += '\n'
        result ++= " " * (indentation.length - 2)
        result ++= "| "
        result ++= underlines.remove(0)
        result += '\n'
        i += 1
      }

      result.toString
    }

  }

}
