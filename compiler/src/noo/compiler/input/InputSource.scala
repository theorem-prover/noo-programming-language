package noo.compiler.input

import noo.compiler.input.SourceText.SourceTextWindow

import java.io.{FileInputStream, InputStream}
import java.nio.charset.Charset
import java.nio.file.{Path, Paths}

sealed trait InputSource {

  /** A human-readable name for this input source. */
  def name: String

  /** The contents of this input source. */
  def contents: SourceText

  /** A human-readable description for this input source. */
  def description: String

}

object InputSource {

  val unknown: InputSource = from(Array.empty[Char], "unknown source", Some("unknown source"))

  def from(path: Path, charset: Charset, description: Option[String]): InputSource = {
    val wdir = Paths.get(".").toAbsolutePath.normalize()
    val abs = path.toAbsolutePath.normalize()
    val name = try wdir.relativize(abs).normalize().toString catch {
      case _: IllegalArgumentException => abs.toString
    }
    from(new FileInputStream(path.toFile), charset, name, description.orElse(Some(s"file '$name'")))
  }

  def from(stream: InputStream, charset: Charset, name: String, description: Option[String]): InputSource =
    from(stream.readAllBytes(), charset, name, description.orElse(Some(s"input stream '$name'")))

  def from(bytes: Array[Byte], charset: Charset, name: String, description: Option[String]): InputSource =
    from(new String(bytes, charset).toCharArray, name, description)

  def from(characters: Array[Char], name: String, description: Option[String]): InputSource =
    from(new SourceTextWindow(characters, 0, characters.length), name, description)

  def from(contents: SourceText, name: String, description: Option[String]): InputSource = {
    val _contents = contents
    val _name = name
    val _description = description.getOrElse(s"input source '${_name}'")
    new InputSource {
      override val name: String = _name
      override val contents: SourceText = _contents
      override val toString: String = s"InputSource(name := $name)"
      override val description: String = _description
    }
  }

}

