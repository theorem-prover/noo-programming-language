package noo.compiler.input

import scala.annotation.tailrec
import scala.collection.mutable

sealed abstract class SourceText extends CharSequence {

  def charAt(index: Int): Char

  def subSequence(start: Int, end: Int): SourceText

  def hashCode: Int

  def equals(any: Any): Boolean

  def toString: String

  def +(any: Any): SourceText

  def nonEmpty: Boolean

}

object SourceText {

  val empty: SourceText = new SourceTextWindow(new Array[Char](0), 0, 0)

  @tailrec
  def from(any: Any): SourceText = any match {
    case window: SourceTextWindow =>
      window

    case string: String =>
      intern(string)

    case sequence: CharSequence =>
      val underlying = new Array[Char](sequence.length())

      var i = 0
      while (i < underlying.length) {
        underlying(i) = sequence.charAt(i)
        i += 1
      }

      new SourceTextWindow(underlying, 0, underlying.length)

    case any =>
      from(any.toString)
  }

  private def cache: mutable.HashMap[String, SourceText] = mutable.HashMap.empty

  private def intern(string: String): SourceText = {
    val intern = string.intern()
    cache.getOrElseUpdate(intern, {
      val underlying = intern.toCharArray
      new SourceTextWindow(underlying, 0, underlying.length)
    })
  }

  private[input] final class SourceTextWindow(
    val underlying: Array[Char],
    val offset: Int,
    override val length: Int
  ) extends SourceText {

    override val nonEmpty: Boolean = length > 0

    override def charAt(index: Int): Char =
      if (index < 0 || index >= length) throw new IndexOutOfBoundsException(index)
      else underlying(index + offset)

    override def subSequence(start: Int, end: Int): SourceText =
      if (start < 0 || start > length) throw new IndexOutOfBoundsException(start)
      else if (end < start || end > length) throw new IndexOutOfBoundsException(end)
      else new SourceTextWindow(underlying, offset + start, end - start)

    override lazy val toString: String = new String(underlying, offset, length)

    override val hashCode: Int = {
      var result = 1
      var i = 0
      while (i < length) {
        result = result * 31 + underlying(offset + i)
        i += 1
      }
      result
    }

    override def equals(any: Any): Boolean =
      any match {
        case window: SourceTextWindow =>
          if (this eq window) {
            return true
          }

          if (hashCode != window.hashCode) {
            return false
          }

          if (length != window.length) {
            // (TODO) We assume that the underlying unicode data is normalized
            return false
          }

          if (underlying.eq(window.underlying) && offset == window.offset) {
            return true
          }

          // Test for character equality
          java.util.Arrays.equals(underlying, offset, offset + length, window.underlying, window.offset, window.offset + window.length)

        case concat: SourceTextConcat =>
          if (hashCode != concat.hashCode) {
            return false
          }

          if (length != concat.length) {
            // We assume that the underlying unicode data is normalized
            return false
          }

          var i = 0
          while (i < length) {
            if (underlying(i) != concat.charAt(i)) {
              return false
            }
            i += 1
          }

          true

        case sequence: CharSequence =>
          if (length != sequence.length()) {
            return false
          }

          var i = 0
          while (i < length) {
            if (underlying(i) != sequence.charAt(i)) {
              return false
            }
            i += 1
          }

          true

        case _ => false
      }

    override def +(any: Any): SourceText = any match {
      case window: SourceTextWindow =>
        if (underlying.eq(window.underlying) && offset + length == window.offset) {
          // The two windows are tightly packed in the underlying array
          new SourceTextWindow(underlying, offset, length + window.length)
        } else if (this.length + window.length <= 8192) {
          // The underlying arrays are small enough for many modern caches => copy them to a consecutive memory block
          val concat = new Array[Char](this.length + window.length)

          if (this.length <= 32) {
            // A copy loop is likely to be faster than the overhead of calling a native method
            var i = 0
            while (i < this.length) {
              concat(i) = this.underlying(i + this.offset)
              i += 1
            }
          } else {
            System.arraycopy(this.underlying, this.offset, concat, 0, this.length)
          }

          if (window.length <= 32) {
            // A copy loop is likely to be faster than the overhead of calling a native method
            var i = 0
            while (i < window.length) {
              concat(i + this.length) = window.underlying(i + window.offset)
              i += 1
            }
          } else {
            System.arraycopy(window.underlying, window.offset, concat, this.length, window.length)
          }

          new SourceTextWindow(concat, 0, concat.length)
        } else {
          // The underlying arrays are so big (> 16384 bytes) that most caches will not be able to hold them
          // entirely, so we can leave them at different memory locations
          new SourceTextConcat(this, window)
        }

      case concat: SourceTextConcat =>
        new SourceTextConcat(this + concat.a, concat.b)

      case _ =>
        this + from(any)
    }

  }

  private final class SourceTextConcat(private[SourceText] val a: SourceText, private[SourceText] val b: SourceText) extends SourceText {

    override val nonEmpty: Boolean = a.nonEmpty || b.nonEmpty

    override val length: Int = a.length() + b.length()

    override lazy val toString: String = a.toString + b.toString

    override val hashCode: Int = (a.hashCode() * 31) + b.hashCode()

    override def charAt(index: Int): Char = {
      if (index < 0 || index >= length) throw new IndexOutOfBoundsException(index)
      else if (index < a.length()) a.charAt(index)
      else b.charAt(index - a.length())
    }

    override def subSequence(start: Int, end: Int): SourceText =
      if (start < 0 || start > length) throw new IndexOutOfBoundsException(start)
      else if (end < start || end > length) throw new IndexOutOfBoundsException(end)
      else if (end <= a.length()) a.subSequence(start, end)
      else if (start >= a.length()) b.subSequence(start - a.length(), end - a.length())
      else a.subSequence(start, a.length()) + b.subSequence(0, end - a.length())

    @tailrec
    override def +(any: Any): SourceText = any match {
      case window: SourceTextWindow =>
        new SourceTextConcat(a, b + window)

      case concat: SourceTextConcat =>
        new SourceTextConcat(a, b + concat)

      case _ =>
        this + from(any)
    }

    override def equals(any: Any): Boolean =
      any match {
        case window: SourceTextWindow =>
          if (hashCode != window.hashCode) {
            return false
          }

          if (length != window.length) {
            // We assume that the underlying unicode data is normalized
            return false
          }

          a == window.subSequence(0, a.length()) && b == window.subSequence(a.length(), a.length() + b.length())

        case concat: SourceTextConcat =>
          if (hashCode != concat.hashCode) {
            return false
          }

          if (length != concat.length) {
            // We assume that the underlying unicode data is normalized
            return false
          }

          var i = 0
          while (i < length) {
            if (charAt(i) != concat.charAt(i)) {
              return false
            }
            i += 1
          }

          true

        case sequence: CharSequence =>
          if (length != sequence.length()) {
            return false
          }

          var i = 0
          while (i < length) {
            if (charAt(i) != sequence.charAt(i)) {
              return false
            }
            i += 1
          }

          true

        case _ => false
      }

  }

}
