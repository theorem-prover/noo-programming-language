package noo.compiler.input

sealed trait SourcePosition {

  def line: Int

  def character: Int

  def offset: Int

  def source: InputSource

  def isDefined: Boolean

  def description: String

}

object SourcePosition {

  case object Undefined extends SourcePosition {

    override val source: InputSource = InputSource.unknown

    override val line: Int = 1

    override val character: Int = 1

    override val offset: Int = 0

    override val isDefined: Boolean = false

    override val toString: String = "<undefined>"

    override val description: String = s"undefined position"

  }

  case class Location(
    override val line: Int,
    override val character: Int,
    override val source: InputSource,
    override val offset: Int
  ) extends SourcePosition {

    override val isDefined: Boolean = true

    override val toString: String = s"${source.name} @ $line:$character"

    override val description: String = s"line $line and character $character of ${source.description}"

  }

}

