package noo.compiler.modifier

sealed trait Polarity extends Modifier {

  def isPositive: Boolean

  def isNegative: Boolean

}

object Polarity {

  case object Negative extends Polarity {

    override val isNegative: Boolean = true

    override val isPositive: Boolean = false

    override val toString: String = "negative"

  }

  case object Positive extends Polarity {

    override val isNegative: Boolean = false

    override val isPositive: Boolean = true

    override val toString: String = "positive"

  }

}
