package noo.compiler.modifier

import scala.reflect.ClassTag

trait Modifier extends Product with Serializable {

  def toString: String

}

object Modifier {

  def of[M <: Modifier](modifiers: List[Modifier])(implicit tag: ClassTag[M]): Option[M] =
    modifiers.collectFirst { case tag(modifier) => modifier }

}
