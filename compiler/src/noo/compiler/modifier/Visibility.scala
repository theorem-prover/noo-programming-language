package noo.compiler.modifier

import noo.compiler.syntax.common.Qualifier

sealed trait Visibility extends Modifier

object Visibility {

  case class Private(scope: Qualifier) extends Visibility {

    override val toString: String = s"private(${scope.toString})"

  }

  case object Public extends Visibility {

    override val toString: String = "public"

  }

}
