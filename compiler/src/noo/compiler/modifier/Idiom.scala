package noo.compiler.modifier

sealed trait Idiom extends Modifier {

  def isImplicit: Boolean

  def isExplicit: Boolean

  def toString: String

}

object Idiom {

  case object Explicit extends Idiom {

    override val isImplicit: Boolean = false

    override val isExplicit: Boolean = true

    override val toString: String = "explicit"

  }

  case object Implicit extends Idiom {

    override val isImplicit: Boolean = true

    override val isExplicit: Boolean = false

    override val toString: String = "implicit"

  }

}
