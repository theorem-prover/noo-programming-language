package noo.compiler.elaboration

import noo.compiler.elaboration.context.{Binding, Context, Environment, Visibility}
import noo.compiler.error.Error
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.input.SourceSpan
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.core.{Declaration, Namespace, Term}

import scala.collection.Factory

@FunctionalInterface
trait Elaboration[@specialized +A] extends (Context => Either[Error, A]) {

  def map[@specialized B](f: A => B): Elaboration[B] = (context: Context) => this(context).map(f)

  def flatMap[@specialized B](f: A => Elaboration[B]): Elaboration[B] = (context: Context) => this(context) match {
    case Left(error) => Left(error)
    case Right(value) => f(value)(context)
  }

  def elaborate(context: Context): Either[Error, A] =
    try apply(context) catch {
      case throwable: Throwable =>
        // For better error reporting we hide all stack trace elements of this class and its companion object
        val stackTrace = throwable.getStackTrace.filterNot(_.getClassName.startsWith("noo.compiler.elaboration.Elaboration"))
        throwable.setStackTrace(stackTrace)
        throw throwable
    }

  lazy val void: Elaboration[Unit] = map(_ => ())

  def withFilter(p: A => Boolean): Elaboration[A] = (context: Context) => this(context) match {
    case Right(value) if p(value) => Right(value)
    case Right(value) =>
      throw new RuntimeException(s"non-exhaustive pattern for $value")
    case error => error
  }

}

object Elaboration {

  def pure[@specialized A](value: A): Elaboration[A] = {
    val result = Right(value)
    (_: Context) => result
  }

  def fail(error: Error): Elaboration[Nothing] = {
    val result = Left(error)
    (_: Context) => result
  }

  def fail(f: SourceSpan => Error): Elaboration[Nothing] =
    context.flatMap(context => fail(f(context.location)))

  def apply[@specialized A](value: => A): Elaboration[A] = (_: Context) => Right(value)

  val unit: Elaboration[Unit] = pure(())

  val context: Elaboration[Context] = (context: Context) => Right(context)

  val environment: Elaboration[Environment] = context.map(_.environment)

  val namespace: Elaboration[Declaration with Namespace] = context.map(_.namespace)

  def modify[@specialized A](f: Context => Context)(continuation: => Elaboration[A]): Elaboration[A] = {
    lazy val memoized = continuation
    (context: Context) => memoized(f(context))
  }

  def withContext[@specialized A](context: Context)(continuation: => Elaboration[A]): Elaboration[A] = {
    lazy val memoized = continuation
    (_: Context) => memoized(context)
  }

  def withEnvironment[@specialized A](environment: Environment)(continuation: => Elaboration[A]): Elaboration[A] =
    modify(_.modify(_ => environment))(continuation)

  def withNamespace[@specialized A](namespace: Declaration with Namespace)(continuation: => Elaboration[A]): Elaboration[A] = {
    lazy val memoized = continuation
    (context: Context) => memoized(context at namespace)
  }

  def when(condition: Boolean)(continuation: => Elaboration[Unit]): Elaboration[Unit] =
    if (condition) continuation else unit

  def unless(condition: Boolean)(continuation: => Elaboration[Unit]): Elaboration[Unit] =
    if (condition) unit else continuation

  def bind[@specialized A](name: String, signature: Type, idiom: Idiom, visibility: Visibility)(continuation: Binding => Elaboration[A]): Elaboration[A] =
    (context: Context) => {
      val binding = Binding.Abstraction(name, context.environment.level, signature, idiom, visibility)
      continuation(binding)(context.modify(_.bind(binding)))
    }

  def define[@specialized A](name: String, signature: Type, value: Value, visibility: Visibility)(continuation: Binding => Elaboration[A]): Elaboration[A] =
    (context: Context) => {
      val binding = Binding.Definition(name, context.environment.level, signature, value, visibility)
      continuation(binding)(context.modify(_.bind(binding)))
    }

  def at[A](span: SourceSpan)(continuation: => Elaboration[A]): Elaboration[A] =
    modify(_ at span)(continuation)

  def fresh(signature: Type): Elaboration[Term] =
    for {
      context <- Elaboration.context
      variable <- context.fresh(signature)
      result <- context.environment.apply(Term.Variable.Flexible(variable))
    } yield result

  def foldl[A](signature: Type)(identity: A)(f: (Binding, A) => Elaboration[A]): Elaboration[A] =
    Conversion.force(signature, unfold = true).flatMap {
      case Value.Pi(parameter, head, body, idiom) =>
        Elaboration.bind(parameter, head, idiom, Visibility.Hidden) { binder =>
          f(binder, identity).flatMap(identity => body(binder.value).flatMap(foldl(_)(identity)(f)))
        }

      case _ =>
        Elaboration.pure(identity)
    }

  implicit final class OptionExtension1[A](val option: Option[Elaboration[A]]) extends AnyVal {

    def sequence: Elaboration[Option[A]] = option match {
      case Some(elaboration) => elaboration.map(Some(_))
      case None => Elaboration.pure(None)
    }

  }

  implicit final class IterableExtension1[I[X] <: IterableOnce[X], A](val iterable: I[Elaboration[A]]) extends AnyVal {

    def sequence(implicit factory: Factory[A, I[A]]): Elaboration[I[A]] = { context: Context =>
      val builder = factory.newBuilder
      val iterator = iterable.iterator
      var failure: Error = null

      while (iterator.hasNext && failure.eq(null)) {
        val elaboration = iterator.next()
        elaboration.elaborate(context) match {
          case Left(error) => failure = error
          case Right(value) => builder += value
        }
      }

      if (failure eq null) Right(builder.result())
      else Left(failure)
    }

  }

  implicit final class OptionExtension2[A](val option: Option[A]) extends AnyVal {

    def mapM[B](f: A => Elaboration[B]): Elaboration[Option[B]] =
      option.map(f).sequence

  }

  implicit final class IterableExtension2[I[X] <: IterableOnce[X], A](val iterable: I[A]) extends AnyVal  {

    def mapM[B](f: A => Elaboration[B])(implicit factory: Factory[B, I[B]]): Elaboration[I[B]] =
      iterable.iterator.map(f).sequence.map(_.to(factory))

  }

}
