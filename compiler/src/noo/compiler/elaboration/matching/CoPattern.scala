package noo.compiler.elaboration.matching

import noo.compiler.elaboration.Elaboration
import noo.compiler.error.{IllegalPattern, IncompleteProjectionPattern}
import noo.compiler.input.SourceSpan
import noo.compiler.modifier.Polarity
import noo.compiler.syntax.common.{Argument, Elimination, Spine}
import noo.compiler.syntax.core.Namespace.Entry
import noo.compiler.syntax.core.{Declaration, Signature}
import noo.compiler.syntax.surface.Expression

/**
 * The internal representation of a partially decomposed (co-) pattern that is not yet fully elaborated. It consists of
 * a sequence of eliminations (which are either projections or applications), a sequence of constraints and a sequence
 * of introductions. A pattern counts as "matching" only when all eliminations are elaborated and all constraints are
 * solved. The introductions indicate the pattern variables that are available to the user.
 *
 * @param eliminations The list of eliminations of this pattern.
 * @param constraints The list of constraints of this pattern.
 * @param introductions The list of introductions.
 * @param span The source code span that ranges over the whole pattern.
 */
case class CoPattern(
  eliminations: List[Elimination[Expression]],
  constraints: List[Constraint],
  introductions: List[Introduction],
  span: SourceSpan
) {

  /**
   * Whether this pattern matches or not. That is, whether all eliminations are elaborated and all constraints have
   * been solved.
   */
  val matches: Boolean = eliminations.isEmpty && constraints.isEmpty

}

object CoPattern {

  /**
   * Decomposes the provided expression into a partially decomposed (co-) pattern and inserts any missing implicit
   * application patterns (except for trailing ones).
   * @param expression The expression that shall be decomposed.
   * @param definition The definition this pattern belongs to.
   * @return The partially decomposed pattern.
   */
  def from(expression: Expression, definition: Declaration with Signature): Elaboration[CoPattern] =
    for {
      (declaration, arguments) <- decompose(expression)
      pattern <- declaration match {
        case projection: Declaration.Clause.Projection =>
          // To simplify things, projections (and constructors) are not allowed to match on their family parameters.
          // From a theoretical perspective this is even cleaner, since a family parameter actually means, that the
          // whole family is parameterized. So, for example, we do not talk about a type `Stream A : Type` and a
          // constructor `head { A } : A` but instead, we consider these to be duplicated for each `A` (i.e., we
          // consider them as `Stream_A : Type` and `head_A : A` instead). Moreover, the parameters are contextually
          // known anyway. That is, when we match on the projection `head` we already know the type of the stream, and
          // therefore we also know the parameter `A`. In summary, we expect the first pattern that follows this
          // projection here to be the projector pattern (i.e., the next co-pattern we must decompose).

          if (arguments.isEmpty) {
            // The user did not supply enough arguments for the co-pattern to be complete (i.e., the projector is missing).
            Elaboration.fail(IncompleteProjectionPattern(projection) _)
          } else {
            // Here, we decompose the new co-pattern and append this projection with its remaining eliminations to the
            // sequence of all eliminations. In this way, we achieve that all eliminations are in the right order (i.e.,
            // first comes the innermost projection).
            val (Spine(Argument(copattern, _)), tail) = arguments.split(1)
            from(copattern, definition).map { case CoPattern(eliminations, constraints, variables, span) =>
              CoPattern(eliminations ++ (Elimination.Projection(projection) :: tail.toList.map(Elimination.Application(_))), constraints, variables, span)
            }
          }

        case declaration if declaration == definition =>
          // At this point we know that we hit the definition. Since we do not treat the definition itself as a projection
          // we just have to return the sequence of application patterns here.
          Elaboration.pure(CoPattern(arguments.toList.map(Elimination.Application(_)), Nil, Nil, expression.getSourceSpan.getOrElse(SourceSpan.Undefined)))

        case declaration =>
          Elaboration.fail(IllegalPattern(declaration.identifier, Polarity.Negative) _)
      }
    } yield pattern

  /**
   * Decomposes the provided expression into an application pattern, where the argument patterns are collected as a
   * spine. Additionally, any unspecified implicit arguments are inserted as wildcard patterns.
   * @param expression The expression that shall be decomposed.
   * @return The decomposed application consisting of the argument patterns and either the definition or a projection.
   */
  private def decompose(expression: Expression): Elaboration[(Declaration with Signature, Spine[Expression])] =
    expression match {
      case Expression.Position(expression, span) =>
        Elaboration.at(span)(decompose(expression))

      case Expression.Variable(qualifier) =>
        Elaboration.namespace.flatMap(_.resolveOrFail(qualifier)).flatMap {
          case Entry(declaration: Declaration with Signature) =>
            Elaboration.pure((declaration, Spine.empty))
          case Entry(declaration) =>
            Elaboration.fail(IllegalPattern(declaration.identifier, Polarity.Negative) _)
        }

      case Expression.Application(function, argument) =>
        for {
          (declaration, arguments) <- decompose(function)
          arguments <- Pattern.apply(declaration, arguments, argument)
        } yield (declaration, arguments)

      case _ =>
        Elaboration.fail(IllegalPattern(expression.kind, Polarity.Negative) _)
    }

}
