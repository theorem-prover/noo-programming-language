package noo.compiler.elaboration.matching

import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Level, Value}

sealed trait Introduction {

  def name: String

  def signature: Type

}

object Introduction {

  case class Synonym(level: Level, name: String, signature: Type) extends Introduction

  case class Definition(value: Value.Application, name: String, signature: Type) extends Introduction

}
