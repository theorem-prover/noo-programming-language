package noo.compiler.elaboration.matching

import noo.compiler.common.~>
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.Elaboration.OptionExtension2
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.elaboration.context.{Binding, Environment}
import noo.compiler.elaboration.matching.tactic.Split
import noo.compiler.error.{RecursiveSolution, TypeMismatch}
import noo.compiler.evaluation._
import noo.compiler.syntax.common.Spine.:+
import noo.compiler.syntax.common.{Argument, Spine}
import noo.compiler.syntax.core.{Declaration, PrettyPrinter}

/**
 * This object provides a left-hand side unification by equating bound variables using
 * the injectivity of constructors and by solving reflexive equations. This is incompatible
 * with HoTT since it allows the definition of the Uniqueness of Identity Proofs (UIP) and
 * therefore also axiom K.
 */
object Equation {

  /**
   * Try to unify the two spines by equating their bound variables in the current context. The order of the two values
   * indicates which one is expected in the current context and which one is the actual one, the elaboration encountered.
   *
   * @param xs The first spine that shall be unified.
   * @param ys The second spine that shall be unified.
   * @param expected The value that is the origin of the first spine and which should be used for error reporting.
   * @param actual The value that is the origin of the second spine and which should be used for error reporting.
   * @param justification The justification why the first value is the expected one.
   * @return On positive success this method returns an updated context and the set of bound variables that were equated.
   *         On negative success `None` is returned (i.e., when there is mismatch). When the two types cannot be unified
   *         (because the algorithm cannot tell if they are unifiable or not) a type mismatch is triggered just like in
   *         right-hand side unification.
   */
  def equate[A](xs: Spine[Value], ys: Spine[Value], expected: Value, actual: Value, justification: String)(continuation: (Substitution, Seq ~> Seq) => Elaboration[A]): Elaboration[Option[A]] =
    for {
      context <- Elaboration.context
      result <- equate(xs, ys, expected, actual, justification, context.environment)
      result <- result.mapM { case (environment, a) =>
        order(environment).flatMap { case (environment, order, b) =>
          for {
            substitution <- a.compose(b)
            result <- Elaboration.withContext(context.modify(_ => environment)) {
              continuation(substitution, order)
            }
          } yield result
        }
      }
    } yield result

  private def equate(expected: Value, actual: Value, justification: String, environment: Environment): Elaboration[Option[(Environment, Substitution)]] =
    for {
      expected <- Conversion.force(expected, unfold = true)
      actual <- Conversion.force(actual, unfold = true)
      equal <- equal(expected, actual)
      result <- (expected, actual) match {
        case _ if equal =>
          // This is the reflexivity rule, which is incompatible with HoTT since it allows the definition of UIP (and hence axiom K).
          // When we disallow this case, we run in a recursive solution in the below cases (which then should yield an error).
          Elaboration.pure(Some((environment, Substitution.empty)))

        case (Value.Application.Global.Declaration(c1: Declaration.Clause.Constructor, xs), Value.Application.Global.Declaration(c2: Declaration.Clause.Constructor, ys)) if c1 == c2 =>
          // This is the injectivity rule. Since we know that constructors are injective, we can equate their arguments here.
          equate(xs, ys, expected, actual, justification, environment)

        case (Value.Application.Global.Declaration(_: Declaration.Clause.Constructor, _), Value.Application.Global.Declaration(_: Declaration.Clause.Constructor, _)) =>
          // This is a constructor mismatch, so we know that both values cannot be equal.
          Elaboration.pure(None)

        case (Value.Application.Local(level, spine), actual) if environment.get(level).isDefinition =>
          for {
            expected <- Conversion.apply(environment.valueByLevel(level), spine)
            result <- equate(expected, actual, justification, environment)
          } yield result

        case (Value.Application.Local(level, Spine.empty), actual) =>
          solve(level, actual, environment).map(Some(_))

        case (expected, Value.Application.Local(level, spine)) if environment.get(level).isDefinition =>
          for {
            actual <- Conversion.apply(environment.valueByLevel(level), spine)
            result <- equate(expected, actual, justification, environment)
          } yield result

        case (expected, Value.Application.Local(level, Spine.empty)) =>
          solve(level, expected, environment).map(Some(_))

        case (expected, actual) =>
          Elaboration.modify(_.modify(_ => environment)) {
            for {
              expected <- PrettyPrinter.print(expected)
              actual <- PrettyPrinter.print(actual)
              result <- Elaboration.fail(TypeMismatch(expected, actual, Some(justification)) _)
            } yield result
          }
      }
    } yield result

  private def solve(level: Level, value: Value, environment: Environment): Elaboration[(Environment, Substitution)] =
    for {
      _ <- checkRecursiveOccurrence(level, value)(environment)
      (prefix, Binding.Abstraction(name, _, signature, _, origin) +: suffix) = environment.bindings.splitAt(level.value)
      substitution = Substitution(level -> value)
      prefix <- prefix.map(Split.refine(_, substitution)).sequence
      suffix <- suffix.map(Split.refine(_, substitution)).sequence
      bindings = prefix ++ (Binding.Definition(name, level, signature, value, origin) +: suffix)
    } yield (Environment.from(bindings), substitution)

  private def equate(xs: Spine[Value], ys: Spine[Value], expected: Value, actual: Value, justification: String, environment: Environment): Elaboration[Option[(Environment, Substitution)]] =
    (xs, ys) match {
      case (xs :+ Argument(x, _), ys :+ Argument(y, _)) =>
        for {
          // We check if the initial part of the spines can be equated ...
          result <- equate(xs, ys, expected, actual, justification, environment)
          // ... and if the last elements can be equated
          context <- result match {
            case Some((environment, a)) =>
              equate(x, y, justification, environment).flatMap(_.mapM {
                case (environment, b) => a.compose(b).map((environment, _))
              })
            case None =>
              // The spines already mismatch, so we drop the result of the below unification attempt.
              // Nevertheless, we still check if we could equate `x` and `y` to detect real type mismatches.
              equate(x, y, justification, environment).map(_ => None)
          }
        } yield context

      case (Spine.empty, Spine.empty) =>
        Elaboration.pure(Some((environment, Substitution.empty)))

      case _ =>
        // The arity of the spines does not match, so we do not know if the two values are equal and report an error.
        for {
          expected <- PrettyPrinter.print(expected)
          actual <- PrettyPrinter.print(actual)
          result <- Elaboration.fail(TypeMismatch(expected, actual, Some(justification)) _)
        } yield result
    }

  private def equal(x: Value, y: Value): Elaboration[Boolean] =
    for {
      x <- Conversion.force(x, unfold = true)
      y <- Conversion.force(y, unfold = true)
      result <- (x, y) match {
        case (Value.Type, Value.Type) =>
          Elaboration.pure(true)

        case (Value.Application.Local(x, xs), Value.Application.Local(y, ys)) if x == y =>
          equal(xs, ys)

        case (Value.Application.Flexible(x, xs), Value.Application.Flexible(y, ys)) if x == y =>
          equal(xs, ys)

        case (Value.Application.Global.Declaration(x, xs), Value.Application.Global.Declaration(y, ys)) if x == y =>
          equal(xs, ys)

        case (Value.Application.Global.Definition(x, xs), Value.Application.Global.Definition(y, ys)) if x == y =>
          equal(xs, ys)

        case (Value.Lambda(parameter, xDomain, xBody, xIdiom), Value.Lambda(_, yDomain, yBody, yIdiom)) if xIdiom == yIdiom =>
          equal(xDomain, yDomain).flatMap {
            case true =>
              Elaboration.bind(parameter, xDomain, xIdiom, Visible) { binder =>
                for {
                  xBody <- xBody(binder.value)
                  yBody <- yBody(binder.value)
                  result <- equal(xBody, yBody)
                } yield result
              }

            case false =>
              Elaboration.pure(false)
          }

        case (Value.Pi(parameter, xDomain, xBody, xIdiom), Value.Pi(_, yDomain, yBody, yIdiom)) if xIdiom == yIdiom =>
          equal(xDomain, yDomain).flatMap {
            case true =>
              Elaboration.bind(parameter, xDomain, xIdiom, Visible) { binder =>
                for {
                  xBody <- xBody(binder.value)
                  yBody <- yBody(binder.value)
                  result <- equal(xBody, yBody)
                } yield result
              }

            case false =>
              Elaboration.pure(false)
          }

        case _ =>
          Elaboration.pure(false)
      }
    } yield result

  private def equal(xs: Spine[Value], ys: Spine[Value]): Elaboration[Boolean] = (xs, ys) match {
    case (xs :+ Argument(x, _), ys :+ Argument(y, _)) =>
      for {
        result <- equal(xs, ys)
        result <- if (result) equal(x, y) else Elaboration.pure(false)
      } yield result

    case (Spine.empty, Spine.empty) =>
      Elaboration.pure(true)

    case _ =>
      Elaboration.pure(false)
  }

  private def order(environment: Environment): Elaboration[(Environment, Seq ~> Seq, Substitution)] =
    for {
      // First we pair each binder with its "free" variables (i.e., the variables that are bound in this environment)
      bindings <- environment.bindings.map {
        case binder @ Binding.Definition(_, _, signature, value, _) =>
          for {
            xs <- Value.free(signature)
            ys <- Value.free(value)
          } yield (binder, xs ++ ys)

        case binder @ Binding.Abstraction(_, _, signature, _, _) =>
          Value.free(signature).map((binder, _))
      }.sequence
      // Now we can sort these binders according to their dependencies. That is, a binder `a` precedes another binder `b`
      // if `a` is used in the signature of `b` or, in case of `b` being a definition, if `a` is used in the value of `b`.
      sorted = bindings.sortWith {
        case ((a, _), (b, _)) if a.level == b.level =>
          // The two binders are equal, so `a` does not precede `b`
          false
        case ((a, _), (_, dependencies)) if dependencies.contains(a.level) =>
          // The second binder depends on the first binder, so `a` precedes `b`
          true
        case ((_, dependencies), (b, _)) if dependencies.contains(b.level) =>
          // The first binder depends on the second binder, so `b` precedes `a`
          false
        case _ =>
          // The two binders are not related in any way, so `a` does not precede `b` (this should leave the order untouched)
          false
      }.map(_._1)
      // On top of the sorted bindings we can now create a substitution that reassigns the levels according to the new order.
      substitution = sorted.zipWithIndex.foldLeft(Substitution.empty) {
        case (substitution, (binder, level)) if binder.level.value == level =>
          // The level for this binder did not change, so we do not have to add it to the substitution
          substitution
        case (substitution, (binder, level)) =>
          substitution + (binder.level -> Value.Application.Local(level, Spine.empty))
      }
      // We call order during evaluation where we face a dynamic environment that is larger than the static environment here.
      // That is, during evaluation we face a runtime stack where we are only interested in the topmost element (the last
      // stack frame). This frame corresponds to the currently called definition, whose pattern match shall be unfolded.
      frameSize = environment.size
      order = new ~>[Seq, Seq] {
        override def apply[A](stack: Seq[A]): Seq[A] = {
          val (remaining, frame) = stack.splitAt(stack.size - frameSize)
          val array = new Array[Any](frame.size)

          var insert = 0
          for (binding <- sorted) {
            array(insert) = frame(binding.level.value)
            insert += 1
          }

          remaining ++ array.asInstanceOf[Array[A]]
        }
      }: Seq ~> Seq
      // Using the above substitution we can now update all binders
      bindings <- sorted.zipWithIndex.map {
        case (Binding.Abstraction(name, _, signature, idiom, origin), level) =>
          for {
            signature <- substitution(signature)
          } yield Binding.Abstraction(name, level, signature, idiom, origin)
        case (Binding.Definition(name, _, signature, value, origin), level) =>
          for {
            signature <- substitution(signature)
            value <- substitution(value)
          } yield Binding.Definition(name, level, signature, value, origin)
      }.sequence
    } yield (Environment.from(bindings), order, substitution)

  private def checkRecursiveOccurrence(level: Level, value: Value)(implicit environment: Environment): Elaboration[Unit] = Conversion.force(value, unfold = true).flatMap {
    case Value.Application.Global.Declaration(_, spine) =>
      checkRecursiveOccurrence(level, spine)

    case Value.Application.Global.Definition(_, spine) =>
      checkRecursiveOccurrence(level, spine)

    case Value.Application.Local(other, _) if other == level =>
      Elaboration.fail(RecursiveSolution(environment.get(level).name) _)

    case Value.Application.Local(_, spine) =>
      checkRecursiveOccurrence(level, spine)

    case Value.Application.Flexible(_, spine) =>
      checkRecursiveOccurrence(level, spine)

    case Value.Lambda(name, domain, body, idiom) =>
      Elaboration.bind(name, domain, idiom, Visible) {
        binding => body(binding.value).flatMap(checkRecursiveOccurrence(level, _))
      }

    case Value.Pi(name, head, body, idiom) =>
      for {
        _ <- checkRecursiveOccurrence(level, head)
        _ <- Elaboration.bind(name, head, idiom, Visible) {
          binding => body(binding.value).flatMap(checkRecursiveOccurrence(level, _))
        }
      } yield ()

    case _ =>
      Elaboration.unit
  }

  private def checkRecursiveOccurrence(level: Level, spine: Spine[Value])(implicit environment: Environment): Elaboration[Unit] =
    spine.mapM(checkRecursiveOccurrence(level, _)).void

}
