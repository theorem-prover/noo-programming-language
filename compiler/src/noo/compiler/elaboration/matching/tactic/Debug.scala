package noo.compiler.elaboration.matching.tactic
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.matching.{CoPattern, Configuration, Constraint, Introduction, Pattern}
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.{Argument, Clause, Elimination, Qualifier}
import noo.compiler.syntax.core.{CaseTree, PrettyPrinter}
import noo.compiler.syntax.surface.Expression

/**
 * A tactic for debugging purposes. This tactic does never solve anything and always returns `None`, but it prints
 * debugging information such as the context and the target.
 */
object Debug extends Tactic {

    override def apply(configuration: Configuration): Elaboration[Option[CaseTree]] =
      for {
        context <- Elaboration.context
        entries <- context.environment.show.map(_.split('\n').toList)
        target <- PrettyPrinter.print(configuration.target)
        max = (target :: entries).map(_.length).max
        _ = println()
        _ = entries.foreach(entry => println("  " + entry))
        _ = println("  " + "-" * max)
        _ = println("  " + target + "\n")
      clauses <- configuration.clauses.map {
        case Clause(CoPattern(eliminations, constraints, introductions, _), _) =>
          for {
            constraints <- constraints.map(print).sequence
            introductions <- introductions.map(print).sequence
            pattern = noo.compiler.syntax.surface.PrettyPrinter.print(eliminations.map {
              case Elimination.Application(argument) => argument
              case Elimination.Projection(projection) => Argument.Default(Expression.Variable(Qualifier("." + projection.qualifier.last.get)), Idiom.Explicit)
            }.foldLeft[Expression](Expression.Variable(Qualifier("")))(Expression.Application))
          } yield s"case$pattern [" + (constraints ++ introductions).mkString(", ") + "] =>"
      }.sequence
      _ = println("  " + clauses.mkString("\n  "))
      _ = println()
    } yield None

  private def print(introduction: Introduction): Elaboration[String] =
    Elaboration.context.flatMap { context =>
      introduction match {
        case Introduction.Synonym(level, name, signature) =>
          for {
            signature <- PrettyPrinter.print(signature)
            binderName = context.environment.get(level).name
          } yield s"$binderName / $name : $signature"
        case Introduction.Definition(value, name, signature) =>
          for {
            signature <- PrettyPrinter.print(signature)
            value <- PrettyPrinter.print(value)
          } yield s"$value / $name : $signature"
      }
    }

  private def print(constraint: Constraint): Elaboration[String] =
    Elaboration.context.flatMap { context =>
      constraint match {
        case Constraint.Simple(level, pattern, signature) =>
          PrettyPrinter.print(signature).map { signature =>
            s"${context.environment.get(level).name} /? ${print(pattern)} : $signature"
          }
        case Constraint.Complex(value, pattern, signature) =>
          for {
            signature <- PrettyPrinter.print(signature)
            value <- PrettyPrinter.print(value)
          } yield s"$value /? ${print(pattern)} : $signature"
      }
    }

  private def print(pattern: Pattern): String = pattern match {
    case Pattern.Wildcard(_) => "_"
    case Pattern.Variable(name, _) => name
    case Pattern.Application(constructor, arguments, _) =>
      noo.compiler.syntax.surface.PrettyPrinter.print(arguments.foldLeft[Expression](Expression.Variable(constructor.qualifier))(Expression.Application))
  }

}
