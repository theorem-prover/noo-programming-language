package noo.compiler.elaboration.matching.tactic
import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.matching.{CoPattern, Configuration}
import noo.compiler.error.{IllegalApplicationPattern, TypeMismatch}
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.syntax.common.{Argument, Clause, Elimination, Spine}
import noo.compiler.syntax.core.{CaseTree, Declaration, PrettyPrinter}

object CoSplit extends Tactic {

  override def apply(configuration: Configuration): Elaboration[Option[CaseTree]] = {
    Conversion.force(configuration.target, unfold = true).flatMap {
      case Value.Application.Global.Declaration(family: Declaration.Family, arguments)
        // Is the target type a negative family and does the first clause start with a projection elimination?
        if family.polarity.isNegative && configuration.clauses.headOption.exists(_.head.eliminations.headOption.exists(_.isProjection)) =>
        // This type is already well-typed, which means that it is fully applied to all of its arguments and especially
        // its family parameters.
        if (Tactic.debug) println("[DEBUG] Apply tactic 'cosplit'.")
        cosplit(arguments.take(family.parameters.size), family.clauses.map(_.asInstanceOf[Declaration.Clause.Projection]), CaseTree.Record(Nil), configuration).map(Some(_))

      case _ =>
        // A cosplit is not possible, since the target type is not a negative family.
        Elaboration.pure(None)
    }
  }

  private def cosplit(
    parameters: Spine[Value],
    projections: List[Declaration.Clause.Projection],
    projector: CaseTree.Record,
    configuration: Configuration
  ): Elaboration[CaseTree.Record] = projections match {
    case projection :: projections =>
      for {
        // First, we instantiate the projection signature to the family's parameters (these cannot be matched).
        signature <- Conversion.instantiate(projection.signature, parameters)
        // We know that a projection must accept its projector as the next argument (this is ensured as part of the
        // elaboration of negative families).
        (projectorType, body) <- Conversion.force(signature, unfold = true).map {
          case Value.Pi(_, projectorType, body, _) => (projectorType, body)
          case _ => impossible
        }
        // To obtain the new target we can instantiate the above dependent function type with the current projector value.
        target <- body(configuration.leftHandSide)
        // Now we can filter out all clauses that do not start with this projection (as they can never match).
        clauses <- configuration.clauses.map(_.mapM {
          case CoPattern(Elimination.Projection(other) :: eliminations, constraints, variables, span) if other == projection =>
            // This is a match, so we return this clause but remove the projection from its list of eliminations
            Elaboration.pure(Some(CoPattern(eliminations, constraints, variables, span)))

          case CoPattern((_: Elimination.Projection) :: _, _, _, _) =>
            // This is a mismatch, so we return `None` to filter out this clause
            Elaboration.pure(None)

          case CoPattern(Elimination.Application(Argument(pattern, _)) :: _, _, _, span) =>
            // This is an error, since we expect an elimination that can handle the projector type (which can only be
            // a projection by definition). This happens when the user tries to apply a pattern such as `const n : Stream Nat`
            // to a pattern variable `x`, for example, instead of one of the two projections `head` or `tail` (i.e., consider
            // the two patterns `case head (const n) => ...` and `case const n x => ...`).
            for {
              projectorType <- PrettyPrinter.print(projectorType)
              result <- Elaboration.fail(IllegalApplicationPattern(s"a value of type `$projectorType`", pattern)(span))
            } yield result

          case CoPattern(Nil, _, _, span) =>
            // This pattern is applied to too few arguments. Since we already introduced the current parameter
            // we expect the type to be the return type `B[a]` of our (dependent) function type `(a: A) -> B[a]`.
            // But the user did not provide an argument pattern for `(a: A)`, which means the type of this specific
            // pattern stays at `(a: A) -> B[a]`. So we have a mismatch between the old and the new target type.
            for {
              expected <- PrettyPrinter.print(target)
              actual <- PrettyPrinter.print(signature)
              result <- Elaboration.fail(TypeMismatch(expected, actual, Some(configuration.justification))(span))
            } yield result
        }).sequence.map(_.collect { case Clause(Some(copattern), body) => Clause(copattern, body) })
        // Now we can continue the elaboration of the remaining case tree under this projection.
        projectorArgument = projection.names.drop(projection.definition.parameters.size).head.map(_ => configuration.leftHandSide)
        clause <- Tactic.solve(configuration.copy(tree = body => configuration.tree(CaseTree.Record(projector.clauses :+ Clause(projection, body))), leftHandSide = Value.Application.Global.Declaration(projection, parameters :+ projectorArgument), target = target, clauses = clauses)).map(_.map(Clause(projection, _)))
        // In case the above tactic was successful, we can extend our projection by the new clause here. Otherwise, the
        // pattern this projection is part of cannot match, and we do not need to add it.
        newProjector = clause.map { clause =>
          CaseTree.Record(projector.clauses :+ clause)
        }.getOrElse(projector)
        // We update the definition's case tree already, such that later projections can depend on former projections
        _ = configuration.definition.tree = Some(configuration.tree(newProjector))
        // Using the extended projector, we can now cosplit on the remaining projections
        projector <- cosplit(parameters, projections, newProjector, configuration)
      } yield projector

    case Nil =>
      // Here, we are done, because we considered all projections of the family.
      Elaboration.pure(projector)
  }

}
