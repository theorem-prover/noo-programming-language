package noo.compiler.elaboration.matching.tactic
import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.matching.Configuration
import noo.compiler.syntax.core.CaseTree

object Stuck extends Tactic {

  override def apply(configuration: Configuration): Elaboration[Option[CaseTree]] =
    impossible("there should always be tactic that is applicable")

}
