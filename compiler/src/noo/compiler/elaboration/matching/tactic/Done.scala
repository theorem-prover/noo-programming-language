package noo.compiler.elaboration.matching.tactic
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.checking.TypeCheck
import noo.compiler.elaboration.context.{Binding, Visibility}
import noo.compiler.elaboration.matching.{CoPattern, Configuration, Introduction}
import noo.compiler.error.NonExhaustivePatternMatch
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.syntax.common.Clause
import noo.compiler.syntax.core.{CaseTree, Declaration, Term}

object Done extends Tactic {

  override def apply(configuration: Configuration): Elaboration[Option[CaseTree]] = {
    if (configuration.clauses.headOption.exists(!_.head.matches)) {
      // We are not done yet!
      Elaboration.pure(None)
    } else if (configuration.clauses.isEmpty) {
      Conversion.force(configuration.target, unfold = true).flatMap {
        case Value.Application.Global.Declaration(family: Declaration.Family, _) if family.polarity.isNegative && family.clauses.isEmpty =>
          if (Tactic.debug) println("[DEBUG] Apply tactic 'done'.")
          Elaboration.pure(Some(CaseTree.Record(Nil)))

        case Value.Application.Global.Declaration(family: Declaration.Family, _) =>
          Elaboration.fail(NonExhaustivePatternMatch(configuration.definition, family.clauses.map(_.qualifier.last.get).map("`" + family.name + "." + _ + "`")) _)

        case _ =>
          Elaboration.fail(NonExhaustivePatternMatch(configuration.definition, Nil) _)
      }
    } else {
      if (Tactic.debug) println("[DEBUG] Apply tactic 'done'.")
      // Since the first clause matches, its eliminations are elaborated and its constraints are solved.
      // We are only interested in the pattern variables (to rename and define the binders accordingly).
      val Clause(CoPattern(_, _, introductions, _), expression) = configuration.clauses.head

      introduce(introductions) {
        TypeCheck.check(expression, configuration.target, configuration.justification)
      }.map(CaseTree.Body).map(Some(_))
    }
  }

  private def introduce(introductions: List[Introduction])(continuation: => Elaboration[Term]): Elaboration[Term] =
    introductions match {
      case introduction :: introductions =>
        introduce(introduction)(introduce(introductions)(continuation))
      case Nil =>
        continuation
    }

  private def introduce(introduction: Introduction)(continuation: => Elaboration[Term]): Elaboration[Term] = introduction match {
    case Introduction.Synonym(level, name, _) =>
      Elaboration.context.flatMap { context =>
        val binding = context.environment.get(level) match {
          case Binding.Definition(_, level, signature, value, _) =>
            Binding.Definition(name, level, signature, value, Visibility.Visible)
          case Binding.Abstraction(_, level, signature, idiom, _) =>
            Binding.Abstraction(name, level, signature, idiom, Visibility.Visible)
        }

        Elaboration.withContext(context.modify(_.bind(binding)))(continuation)
      }

    case Introduction.Definition(value, name, signature) =>
      // TODO: Maybe remember the level, where this definition should be inserted into the context? In this case we can drop the `Introduction` data type and
      //       replace it with the `Binding` type. This would simplify the code here, as the environment already provides a method to bind a sequence of bindings.
      for {
        domain <- Conversion.reify(signature, unfold = false)
        definition <- Conversion.reify(value, unfold = false)
        body <- Elaboration.define(name, signature, value, Visibility.Visible) {
          _ => continuation
        }
      } yield Term.Let(name, domain, definition, body)

  }

}
