package noo.compiler.elaboration.matching.tactic

import noo.compiler.common.{impossible, ~>}
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.elaboration.context.{Binding, Environment, Visibility}
import noo.compiler.elaboration.matching._
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation._
import noo.compiler.syntax.common.Clause.ClauseGroup
import noo.compiler.syntax.common.{Argument, Clause, Spine}
import noo.compiler.syntax.core.{CaseTree, Declaration, Index}
import noo.compiler.syntax.surface.Expression

object Split extends Tactic {

  override def apply(configuration: Configuration): Elaboration[Option[CaseTree]] =
    if (configuration.clauses.isEmpty || configuration.clauses.head.head.constraints.isEmpty) {
      // We cannot split anything
      Elaboration.pure(None)
    } else {
      Elaboration.context.flatMap { context =>
        val name = context.environment.get(configuration.clauses.head.head.constraints.head.asInstanceOf[Constraint.Simple].level).name
        if (Tactic.debug) println(s"[DEBUG] Apply tactic 'split' on parameter `$name`.")
        // Here, we try to solve the first constraint of the first clause by splitting on it. If this is successful and
        // the clause as no more constraint afterward, it matches and thus all remaining clauses cannot match anymore (which
        // is reported in the `Done` tactic).
        configuration.clauses.head.head.constraints match {
          case Constraint.Simple(level, Pattern.Application(constructor, _, span), signature) :: _ =>
            Elaboration.at(span) { // We use the position of the whole pattern to report errors
              Conversion.force(signature, unfold = true).flatMap { // We check if the type of the constraint evaluates to an application of the constructor's family
                case Value.Application.Global.Declaration(family: Declaration.Family, arguments) if constructor.definition == family =>
                  family.clauses.map {
                    case constructor: Declaration.Clause.Constructor =>
                      // This type is already well-typed so we know that it must be fully applied
                      refine(level, constructor, arguments, configuration) { case (configuration, matcher) =>
                        for {
                          configuration <- Simplification.simplify(configuration)
                          result <- Tactic.solve(configuration)
                        } yield result.map(Clause(matcher, _))
                      }.map(_.flatten)
                    case _ =>
                      impossible("a positive family cannot have clauses that are not constructors")
                  }.sequence.flatMap { clauses =>
                    Elaboration.context.map(context => Some(CaseTree.Match(level.toIndex(context.environment.level), clauses.flatten)))
                  }

                case expected =>
                  // The pattern has an illegal type (e.g. a constructor of a completely different family)
                  Tactic.failWithPositiveFamilyMismatch(expected, constructor, configuration.justification, span)
              }
            }

          case Constraint.Simple(_, Pattern.Wildcard(_), _) :: constraints =>
            // A wildcard pattern does not lead to a renaming. In this way, the binder that was previously introduced
            // by the tactic 'introduce' remains bound as a signature binder and is not available to the user.
            Tactic.solve(configuration.copy(clauses = configuration.clauses.head.map {
              case CoPattern(eliminations, _, variables, span) =>
                CoPattern(eliminations, constraints, variables, span)
            } :: configuration.clauses.tail))

          case Constraint.Simple(level, Pattern.Variable(name, _), signature) :: constraints =>
            // Just like a wildcard, a pattern variable always matches. However, it leads to a renaming, as the name of
            // the pattern variable might differ from the name in the signature. Moreover, by renaming the binder that
            // was previously introduced by the tactic 'introduce' is changed to a source binder, such that it is available
            // to the user.
            Tactic.solve(configuration.copy(clauses = configuration.clauses.head.map {
              case CoPattern(eliminations, _, variables, span) =>
                CoPattern(eliminations, constraints, Introduction.Synonym(level, name, signature) :: variables, span)
            } :: configuration.clauses.tail))

          case _ =>
            // This should not happen, because when we start elaborating a (co)pattern match only simple constraints are generated.
            // Whenever we split on such a simple constraint, either new simple constraints are generated or the constraint leads
            // to a definition or renaming.
            impossible(s"split should never encounter a complex constraint")
        }
      }
    }

  private def refine[A](scrutinee: Level, constructor: Declaration.Clause.Constructor, familyArguments: Spine[Value], configuration: Configuration)(continuation: (Configuration, Matcher) => Elaboration[A]): Elaboration[Option[A]] =
    for {
      context <- Elaboration.context
      (prefix, _ +: suffix) = context.environment.bindings.splitAt(scrutinee.value)
      // Parameterize the constructor signature according to the family parameters
      parameters = familyArguments.take(constructor.definition.parameters.size)
      signature <- Conversion.instantiate(constructor.signature, parameters)
      // Introduce the remaining arguments of the constructor, such that it is fully applied
      (introductions, Value.Application.Global.Declaration(_, constructorTargetArguments)) <- introduce(constructor, signature, scrutinee)
      refinement = Value.Application.Global.Declaration(constructor, parameters ++ introductions.map(_.map(_.value)))
      // Prepare the new context by adapting all levels that occur after the scrutinee. Since we remove the scrutinee
      // but introduce the constructor's arguments, we have to offset all levels after the scrutinee by `n - 1`, where
      // `n` is the number of constructor arguments (i.e., without the family parameters). On top of this, we also
      // replace the scrutinee with its refinement.
      offset = introductions.size - 1
      substitution = Substitution((scrutinee + 1 until scrutinee + 1 + suffix.size).map {
        level => (level: Level, Value.Application.Local(level + offset, Spine.empty))
      }: _*) + (scrutinee, refinement)
      // We only have to adapt the levels in `suffix` since these are the bindings that may actually depend on `scrutinee`.
      // Since contexts are always well-ordered, `prefix` cannot depend on `scrutinee`.
      bindings <- suffix.map(refine(_, substitution)).sequence.map(prefix ++ introductions.map(_.argument) ++ _)
      newEnvironment = Environment.from(bindings)

      // Here, we actually go under the context using the newly constructed bindings
      result <- Elaboration.withContext(context.modify(_ => newEnvironment)) {
        for {
          target <- substitution(configuration.target)
          clauses <- refine(configuration.clauses, substitution)
          value <- substitution.apply(configuration.leftHandSide)
          // Now we can equate the family indices under the new context. For this purpose, we first have to adapt the indices
          // of the type of the scrutinee. We remove the scrutinee from the substitution here, because its type cannot depend
          // on itself. After this substitution, scrutinee type can be safely used under the new context.
          familyArguments <- (substitution - scrutinee)(familyArguments)

          // Try to unify the indices of the scrutinee type with the indices of the type of the constructor application
          result <- Equation.equate(
            familyArguments.drop(parameters.size),
            constructorTargetArguments.drop(parameters.size),
            Value.Application.Global.Declaration(constructor.definition, familyArguments),
            Value.Application.Global.Declaration(constructor.definition, constructorTargetArguments),
            configuration.justification
          ) { case (substitution, order) =>
            for {
              // Now we can update the target and all clauses (and especially their constraints)
              target <- substitution(target)
              clauses <- refine(clauses, substitution)
              value <- substitution.apply(value)
              parameters <- substitution(familyArguments.take(parameters.size))
              matcher = this.matcher(constructor, parameters, order)
              result <- continuation(configuration.copy(leftHandSide = value, target = target, clauses = clauses), matcher)
            } yield result
          }
        } yield result
      }
    } yield result

  private def refine(clauses: ClauseGroup[CoPattern, Expression], substitution: Substitution): Elaboration[ClauseGroup[CoPattern, Expression]] =
    clauses.map(_.mapM { case CoPattern(eliminations, constraints, variables, span) =>
      for {
        constraints <- constraints.map(refine(_, substitution)).sequence
        variables <- variables.map(refine(_, substitution)).sequence
      } yield CoPattern(eliminations, constraints, variables, span)
    }).sequence

  private def refine(introduction: Introduction, substitution: Substitution): Elaboration[Introduction] = introduction match {
    case Introduction.Synonym(level, name, signature) =>
      substitution(signature).map(Introduction.Synonym(level, name, _))

    case Introduction.Definition(value, name, signature) =>
      for {
        value <- substitution(value).map(_.asInstanceOf[Value.Application])
        signature <- substitution(signature)
      } yield Introduction.Definition(value, name, signature)
  }

  private def refine(constraint: Constraint, substitution: Substitution): Elaboration[Constraint] =
    for {
      value <- substitution(constraint.value)
      signature <- substitution(constraint.signature)
      constraint <- Constraint.from(value, constraint.pattern, signature)
    } yield constraint

  def refine(binder: Binding, substitution: Substitution): Elaboration[Binding] =
    binder match {
      case Binding.Definition(name, level, signature, value, origin) =>
        for {
          signature <- substitution(signature)
          value <- substitution(value)
          // We only call this method on bindings which are not the scrutinee binder.
          // Because of the way how the substitution is constructed below, any other replacement beside the one for
          // the scrutinee is just a bound variable whose level is offset.
          newLevel = if (substitution.isDefinedAt(level)) substitution(level).asInstanceOf[Value.Application.Local].level else level
        } yield Binding.Definition(name, newLevel, signature, value, origin)

      case Binding.Abstraction(name, level, signature, idiom, visibility) =>
        for {
          signature <- substitution(signature)
          // We only call this method on bindings which are not the scrutinee binder.
          // Because of the way how the substitution is constructed below, any other replacement beside the one for
          // the scrutinee is just a bound variable whose level is offset.
          newLevel = if (substitution.isDefinedAt(level)) substitution(level).asInstanceOf[Value.Application.Local].level else level
        } yield Binding.Abstraction(name, newLevel, signature, idiom, visibility)
    }

  private def introduce(constructor: Declaration.Clause.Constructor, signature: Type, level: Level): Elaboration[(Vector[Argument[Binding]], Value.Application.Global.Declaration)] =
    Conversion.force(signature, unfold = true).flatMap {
      case Value.Pi(parameter, head, body, idiom) =>
        // This is the binder for the n-th index of the constructor, where the level starts at `scrutinee` in order to replace it.
        val binder = Binding.Abstraction(parameter, level, head, idiom, Visibility.Hidden)
        for {
          signature <- body(binder.value)
          (bindings, target) <- introduce(constructor, signature, level + 1)
        } yield (Argument.Default(binder, idiom) +: bindings, target)

      case target @ Value.Application.Global.Declaration(family, _) if family == constructor.definition =>
        Elaboration.pure((Vector.empty, target))

      case _ =>
        impossible
    }

  private def matcher(c: Declaration.Clause.Constructor, parameters: Spine[Value], order: Seq ~> Seq): Matcher =
    new Matcher {

      override val constructor: Declaration.Clause.Constructor = c

      def define[A](index: Index, arguments: Vector[Value])(continuation: => Elaboration[A]): Elaboration[A] =
        for {
          signature <- Conversion.instantiate(constructor.signature, parameters)
          environment <- Elaboration.environment
          level = index.toLevel(environment.level)

          bindings <- introduce(signature, arguments.drop(constructor.definition.parameters.size), level)
          refinement = Value.Application.Global.Declaration(constructor, parameters ++ constructor.names.drop(parameters.size).zip(bindings).map {
            case (argument, binding) => argument.map(_ => binding.value)
          })

          (prefix, _ +: suffix) = environment.bindings.splitAt(level.value)

          offset = bindings.size - 1
          substitution = Substitution((level + 1 until level + 1 + suffix.size).map {
            level => (level: Level, Value.Application.Local(level + offset, Spine.empty))
          }: _*) + (level, refinement)
          suffix <- suffix.map(Split.refine(_, substitution)).sequence

          result <- Elaboration.withEnvironment(Environment.from(order.apply(prefix ++ bindings ++ suffix))) {
            continuation
          }
        } yield result

      def introduce(signature: Type, arguments: Vector[Value], level: Level): Elaboration[List[Binding]] =
        arguments match {
          case argument +: arguments =>
            for {
              (parameter, head, body) <- Conversion.force(signature, unfold = true).map {
                case Value.Pi(parameter, head, body, _) => (parameter, head, body)
                case _ => impossible
              }
              binding = Binding.Definition(parameter, level, head, argument, Visible)
              signature <- body(binding.value)
              bindings <- introduce(signature, arguments, level + 1)
            } yield binding :: bindings

          case Vector() =>
            Elaboration.pure(Nil)
        }

    }

}
