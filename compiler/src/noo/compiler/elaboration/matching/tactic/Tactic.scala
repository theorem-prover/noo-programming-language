package noo.compiler.elaboration.matching.tactic

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility
import noo.compiler.elaboration.matching.Configuration
import noo.compiler.error.TypeMismatch
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Level, Value}
import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.Declaration.Clause.{Constructor, Projection}
import noo.compiler.syntax.core.{CaseTree, PrettyPrinter}

trait Tactic extends (Configuration => Elaboration[Option[CaseTree]]) {

  def |(tactic: Tactic): Tactic = (configuration: Configuration) =>
    this(configuration).flatMap {
      case Some(term) => Elaboration.pure(Some(term))
      case None => tactic(configuration)
    }

  def apply(configuration: Configuration): Elaboration[Option[CaseTree]]

}

object Tactic {

  val debug = false

  /**
   * The general tactic to solve (or, more technically, elaborate) a pattern matching problem. The order should not be
   * functionally important (except for the stuck tactic which always fails), but the below order is the most efficient
   * one. This is because a cosplit can only take place when the target type is a negative family (which is impossible
   * when we still have to introduce things) and a split can only take place when constraints have been previously
   * introduced.
   */
  val solve: Tactic = Done | Introduction | CoSplit | Split | Stuck

  /**
   * Aborts elaboration with an error message that states a type mismatch between the projection's source type and the
   * specified expected type.
   * @param expected The expected type.
   * @param projection The projection whose source type does not match.
   * @param span The position where the error should be reported.
   * @return An elaboration that fails with a type mismatch.
   */
  def failWithNegativeFamilyMismatch(expected: Type, projection: Projection, justification: String, span: SourceSpan): Elaboration[Nothing] = {
    def printProjectorType(signature: Type, level: Level): Elaboration[String] =
      Conversion.force(signature, unfold = true).flatMap {
        case Value.Pi(_, projectorType, _, _) if level.value == projection.definition.parameters.size =>
          PrettyPrinter.print(projectorType)

        case Value.Pi(parameter, head, body, idiom) =>
          Elaboration.bind(parameter, head, idiom, Visibility.Visible) {
            binder => body(binder.value).flatMap(printProjectorType(_, level + 1))
          }

        case _ =>
          impossible("we must be able to reach the projector type")
      }

    for {
      actual <- printProjectorType(projection.signature, 0)
      expected <- PrettyPrinter.print(expected)
      result <- Elaboration.fail(TypeMismatch(expected, actual, Some(justification))(span))
    } yield result
  }

  /**
   * Aborts elaboration with an error message that states a type mismatch between the constructor's target type and the
   * specified expected type.
   * @param expected The expected type.
   * @param constructor The constructor whose target type does not match.
   * @param span The position where the error should be reported.
   * @return An elaboration that fails with a type mismatch.
   */
  def failWithPositiveFamilyMismatch(expected: Type, constructor: Constructor, justification: String, span: SourceSpan): Elaboration[Nothing] = {
    def printConstructedType(signature: Type): Elaboration[String] =
      Conversion.force(signature, unfold = true).flatMap {
        case Value.Pi(parameter, head, body, idiom) =>
          Elaboration.bind(parameter, head, idiom, Visibility.Visible) {
            binder => body(binder.value).flatMap(printConstructedType)
          }

        case actual =>
          PrettyPrinter.print(actual)
      }

    for {
      actual <- printConstructedType(constructor.signature)
      expected <- PrettyPrinter.print(expected)
      result <- Elaboration.fail(TypeMismatch(expected, actual, Some(justification))(span))
    } yield result
  }

}
