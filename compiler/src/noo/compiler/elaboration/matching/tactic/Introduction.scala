package noo.compiler.elaboration.matching.tactic
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility
import noo.compiler.elaboration.matching._
import noo.compiler.error.TypeMismatch
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.syntax.common.{Argument, Elimination}
import noo.compiler.syntax.core.{CaseTree, PrettyPrinter}

object Introduction extends Tactic {

  override def apply(configuration: Configuration): Elaboration[Option[CaseTree]] =
    Conversion.force(configuration.target, unfold = true).flatMap {
      // We can introduce the next parameter if `target ↘ (parameter : head) -> body` and the first clause has an application elimination
      case oldTarget @ Value.Pi(parameter, head, body, idiom) if configuration.clauses.headOption.exists(_.head.eliminations.headOption.exists(_.isApplication)) =>
        if (Tactic.debug) println(s"[DEBUG] Apply tactic 'introduction' on parameter `$parameter`.")
        for {
          domain <- Conversion.reify(head, unfold = false)
          // The target type is a (dependent) function type. So we introduce its parameter here.
          // The name is only the one specified in the type signature for now. If the user specified a pattern variable
          // in place of this parameter, the binder created here will be renamed in the tactic `Done`.
          body <- Elaboration.bind(parameter, head, idiom, Visibility.Hidden) { binder =>
            for {
              // The new target type is the return type of the (dependent) function type.
              newTarget <- body(binder.value)
              // Here, we add a new simple constraint which tries to match the parameter we introduced above against the
              // pattern that comes next in the list of eliminations. It is important to note, that a parameter can only
              // be matched against an application elimination. If the next elimination is a projection (or is missing at
              // all) this constitutes a type mismatch between the pattern type and the target type.
              clauses <- configuration.clauses.map(_.mapM {
                case CoPattern(Elimination.Application(Argument(pattern, _)) :: eliminations, constraints, variables, span) =>
                  Constraint.from(binder.value, pattern, head).map { constraint =>
                    CoPattern(
                      eliminations,
                      constraints :+ constraint,
                      variables,
                      span
                    )
                  }

                case CoPattern(Elimination.Projection(projection) :: _, _, _, span) =>
                  // This projection is too "early". That is, our current target type is a (dependent) function type,
                  // but the projection expects its projector, which must be an instance of a negative family. Since a
                  // (dependent) function is not an instance of a negative family, this is always a type mismatch
                  // between this (dependent) function type and the type of the projector (which is actually expected).
                  Tactic.failWithNegativeFamilyMismatch(oldTarget, projection, configuration.justification, span)

                case CoPattern(Nil, _, _, span) =>
                  // This pattern is applied to too few arguments. Since we already introduced the current parameter
                  // we expect the type to be the return type `B[a]` of our (dependent) function type `(a: A) -> B[a]`.
                  // But the user did not provide an argument pattern for `(a: A)`, which means the type of this specific
                  // pattern stays at `(a: A) -> B[a]`. So we have a mismatch between the old and the new target type.
                  for {
                    expected <- PrettyPrinter.print(newTarget)
                    actual <- PrettyPrinter.print(oldTarget)
                    result <- Elaboration.fail(TypeMismatch(expected, actual, Some(configuration.justification))(span))
                  } yield result
              }).sequence

              // Now that we introduced the parameter we can continue elaboration with the refined target and clauses.
              value <- Conversion.apply(configuration.leftHandSide, Argument.Default(binder.value, idiom))
              result <- Tactic.solve(configuration.copy(tree = body => configuration.tree(CaseTree.Introduction(parameter, domain, body, idiom)), leftHandSide = value, target = newTarget, clauses = clauses))
            } yield result
          }
        } yield body.map(CaseTree.Introduction(parameter, domain, _, idiom))

      case _ =>
        // We cannot introduce anymore!
        Elaboration.pure(None)
    }

}
