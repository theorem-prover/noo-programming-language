package noo.compiler.elaboration.matching

import noo.compiler.elaboration.Elaboration
import noo.compiler.evaluation.Value
import noo.compiler.syntax.core.{Declaration, Index}

trait Matcher {

  def constructor: Declaration.Clause.Constructor

  def define[A](scrutinee: Index, arguments: Vector[Value])(continuation: => Elaboration[A]): Elaboration[A]

}
