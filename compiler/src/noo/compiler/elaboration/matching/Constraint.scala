package noo.compiler.elaboration.matching

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Level, Value}
import noo.compiler.syntax.common.Spine
import noo.compiler.syntax.surface.Expression

/**
 * A constraint represents a requirement that must be fulfilled in order for a clause to match. It generally consists
 * of a left-hand side (the actual value), a right-hand side (the user pattern) and a type. The left-hand side must
 * match the right-hand side in order for a constraint to be solved.
 */
sealed trait Constraint {

  /** The left-hand side (i.e., the actual value) of this constraint. */
  def value: Value.Application

  /** The right-hand side (i.e., the user pattern) of this constraint. */
  def pattern: Pattern

  /** The type signature of this constraint. */
  def signature: Type

  /** Whether this constraint is simple (i.e., whether its left-hand side is known to be a bound variable). */
  def isSimple: Boolean

}

object Constraint {

  def from(value: Value, pattern: Pattern, signature: Type): Elaboration[Constraint] =
    Conversion.force(value, unfold = true).map {
      case Value.Application.Local(level, Spine.empty) =>
        Simple(level, pattern, signature)
      case value: Value.Application =>
        Complex(value, pattern, signature)
      case _ =>
        impossible("a constraint may never be split into something that is not a constructor application or bound variable")
    }

  def from(value: Value, expression: Expression, signature: Type): Elaboration[Constraint] =
    for {
      pattern <- Pattern.from(expression)
      constraint <- from(value, pattern, signature)
    } yield constraint

  /**
   * A constraint that is simple insofar it can either lead to the renaming of a bound variable (when the user pattern
   * is a pattern variable) or to a further split (in case the user pattern is the application of a constructor).
   * @param level The level of the bound variable.
   * @param pattern The user pattern.
   * @param signature The type signature of this constraint.
   */
  case class Simple(level: Level, pattern: Pattern, signature: Type) extends Constraint {

    val value: Value.Application = Value.Application.Local(level, Spine.empty)

    val isSimple: Boolean = true

  }

  /**
   * A constraint that is complex insofar its left-hand side is not known to be a bound variable, but may be an
   * application of a constructor. A complex constraint can be simplified when the user pattern is a corresponding
   * constructor application as well.
   * @param value The actual value.
   * @param pattern The composed part of the user pattern.
   * @param signature The type signature of this constraint.
   */
  case class Complex(value: Value.Application, pattern: Pattern, signature: Type) extends Constraint {

    val isSimple: Boolean = false

  }

  def unapply(constraint: Constraint): Option[(Value.Application, Pattern, Type)] =
    Some((constraint.value, constraint.pattern, constraint.signature))

}
