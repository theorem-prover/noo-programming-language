package noo.compiler.elaboration.matching

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.matching.tactic.Tactic
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.syntax.common.{Clause, Spine}
import noo.compiler.syntax.core.Declaration
import noo.compiler.syntax.surface.Expression

object Simplification {

  def simplify(configuration: Configuration): Elaboration[Configuration] =
    if (configuration.clauses.isEmpty) {
      Elaboration.pure(configuration)
    } else configuration.clauses.map { case Clause(CoPattern(eliminations, constraints, introductions, span), body) =>
      Elaboration.at(span) {
        simplify(constraints)(configuration).map(_.map {
          case (constraints, newIntroductions) => Clause(CoPattern(eliminations, constraints, introductions ++ newIntroductions, span), body)
        })
      }
    }.sequence.map(_.collect {
      case Some(clause) => clause
    }).map(clauses => configuration.copy(clauses = clauses))

  private def simplify(constraints: List[Constraint])(implicit configuration: Configuration): Elaboration[Option[(List[Constraint], List[Introduction])]] =
    constraints.map(simplify).sequence.map(_.foldLeft[Option[(List[Constraint], List[Introduction])]](Some((Nil, Nil))) {
      case (None, _) => None
      case (_, None) => None
      case (Some((constraints1, introductions1)), Some((constraints2, introductions2))) =>
        Some((constraints1 ++ constraints2, introductions1 ++ introductions2))
    })

  private def simplify(constraint: Constraint)(implicit configuration: Configuration): Elaboration[Option[(List[Constraint], List[Introduction])]] = constraint match {
    case simple: Constraint.Simple =>
      Elaboration.pure(Some((List(simple), Nil)))

    case Constraint.Complex(value, pattern, signature) =>
      Conversion.force(value, unfold = true).flatMap {
        case Value.Application.Local(level, Spine.empty) =>
          Elaboration.pure(Some((List(Constraint.Simple(level, pattern, signature)), Nil)))
        case value =>
          simplify(Constraint.Complex(value.asInstanceOf[Value.Application], pattern, signature))
      }
  }

  private def simplify(complex: Constraint.Complex)(implicit configuration: Configuration): Elaboration[Option[(List[Constraint], List[Introduction])]] = complex match {
    case Constraint.Complex(Value.Application.Global.Declaration(other: Declaration.Clause.Constructor, valueArguments), Pattern.Application(constructor, patterns, span), signature) if other == constructor =>
      val (parameters, values) = valueArguments.split(constructor.definition.parameters.size)
      // At this point the user may have supplied too few arguments for the constructor to be fully applied. It is,
      // however, not possible that the user supplied too many arguments, as this would have lead to an error during
      // the pattern decomposition.
      if (patterns.size < values.size) {
        Tactic.failWithPositiveFamilyMismatch(signature, constructor, configuration.justification, span)
      } else {
        for {
          signature <- Conversion.instantiate(constructor.signature, parameters)
          constraints <- constrainArguments(signature, values.toList.map(_.argument), patterns.toList.map(_.argument))
          result <- simplify(constraints)
        } yield result
      }

    case Constraint.Complex(Value.Application.Global.Declaration(other: Declaration.Clause.Constructor, valueArguments), Pattern.Application(constructor, patterns, span), signature) if other.definition != constructor.definition =>
      // The pattern has an illegal type (e.g. a constructor of a completely different family)
      Tactic.failWithPositiveFamilyMismatch(signature, constructor, configuration.justification, span)

    case Constraint.Complex(Value.Application.Global.Declaration(_: Declaration.Clause.Constructor, _), Pattern.Application(_, _, _), _) =>
      Elaboration.pure(None)

    case Constraint.Complex(_, Pattern.Wildcard(_), _) =>
      Elaboration.pure(Some((Nil, Nil)))

    case Constraint.Complex(value, Pattern.Variable(name, _), signature) =>
      Elaboration.pure(Some((Nil, List(Introduction.Definition(value, name, signature)))))

    case _ =>
      // The pattern is an application pattern, but the value does not match its constructor
      impossible
  }

  private def constrainArguments(signature: Type, values: List[Value], patterns: List[Expression]): Elaboration[List[Constraint]] = (values, patterns) match {
    case (value :: values, pattern :: patterns) =>
      Conversion.force(signature, unfold = true).flatMap {
        case Value.Pi(_, head, body, _) =>
          for {
            constraint <- Constraint.from(value, pattern, head)
            signature <- body(value)
            constraints <- constrainArguments(signature, values, patterns)
          } yield constraint :: constraints

        case _ =>
          impossible
      }

    case (Nil, Nil) =>
      Elaboration.pure(Nil)

    case _ =>
      impossible
  }

}
