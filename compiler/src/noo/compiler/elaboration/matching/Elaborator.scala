package noo.compiler.elaboration.matching

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.matching.tactic.Tactic
import noo.compiler.error.NonExhaustivePatternMatch
import noo.compiler.evaluation.Value
import noo.compiler.syntax.common.Clause.ClauseGroup
import noo.compiler.syntax.common.Spine
import noo.compiler.syntax.core.CaseTree
import noo.compiler.syntax.core.Declaration.Definition
import noo.compiler.syntax.surface.Expression

object Elaborator {

  /**
   * Elaborate the provided user clauses of the specified definition into a case tree.
   * @param definition The definition whose user clauses shall be elaborated.
   * @param clauses The user clauses that shall be elaborated.
   * @return The case tree that corresponds to the provided user clauses.
   */
  def elaborate(definition: Definition, clauses: ClauseGroup[Expression, Expression]): Elaboration[CaseTree] =
    for {
      // First, we need to (partially) decompose the expression patterns into a sequence of eliminations. This is
      // necessary because we have to analyze co-patterns inside-out. By decomposing the user pattern, the innermost
      // projection is the first elimination, where the definition itself is completely removed. As a convenient
      // addition, projections have already been resolved at the correct code location, which is why we can drop any
      // positional object on the top-level of the pattern.
      clauses <- clauses.map(_.mapM(CoPattern.from(_, definition))).sequence
      // Now, we can run the actual elaboration on the below configuration here.
      configuration = Configuration(definition, Value.Application.Global.Definition(definition, Spine.empty), definition.signature, s"the signature of ${definition.identifier}", body => body, clauses)
      term <- Tactic.solve(configuration).flatMap {
        case Some(term) =>
          Elaboration.pure(term)

        case None =>
          Elaboration.fail(NonExhaustivePatternMatch(definition, Nil) _)
      }
    } yield term


}
