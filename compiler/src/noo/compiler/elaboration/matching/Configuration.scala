package noo.compiler.elaboration.matching

import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.Value
import noo.compiler.syntax.common.Clause.ClauseGroup
import noo.compiler.syntax.core.CaseTree
import noo.compiler.syntax.core.Declaration.Definition
import noo.compiler.syntax.surface.Expression

/**
 * The configuration that is used in addition to the elaboration context when elaborating a pattern match. Each
 * configuration corresponds to a branch of the final case tree, where the target type is the expected (and possibly
 * refined) return type of the corresponding branch.
 * @param definition The definition whose pattern match is currently being elaborated.
 * @param leftHandSide The left hand side value of this configuration.
 * @param target The current target type.
 * @param justification The justification of the target type.
 * @param tree A constructor of the currently elaborated case tree that accepts the body.
 * @param clauses The remaining group of user clauses.
 */
case class Configuration(
  definition: Definition,
  leftHandSide: Value,
  target: Type,
  justification: String,
  tree: CaseTree => CaseTree,
  clauses: ClauseGroup[CoPattern, Expression]
)
