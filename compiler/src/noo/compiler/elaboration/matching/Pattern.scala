package noo.compiler.elaboration.matching

import noo.compiler.elaboration.Elaboration
import noo.compiler.error._
import noo.compiler.input.SourceSpan
import noo.compiler.modifier.{Idiom, Polarity}
import noo.compiler.syntax.common.{Argument, Spine}
import noo.compiler.syntax.core.Namespace.Entry
import noo.compiler.syntax.core.{Declaration, Signature}
import noo.compiler.syntax.surface.Expression

import scala.annotation.tailrec

/**
 * The internal representation of a partially decomposed pattern. Such a pattern may either be:
 * 1. A wildcard pattern, which corresponds to an underscore in the surface language
 * 2. A pattern variable
 * 3. A constructor application
 */
sealed trait Pattern {

  def span: SourceSpan

}

object Pattern {

  /** A class that represents wildcard patterns. */
  case class Wildcard(span: SourceSpan) extends Pattern

  /** A class that represents pattern variables. */
  case class Variable(name: String, span: SourceSpan) extends Pattern

  /** A class that represents constructor application patterns. */
  case class Application(constructor: Declaration.Clause.Constructor, arguments: Spine[Expression], span: SourceSpan) extends Pattern

  /**
   * Decomposes the provided expression into a (partially decomposed) pattern. On top of this, missing implicit
   * pattern arguments are inserted.
   * @param expression The expression that shall be decomposed.
   * @return The partially decomposed pattern where missing implicit pattern arguments are inserted.
   */
  def from(expression: Expression): Elaboration[Pattern] = {
    val span = expression.getSourceSpan.getOrElse(SourceSpan.Undefined)

    def decompose(expression: Expression): Elaboration[Pattern] = expression match {
      case Expression.Hole =>
        // A surface hole in a pattern is treated as a wildcard pattern
        Elaboration.pure(Wildcard(span))

      case Expression.Position(expression, span) =>
        Elaboration.at(span)(decompose(expression))

      case Expression.Variable(qualifier) =>
        // Try to resolve the `qualifier` but do not fail, as the `qualifier` might be a pattern variable otherwise
        Elaboration.namespace.flatMap(_.resolve(qualifier)).flatMap {
          case Some(Entry(constructor: Declaration.Clause.Constructor)) =>
            Elaboration.pure(Application(constructor, Spine.empty, span))

          case _ if qualifier.length == 1 =>
            // We treat this as a pattern variable regardless of whether the qualifier actually references something or not
            Elaboration.pure(Variable(qualifier.elements.head, span))

          case Some(Entry(declaration)) =>
            // This might be a projection (which are not allowed in pattern position) or even something completely different.
            Elaboration.fail(IllegalPattern(declaration.identifier, Polarity.Positive) _)

          case None =>
            // The qualifier is neither a pattern variable nor does it even exist, so we report this as an unknown identifier.
            Elaboration.namespace.flatMap { namespace =>
              Elaboration.fail(UnknownIdentifier(qualifier.toString, namespace) _)
            }
        }

      case Expression.Application(function, argument) =>
        decompose(function).flatMap {
          case Application(constructor, arguments, span) =>
            apply(constructor, arguments, argument).map {
              arguments => Application(constructor, arguments, span)
            }
          case Wildcard(_) =>
            Elaboration.fail(IllegalApplicationPattern("a wildcard", argument.argument) _)
          case Variable(name, _) =>
            Elaboration.fail(IllegalApplicationPattern(s"pattern variable `$name`", argument.argument) _)
        }
    }

    decompose(expression).map {
      case Application(constructor, arguments, span) =>
        // We infer any trailing implicit pattern arguments here, since a constructor has to be fully applied anyway.
        // This is just for convenience, because the user does not have to provide such trailing implicits then. If there
        // are remaining explicit arguments after this step, this will later lead to a type mismatch in the tactic 'Split'.
        val numberOfAppliedArguments = constructor.definition.parameters.size + arguments.size
        Application(constructor, arguments ++ constructor.names.drop(numberOfAppliedArguments).takeWhile(_.idiom.isImplicit).map(_.map(_ => Expression.Hole)), span)

      case pattern =>
        pattern
    }
  }

  /**
   * Applies the specified argument to the provided application and returns the complete application spine. On top of
   * this, we also insert implicit wildcard patterns when the argument supplied forces us to do so (except for family
   * parameters, which cannot be matched on).
   * @param declaration The declaration that is applied.
   * @param arguments The argument spine of the current application.
   * @param argument The next argument that shall be supplied.
   * @return The complete application spine containing the provided arguments, any inserted wildcard patterns and the
   *         specified argument.
   */
  def apply(declaration: Declaration with Signature, arguments: Spine[Expression], argument: Argument[Expression]): Elaboration[Spine[Expression]] = {
    @tailrec
    def apply(parameters: List[Argument[String]], arguments: Spine[Expression]): Elaboration[Spine[Expression]] = parameters match {
      case parameter :: parameters if argument.isSpecific =>
        // Insert until we hit an implicit parameter with the specified name
        if (parameter.idiom.isExplicit) {
          // The argument is an implicit one, but the next parameter is explicit. Hence, we did not find the specified name.
          Elaboration.fail(UnknownParameter(argument.asSpecific.get.name, declaration.names.map(_.argument)) _)
        } else if (parameter.argument == argument.asSpecific.get.name) {
          // We found it, so we return the complete application spine here.
          Elaboration.pure(arguments :+ argument)
        } else {
          // The name did not match, so we insert an implicit wildcard pattern and continue at the next parameter.
          apply(parameters, arguments :+ Argument.Default(Expression.Hole, Idiom.Implicit))
        }

      case parameter :: _ if argument.idiom == parameter.idiom =>
        // The idiom of the parameter matches that of the argument (which is not a specific one), so we can just return the complete application spine.
        Elaboration.pure(arguments :+ argument)

      case parameter :: _ if argument.idiom.isImplicit =>
        // The argument is an implicit one, but the parameter is an explicit one (otherwise we would have ended up in the above case). This is an error.
        Elaboration.fail(IdiomMismatch(parameter.idiom, argument.idiom) _)

      case _ :: parameters =>
        // At this point we know that the parameter is implicit and the argument is explicit, so we just infer a wildcard pattern for this parameter.
        apply(parameters, arguments :+ Argument.Default(Expression.Hole, Idiom.Implicit))

      case Nil =>
        if (argument.isSpecific) {
          Elaboration.fail(UnknownParameter(argument.asSpecific.get.name, declaration.names.map(_.argument)) _)
        } else {
          Elaboration.fail(TooManyPatterns(arguments.size + 1, declaration) _)
        }
    }

    declaration match {
      case clause: Declaration.Clause =>
        // Drop the arguments that are already applied and skip the implicit arguments for the family parameters
        apply(clause.names.drop(clause.definition.parameters.size + arguments.size), arguments)

      case _ =>
        // Just drop the arguments that are already applied to this declaration
        apply(declaration.names.drop(arguments.size), arguments)
    }
  }

}
