package noo.compiler.elaboration.context

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility.{Hidden, Visible}
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Level, Value}
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.{Argument, Spine}
import noo.compiler.syntax.core.{Index, PrettyPrinter, Term}

import scala.collection.View

final class Environment private(
  /** The sequence of bindings in this environment indexed by de Brujin levels. */
  val bindings: Vector[Binding],
  /** A symbol table for fast name lookup of visible bindings. */
  private val symbols: Map[String, Binding],
) {

  /** The size of this environment (i.e., the number of bindings in this environment). */
  val size: Int = bindings.size

  /** The current level of this environment (which equals the size of this environment). */
  val level: Level = bindings.size

  /** The sequence of values that corresponds to this environment indexed by de Brujin indices. */
  val values: View[Value] = bindings.view.map(_.value).reverse

  /**
   * Bind a new binding in this environment. If there is already a binding with the same level bound in this environment,
   * it is removed first. The new binding is then inserted, taking into account any potential name shadowing.
   * @param binding The binding which shall be inserted.
   * @return This environment extended by the specified binding.
   */
  def bind(binding: Binding): Environment = {
    // We have to remove any previous binding from the symbol table
    val symbols1 = if (binding.level.value < bindings.size) {
      // There is a previous binding that might be visible
      val previous = bindings(binding.level.value)
      if (previous.isVisible && symbols(previous.name) == previous) {
        // The previous binding is visible and not shadowed by another binding, so we have to remove it
        symbols - previous.name
      } else symbols
    } else symbols

    // When the new binding is visible and not shadowed by a later binding, we have to add it to the symbol table
    val symbols2 = if (binding.isVisible) {
      // We search for an existing binding with this name
      val other = symbols1.get(binding.name)
      if (other.forall(_.level.value < binding.level.value)) {
        // There is no later (!) binding in the symbol table which could shadow the new binding, so we have to add the new binding
        symbols1 + ((binding.name, binding))
      } else symbols1
    } else symbols1

    if (binding.level.value == bindings.size) {
      new Environment(bindings :+ binding, symbols2)
    } else {
      new Environment(bindings.updated(binding.level.value, binding), symbols2)
    }
  }

  /**
   * Bind the provided sequence of bindings in this environment. This method behaves just like [[noo.compiler.elaboration.context.Environment#bind(noo.compiler.elaboration.context.Binding)]].
   * @param bindings The sequence of bindings which shall be inserted.
   * @return This environment extended by the specified bindings.
   */
  def bind(bindings: Binding*): Environment =
    bindings.foldLeft(this)(_ bind _)

  /**
   * Search for the provided name in this environment and return its corresponding binding.
   * @param name The name of the binding which shall be resolved.
   * @return The corresponding binding or `None` if there is no visible binding with such a name.
   */
  def resolve(name: String): Option[Binding] =
    symbols.get(name)

  /**
   * Return the value of the binding at the specified de Brujin index.
   * @param index The de Brujin index of the binding.
   * @return The value of the corresponding binding.
   */
  def value(index: Index): Value = valueByLevel(index.toLevel(level))

  /**
   * Return the value of the binding at the specified de Brujin level.
   * @param level The de Brujin level of the binding.
   * @return The value of the corresponding binding.
   */
  def valueByLevel(level: Level): Value = get(level).value

  /**
   * Return the binding at the specified de Brujin index.
   * @param index The de Brujin index of the binding.
   * @return The corresponding binding.
   */
  def getByIndex(index: Index): Binding = get(index.toLevel(level))

  /**
   * Return the binding at the specified de Brujin level.
   * @param level The de Brujin level of the binding.
   * @return The corresponding binding.
   */
  def get(level: Level): Binding = bindings(level.value)

  /**
   * Unload the specified signature onto this environment. That is, if the signature is a (dependent) function space,
   * its parameter is bound in this environment and its body is unloaded on the resulting environment.
   * @param signature The signature that shall be unloaded onto this environment.
   * @return The resulting environment as well as corresponding final return type of the signature.
   */
  def unload(signature: Type): Elaboration[(Environment, Type)] = Conversion.force(signature, unfold = true).flatMap {
    case Value.Pi(parameter, head, body, idiom) =>
      for {
        signature <- body(Value.Application.Local(bindings.size, Spine.empty))
        result <- bind(Binding.Abstraction(parameter, bindings.size, head, idiom, Visible)).unload(signature)
      } yield result

    case signature =>
      Elaboration.pure((this, signature))
  }

  /**
   * Close the specified term with respect to this environment such that it does not depend on this environment anymore.
   * @param term The term which shall be closed.
   * @return The closed term.
   */
  def close(term: Term): Elaboration[Term] = {
    def close(term: Term, bindings: Vector[Binding]): Elaboration[Term] = bindings match {
      case Vector() =>
        Elaboration.pure(term)

      case binding +: bindings if binding.isDefinition =>
        for {
          domain <- Conversion.reify(binding.signature, unfold = false)
          value <- Conversion.reify(binding.value, unfold = false)
          body <- Elaboration.modify(_.modify(_.bind(binding)))(close(term, bindings))
        } yield Term.Let(binding.name, domain, value, body)

      case binding +: bindings =>
        for {
          head <- Conversion.reify(binding.signature, unfold = false)
          body <- Elaboration.modify(_.modify(_.bind(binding)))(close(term, bindings))
        } yield Term.Lambda(binding.name, head, body, binding.idiom)
    }

    // We leave the local environment here, since we rebuild it step by step to reify the bound values.
    Elaboration.modify(_.clear)(close(term, bindings))
  }

  /**
   * Close the specified value with respect to this environment such that it does not depend on this environment anymore.
   * @param value The value which shall be closed.
   * @return The closed value.
   */
  def close(value: Value): Elaboration[Value] =
    for {
      term <- Conversion.reify(value, unfold = false)
      result <- close(term)
      result <- Elaboration.modify(_.clear)(Conversion.evaluate(result))
    } yield result

  /**
   * Close the specified type with respect to this environment such that it does not depend on this environment anymore.
   * @param term The type which shall be closed.
   * @return The closed type.
   */
  def closeType(term: Term): Elaboration[Term] = {
    def closeType(term: Term, bindings: Vector[Binding]): Elaboration[Term] = bindings match {
      case Vector() =>
        Elaboration.pure(term)

      case binding +: bindings if binding.isDefinition =>
        for {
          domain <- Conversion.reify(binding.signature, unfold = false)
          value <- Conversion.reify(binding.value, unfold = false)
          body <- Elaboration.modify(_.modify(_.bind(binding)))(closeType(term, bindings))
        } yield Term.Let(binding.name, domain, value, body)

      case binding +: bindings =>
        for {
          head <- Conversion.reify(binding.signature, unfold = false)
          body <- Elaboration.modify(_.modify(_.bind(binding)))(closeType(term, bindings))
        } yield Term.Pi(binding.name, head, body, binding.idiom)
    }

    // We leave the local environment here, since we rebuild it step by step to reify the bound values.
    Elaboration.modify(_.clear)(closeType(term, bindings))
  }

  /**
   * Close the specified type with respect to this environment such that it does not depend on this environment anymore.
   * The parameters generated by this method are always treated as explicit.
   * @param value The type which shall be closed.
   * @return The closed type.
   */
  def closeType(value: Value): Elaboration[Value] =
    for {
      term <- Conversion.reify(value, unfold = false)
      result <- closeType(term)
      result <- Elaboration.modify(_.clear)(Conversion.evaluate(result))
    } yield result

  /**
   * Apply the specified term to this environment. That is, any abstract binding leads to an application while a definition
   * is ignored. This is congruent with the type generated by [[noo.compiler.elaboration.context.Environment#closeType(noo.compiler.syntax.core.Term)]],
   * where definitions lead to `let` bindings in the type signature.
   * @param term The term which shall be applied to this environment.
   * @return The resulting application of the specified term to this environment.
   */
  def apply(term: Term): Elaboration[Term] =
    Elaboration.pure {
      bindings.filterNot(_.isDefinition).map {
        binding => Argument.Default(Term.Variable.Local(binding.level.toIndex(level)), binding.idiom)
      }.foldLeft(term)(Term.Application)
    }

  /**
   * Apply the specified value to this environment. That is, any abstract binding leads to an application while a definition
   * is ignored. This is congruent with the type generated by [[noo.compiler.elaboration.context.Environment#closeType(noo.compiler.evaluation.Value)]],
   * where definitions lead to `let` bindings in the type signature.
   * @param value The value which shall be applied to this environment.
   * @return The resulting application of the specified value to this environment.
   */
  def apply(value: Value): Elaboration[Value] =
    Conversion.apply(value, Spine.of(bindings.filterNot(_.isDefinition).map {
      binding => Argument.Default(binding.value, binding.idiom)
    }: _*))

  /**
   * Produces a string representation of this environment, where each binding is printed on a separate line.
   * When a binding is marked as hidden, an additional hint that this binding is not scope is printed at the
   * end of the corresponding line.
   * @return An elaboration that produces the string representation of this environment.
   */
  lazy val show: Elaboration[String] = {
    def show(bindings: Vector[Binding]): Elaboration[List[(String, Visibility)]] = bindings match {
      case Vector() =>
        Elaboration.pure(Nil)

      case binding +: bindings =>
        for {
          line <- binding match {
            case Binding.Definition(name, _, signature, value, visibility) =>
              for {
                signature <- PrettyPrinter.print(signature)
                value <- PrettyPrinter.print(value)
              } yield (s"  $name = $value : $signature  ", visibility)

            case Binding.Abstraction(name, _, signature, idiom, visibility) =>
              for {
                signature <- PrettyPrinter.print(signature)
              } yield idiom match {
                case Idiom.Explicit => (s"  $name : $signature  ", visibility)
                case Idiom.Implicit => (s"{ $name : $signature }", visibility)
              }
          }
          lines <- Elaboration.modify(_.modify(_.bind(binding)))(show(bindings))
        } yield line :: lines
    }

    Elaboration.withEnvironment(Environment.empty)(show(bindings)).map { lines =>
      val max = lines.map(_._1.length).maxOption.getOrElse(0)
      lines.map {
        case (line, Visible) => line
        case (line, Hidden) => line + " " * (max - line.length + 2) + s"(not in scope)"
      }.mkString("\n")
    }
  }

}

object Environment {

  /**
   * Create a new environment from the provided sequence of bindings.
   * @param bindings The sequence of bindings.
   * @return An environment that consists of the provided bindings.
   */
  def from(bindings: Seq[Binding]): Environment =
    empty.bind(bindings: _*)

  /** The empty (i.e., global or top-level) environment. */
  val empty: Environment =
    new Environment(Vector.empty, Map.empty)

}
