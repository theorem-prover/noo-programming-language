package noo.compiler.elaboration.context

/**
 * An algebraic data type that indicates the visibility of a binder. That is, a binder that is marked as
 * visible in some context can be referenced under this context. Otherwise, the reference is treated as any
 * other unknown identifier. Usually, a binder is marked as hidden when it is introduced as part of some
 * inference the user did not explicitly state (e.g., when we lift an expression under an implicit lambda).
 */
sealed trait Visibility extends Product with Serializable {

  def isVisible: Boolean

  def isHidden: Boolean

}

object Visibility {

  case object Hidden extends Visibility {

    override val isVisible: Boolean = false

    override val isHidden: Boolean = true

  }

  case object Visible extends Visibility {

    override val isVisible: Boolean = true

    override val isHidden: Boolean = false

  }

}
