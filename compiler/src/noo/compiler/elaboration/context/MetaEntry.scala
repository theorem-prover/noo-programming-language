package noo.compiler.elaboration.context

import noo.compiler.elaboration.Elaboration
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.Value
import noo.compiler.syntax.core.PrettyPrinter

sealed trait MetaEntry {

  def variable: MetaVariable

  def signature: Type

  def isSolved: Boolean

  def fold[A](empty: A)(f: (A, Value) => A): A

  def show: Elaboration[String]

}

object MetaEntry {

  case class Solved(variable: MetaVariable, value: Value, signature: Type) extends MetaEntry {

    override val isSolved: Boolean = true

    override def fold[A](empty: A)(f: (A, Type) => A): A = f(empty, value)

    override def show: Elaboration[String] =
      Elaboration.modify(_.clear) {
        for {
          signature <- PrettyPrinter.print(signature)
          value <- PrettyPrinter.print(value)
        } yield s"$variable : $signature = $value"
      }

  }

  case class Unsolved(variable: MetaVariable, signature: Type) extends MetaEntry {

    override val isSolved: Boolean = false

    override def fold[A](empty: A)(f: (A, Type) => A): A = empty

    override def show: Elaboration[String] =
      Elaboration.modify(_.clear) {
        for {
          signature <- PrettyPrinter.print(signature)
        } yield s"$variable : $signature"
      }

  }

}
