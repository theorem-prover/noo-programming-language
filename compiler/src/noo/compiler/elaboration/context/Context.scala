package noo.compiler.elaboration.context

import noo.compiler.elaboration.Elaboration
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.input.SourceSpan
import noo.compiler.syntax.core.{Declaration, Namespace}

import scala.collection.View
import scala.collection.mutable.ArrayBuffer
import scala.language.existentials

/**
 * The representation of an elaboration context.
 * @param location The location of this context in form of its corresponding source code span.
 * @param namespace The namespace this context lives in.
 * @param environment The environment of this context.
 * @param entries The mutable buffer that holds all holes that are in use.
 */
final class Context private(
  val location: SourceSpan,
  val namespace: Declaration with Namespace,
  val environment: Environment,
  private val entries: ArrayBuffer[MetaEntry]
) {

  /** A collection of all unsolved meta-variables in this context. */
  val holes: View[MetaVariable] = entries.view.filter(!_.isSolved).map(_.variable)

  /** Sets this context at the specified source code location. */
  def at(location: SourceSpan): Context = new Context(location, namespace, environment, entries)

  /** Sets this context at the specified namespace. */
  def at(namespace: Declaration with Namespace): Context = new Context(location, namespace, environment, entries)

  /** Modify the environment of this context using the specified function. */
  def modify(f: Environment => Environment): Context = new Context(location, namespace, f(environment), entries)

  /** Clears the environment such that this context can be used at the top level. */
  lazy val clear: Context = new Context(location, namespace, Environment.empty, entries)

  /** Returns the meta-entry for the specified meta-variable. */
  def get(variable: MetaVariable): MetaEntry =
    entries(variable.id)

  /** Sets the meta-entry for the associated meta-variable. */
  def set(entry: MetaEntry): this.type = {
    entries(entry.variable.id) = entry
    this
  }

  /**
   * Create a fresh hole in this context by closing over all of its bindings.
   * @param signature The type of the hole.
   * @return The meta-variable that identifies the new hole.
   */
  def fresh(signature: Type): Elaboration[MetaVariable] =
    environment.closeType(signature).map { signature =>
      val variable = MetaVariable(entries.size)
      entries += MetaEntry.Unsolved(variable, signature)
      variable
    }

}

object Context {

  val empty: Context = new Context(
    SourceSpan.Undefined,
    noo.compiler.syntax.core.Declaration.Module.root,
    Environment.empty,
    ArrayBuffer.empty
  )

}
