package noo.compiler.elaboration.context

import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Level, Value}
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.Spine

sealed trait Binding extends Product with Serializable {

  def name: String

  def signature: Type

  def idiom: Idiom

  def level: Level

  def value: Value

  def visibility: Visibility

  def isDefinition: Boolean

  final def isVisible: Boolean = visibility.isVisible

  final def isHidden: Boolean = visibility.isHidden

}

object Binding {

  /** A binding as it is introduced by a definition (i.e., `let`-definition). */
  case class Definition(name: String, level: Level, signature: Type, value: Value, visibility: Visibility) extends Binding {

    val isDefinition: Boolean = true

    val idiom: Idiom = Idiom.Explicit

  }

  /** A binding as it is introduced by a function abstraction (i.e., lambda- or pi-abstraction). */
  case class Abstraction(name: String, level: Level, signature: Type, idiom: Idiom, visibility: Visibility) extends Binding {

    val value: Value.Application.Local = Value.Application.Local(level, Spine.empty)

    val isDefinition: Boolean = false

  }

}
