package noo.compiler.elaboration.context

import noo.compiler.common.Names

import scala.language.implicitConversions

case class MetaVariable(id: Int) extends AnyVal {

  override def toString: String = Names.smallGreekNames.drop(id).head + "?"

}

object MetaVariable {

  implicit def toMetaVariable(id: Int): MetaVariable =
    MetaVariable(id)

}
