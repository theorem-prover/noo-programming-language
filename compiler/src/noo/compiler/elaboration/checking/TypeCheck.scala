package noo.compiler.elaboration.checking

import noo.compiler.common.Names
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility
import noo.compiler.error.{IdiomMismatch, UnknownParameter}
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.evaluation.Value.Thunk
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.Argument
import noo.compiler.syntax.core.Namespace.Entry
import noo.compiler.syntax.core.{Declaration, Signature, Term}
import noo.compiler.syntax.surface.Expression

object TypeCheck {

  def check(expression: Expression, expected: Type, justification: String): Elaboration[Term] = (expression, expected) match {
    case (Expression.Position(expression, span), expected) =>
      Elaboration.modify(_ at span)(check(expression, expected, justification))

    case (Expression.Hole, expected) =>
      Elaboration.fresh(expected)

    case (Expression.Lambda(lambdaParameter, lambdaDomain, lambdaBody), Value.Pi(piParameter, piDomain, piBody, piIdiom))
      if lambdaParameter.idiom == piIdiom && lambdaParameter.asSpecific.forall(_.name == piParameter) =>
      for {
        lambdaDomain <- check(lambdaDomain, Value.Type, "type signature")
        lambdaDomainValue <- Conversion.evaluate(lambdaDomain)
        constraint <- Constraint.from(lambdaDomainValue, piDomain, "type signature")
        _ <- Unification.simplify(constraint)
        body <- Elaboration.bind(lambdaParameter.argument, piDomain, piIdiom, Visibility.Visible) { binder =>
          for {
            expected <- piBody(binder.value)
            body <- check(lambdaBody, expected, justification)
          } yield body
        }
      } yield Term.Lambda(lambdaParameter.argument, lambdaDomain, body, lambdaParameter.idiom)

    case (expression, Value.Pi(parameter, head, body, Idiom.Implicit)) =>
      // In this case, the expected type is an implicit function type but the expression cannot "expect" implicit arguments, since
      // this is only possible with implicit lambdas (in which case we would have ended up in the previous case). So we introduce
      // a new implicit lambda around the expression.
      for {
        body <- Elaboration.bind(parameter, head, Idiom.Implicit, Visibility.Hidden) { binder =>
          body(binder.value).flatMap(check(expression, _, justification))
        }
        head <- Conversion.reify(head, unfold = false)
      } yield Term.Lambda(parameter, head, body, Idiom.Implicit)

    case (expression, expected) =>
      for {
        // We infer the leading implicits here, because the expected type cannot be an implicit pi type anymore. So, we do not
        // expect `expression` to be an implicit lambda and hence our only option is to infer all implicits.
        (term, inferred) <- infer(expression).flatMap((inferImplicitArgumentsToNeutral _).tupled)
        constraint <- Constraint.from(expected, inferred, justification)
        _ <- Unification.simplify(constraint)
      } yield term
  }

  def infer(expression: Expression): Elaboration[(Term, Type)] = expression match {
    case Expression.Position(expression, span) =>
      Elaboration.modify(_ at span)(infer(expression))

    case Expression.Hole =>
      for {
        holeTypeTerm <- Elaboration.fresh(Value.Type)
        holeType <- Conversion.evaluate(holeTypeTerm)
        holeTerm <- Elaboration.fresh(holeType)
      } yield (holeTerm, holeType)

    case Expression.Variable(qualifier) =>
      val head = if (qualifier.length == 1) Some(qualifier.elements.head) else None

      Elaboration.context.flatMap { context =>
        head.flatMap(context.environment.resolve) match {
          /*case Some(binder) if binder.isDefinition =>
            // TODO: control local unfolding
            Evaluation.reify(binder.value, unfold = false).map((_, binder.`type`))*/

          case Some(binding) =>
            Elaboration.pure((Term.Variable.Local(binding.level.toIndex(context.environment.level)), binding.signature))

          case None =>
            // Global variable
            Elaboration.namespace
              .flatMap(_.resolveOrFail(qualifier))
              .flatMap(_.as[Declaration with Signature]("object declaration"))
              .map {
                case Entry(declaration) => (Term.Variable.Global(declaration), declaration.signature)
              }
        }
      }

    case Expression.Application(function, argument) =>
      for {
        // First, we have to find out the actual function we wish to apply as well as its type.
        // This is necessary, as an application may trigger the application of implicit functions first.
        // Consider, for example, the application `id 2`. Here, we first infer an argument for the type parameter such
        // that we actually apply `id { Nat }` to `2`. In case of a specific argument (e.g., `f { a = Type }`), we only
        // infer implicit arguments until the parameter `a` is hit (i.e., excluding `a`).
        (function, functionType) <- argument match {
          case Argument.Default(_, Idiom.Implicit) =>
            infer(function)

          case Argument.Default(_, Idiom.Explicit) =>
            infer(function).flatMap((inferImplicitArguments _).tupled)

          case Argument.Specific(name, _) =>
            // Specific arguments are always implicit, since explicit arguments must be provided in order (otherwise
            // a dependency between two explicit arguments may be ignored). We may introduce specific explicit arguments
            // in the future, when we check that the specified argument does not occur in the set of free variables of
            // any skipped argument. For example, when we apply f : (a : A) -> ... -> (z : Z) -> τ with f (c = 2) we
            // yield a function f' : (a : A) -> (b: B) -> (d: D) -> ... -> (z : Z) -> τ. However, this is only possible
            // when c ∉ free(A) and c ∉ free(B).
            infer(function).flatMap((inferImplicitArgumentsUntil(name) _).tupled)
        }

        (domain, codomain) <- Conversion.force(functionType, unfold = true).flatMap {
          case Value.Pi(_, domain, codomain, idiom) if argument.idiom == idiom =>
            // We already know that the function type is a dependent function type
            Elaboration.pure((domain, codomain))

          case Value.Pi(_, _, _, idiom) =>
            // We inferred as much implicit arguments as possible, so this mismatch of the two idioms is an error
            Elaboration.fail(IdiomMismatch(idiom, argument.idiom) _)

          case functionType =>
            // In this case, the type is either not a (dependent) function type or its computation is blocked.
            // Hence, we create a new dependent function type (x: a? [environment]) -> b? [environment, x:a?[environment]] with
            // meta-variables for the (co)domain and unify it with the actual type.
            for {
              // 1. Create a fresh meta-variable that is applied to the current environment
              domain <- Elaboration.fresh(Value.Type)
              domainValue <- Conversion.evaluate(domain)

              // 2. Since the codomain may vary with the domain (i.e., the function type may be a dependent function type)
              //    we have to create the meta-variable 'b' under the context of 'a'.
              //    Note: As the body of this dependent function type is just the meta-variable 'b', the generated name below
              //    is never free in it.
              binderName <- Elaboration.context.map(context => Names.smallLatinNames.filterNot(context.environment.bindings.map(_.name).toSet.contains).head)
              codomain <- Elaboration.bind(binderName, domainValue, argument.idiom, Visibility.Hidden) { _ =>
                Elaboration.fresh(Value.Type)
              }

              // 3. Now we can create the expected dependent function type and unify it with the actual type.
              expectedType <- Conversion.evaluate(Term.Pi(binderName, domain, codomain, argument.idiom)).map(_.asInstanceOf[Value.Pi])
              constraint <- Constraint.from(expectedType, functionType, "application")
              _ <- Unification.simplify(constraint)
            } yield (expectedType.head, expectedType.body)
        }

        // Check if the provided argument of this application matches the expected function type.
        idiom = argument.idiom
        argument <- check(argument.argument, domain, s"application")
        applicationType <- Conversion.evaluate(argument).flatMap(codomain)
      } yield (Term.Application(function, Argument.Default(argument, idiom)), applicationType)

    case Expression.Annotation(expression, annotation) =>
      for {
        annotation <- check(annotation, Value.Type, "type annotation").flatMap(Conversion.evaluate)
        term <- check(expression, annotation, "type annotation")
      } yield (term, annotation)

    case Expression.Let(name, signature, right, body) =>
      for {
        signature <- check(signature, Value.Type, "type annotation")
        signatureValue <- Conversion.evaluate(signature)
        right <- check(right, signatureValue, "type annotation")
        context <- Elaboration.context
        (body, bodyType) <- Elaboration.define(name, signatureValue, new Thunk(Conversion.evaluate(right), context), Visibility.Visible) { _ =>
          infer(body)
        }
      } yield (Term.Let(name, signature, right, body), bodyType)

    case Expression.Lambda(parameter, head, body) =>
      for {
        parameterTypeTerm <- check(head, Value.Type, "parameter type")
        parameterTypeValue <- Conversion.evaluate(parameterTypeTerm)
        (bodyTerm, bodyTypeTerm) <- Elaboration.bind(parameter.argument, parameterTypeValue, parameter.idiom, Visibility.Visible) { _ =>
          for {
            (body, bodyType) <- infer(body)
            (body, bodyType) <- inferImplicitArgumentsToNeutral(body, bodyType)
            bodyTypeTerm <- Conversion.reify(bodyType, unfold = false)
          } yield (body, bodyTypeTerm)
        }

        piType <- Conversion.evaluate(Term.Pi(parameter.argument, parameterTypeTerm, bodyTypeTerm, parameter.idiom))
      } yield (Term.Lambda(parameter.argument, parameterTypeTerm, bodyTerm, parameter.idiom), piType)

    case Expression.Pi(parameter, domain, codomain, idiom) =>
      for {
        (parameter, visibility, domainTerm) <- parameter match {
          case Some(parameter) =>
            check(domain, Value.Type, "function domain type").map((parameter, Visibility.Visible, _))
          case None =>
            for {
              // TODO: The name may not be free in the body. At the moment this might only lead to display bugs (since we mark the binder as a signature binder).
              parameter <- Elaboration.context.map(context => Names.smallLatinNames.filterNot(context.environment.bindings.map(_.name).toSet.contains).drop((Math.random() * 1000).toInt).head)
              domainTerm <- check(domain, Value.Type, "function domain type")
            } yield (parameter, Visibility.Hidden, domainTerm)
        }

        domainValue <- Conversion.evaluate(domainTerm)
        codomainTerm <- Elaboration.bind(parameter, domainValue, idiom, visibility) { _ =>
          check(codomain, Value.Type, "function codomain type")
        }
      } yield (Term.Pi(parameter, domainTerm, codomainTerm, idiom), Value.Type)
  }

  def inferImplicitArguments(term: Term, `type`: Type): Elaboration[(Term, Type)] =
    inferImplicitArgumentsWhile(_ => true)(term, `type`)

  def inferImplicitArgumentsToNeutral(term: Term, `type`: Type): Elaboration[(Term, Type)] =
    term match {
      case Term.Lambda(_, _, _, Idiom.Implicit) =>
        // Most of the time we cannot solve a fresh meta-variable `a?` when we have an implicit application such as `({ x } => y) a?`
        Elaboration.pure((term, `type`))
      case _ =>
        inferImplicitArgumentsWhile(_ => true)(term, `type`)
    }

  def inferImplicitArgumentsUntil(parameter: String)(term: Term, `type`: Type): Elaboration[(Term, Type)] =
    inferImplicitArgumentsWhile(_.parameter != parameter)(term, `type`).flatMap {
      case (term, termType @ Value.Pi(`parameter`, _, _, Idiom.Implicit)) =>
        // We found the specified parameter, so everything is fine
        Elaboration.pure((term, termType))

      case _ =>
        // We inferred all leading implicit arguments but failed to find the desired parameter
        for {
          parameters <- Conversion.reify(`type`, unfold = false).map(Term.parameters)
          result <- Elaboration.fail(UnknownParameter(parameter, parameters) _)
        } yield result
    }

  private def inferImplicitArgumentsWhile(predicate: Value.Pi => Boolean)(term: Term, `type`: Type): Elaboration[(Term, Type)] =
    Conversion.force(`type`, unfold = true).flatMap {
      case termType @ Value.Pi(_, domain, codomain, Idiom.Implicit) if predicate(termType) =>
        // Infer the implicit argument for this dependent function type
        for {
          variable <- Elaboration.fresh(domain)
          variableValue <- Conversion.evaluate(variable)
          appliedTerm = Term.Application(term, Argument.Default(variable, Idiom.Implicit))
          appliedType <- codomain(variableValue)
          result <- inferImplicitArgumentsWhile(predicate)(appliedTerm, appliedType)
        } yield result

      case termType =>
        Elaboration.pure((term, termType))
    }

}
