package noo.compiler.elaboration.checking.solution

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.{Environment, MetaEntry}
import noo.compiler.error.TypeMismatch
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.syntax.core.PrettyPrinter

object Solving {

  /**
   * Solves the meta-variable that is part of the specified pattern using the specified solution. This operation can
   * fail if the pattern does not fall within Miller's pattern fragment and the solution depends on one of the non-linear
   * variables in the pattern.
   * @param pattern The left-hand side of the solution equation.
   * @param solution The right-hand side of the solution equation.
   * @param expectation Whether the left-hand side is the expected type (used for error reporting).
   * @param justification A reason that justifies why we expect the specified type.
   * @return On success the meta-variable is solved as a side-effect. Otherwise an error is returned.
   * @note Local definitions should be fully unfolded in the solution, as these are not defined by the pattern fragment
   *       of the meta-variable and thus lead to escaping variables.
   */
  def solve(pattern: Pattern, solution: Value, expectation: Boolean, justification: String): Elaboration[Unit] = pattern match {
    case simple: Pattern.Simple =>
      for {
        // This pattern is either a linear pattern fragment (as defined by Miller) or a non-linear one. To ensure that the
        // solution does not depend on variables in non-linear position, we first attempt to prune all variables in non-
        // linear position from the meta-variable's type. That is, we are not interested in the actual pruned type, but
        // only if this pruning is successful.
        signature <- Elaboration.context.map(_.get(pattern.variable).signature)
        _ <- Pruning.pruneType(pattern.variable, signature, simple.mask(simple.isUnique))
        // Now that we know that the meta-variable's signature does not depend on the non-linear variables, we can also
        // attempt to rename the solution. This operation can fail as well, when the solution depends on one of the non-
        // linear variables.
        solution <- Renaming.from(simple).rename(solution, unfold = false)
        // At this point, we know that neither the meta-variable's type signature nor the solution depend on one of the
        // non-linear variables. Consequently, we know that the solution is the most-general one and we can actually use
        // it, to solve the meta-variable now.
        (environment, _) <- Environment.empty.unload(signature)
        solution <- environment.close(solution).flatMap(Conversion.evaluate)
        _ <- Elaboration.context.map(_.set(MetaEntry.Solved(pattern.variable, solution, signature)))
      } yield ()

    case complex: Pattern.Complex =>
      // This is a type mismatch, because we cannot uniquely solve the meta-variable
      for {
        expected <- if (expectation) PrettyPrinter.print(Value.Application.Flexible(pattern.variable, pattern.arguments)) else PrettyPrinter.print(solution)
        actual <- if (expectation) PrettyPrinter.print(solution) else PrettyPrinter.print(Value.Application.Flexible(pattern.variable, pattern.arguments))
        result <- Elaboration.fail(TypeMismatch(expected, actual, Some(justification)) _)
      } yield result
  }

}
