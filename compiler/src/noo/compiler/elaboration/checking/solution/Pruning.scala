package noo.compiler.elaboration.checking.solution

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.elaboration.context.{Binding, Environment, MetaEntry, MetaVariable}
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.syntax.common.{Argument, Spine}
import noo.compiler.syntax.core.Term

object Pruning {

  /** A mask that can be used to prune arguments from a meta-variable, where `false` drops the argument and `true` keeps it. */
  type Mask = List[Boolean]

  def prune(variable: MetaVariable, mask: Mask): Elaboration[MetaVariable] =
    if (mask.forall(identity)) Elaboration.pure(variable) // No argument shall be pruned, so we just return the meta-variable as it is.
    else Elaboration.modify(_.clear) {
      for {
        context <- Elaboration.context
        entry = context.get(variable)
        // Given a meta-variable α? : Δ → τ, we unload Δ onto an empty environment (i.e., we interpret Δ as an environment)
        (environment, _) <- Environment.empty.unload(entry.signature)
        // We prune the type Δ → τ by removing all parameters from Δ for which the mask is set to `false`. The result is
        // a fresh type Δ' → τ where all de Brujin levels are properly renamed. This operation can fail, if some of the
        // parameters in Δ' or in the return type τ depend on some of the removed parameters (i.e., variable escape).
        signature <- pruneType(variable, entry.signature, mask)
        // Now, we can create a fresh variable β? : Δ' → τ ...
        fresh <- context.fresh(signature)
        // ... and apply it to the bindings in Δ that were not removed above. The result is an application β? dom(Δ') : τ
        // which is the solution for the original meta-variable.
        solution <- apply(environment.bindings, mask, Value.Application.Flexible(fresh, Spine.empty))
        // Here, we close β? dom(Δ') : τ such that we can use it as the solution to the original meta-variable.
        solution <- environment.close(solution)
        // Finally, we solve α? by α? = Δ => β? dom(Δ'). Future unification attempts with α? must therefore solve β?
        // instead, which depends on fewer parameters than α? now.
        _ = context.set(MetaEntry.Solved(variable, solution, entry.signature))
      } yield fresh
    }

  def pruneType(variable: MetaVariable, signature: Type, mask: Mask): Elaboration[Type] = {
    def pruneType(signature: Type, mask: Mask, renaming: Renaming): Elaboration[Term] = {
      mask match {
        case keep :: mask =>
          Conversion.force(signature, unfold = true).flatMap {
            case Value.Pi(parameter, head, body, idiom) if keep =>
              for {
                body <- renaming.bind(parameter, head, idiom) { case (binding, renaming) =>
                  body(binding.value).flatMap(pruneType(_, mask, renaming))
                }
                head <- renaming.rename(head, unfold = false)
              } yield Term.Pi(parameter, head, body, idiom)

            case Value.Pi(parameter, head, body, idiom) =>
              Elaboration.bind(parameter, head, idiom, Visible) {
                binding => body(binding.value).flatMap(pruneType(_, mask, renaming))
              }

            case _ =>
              impossible("a pruning mask must be well-typed")
          }

        case Nil =>
          renaming.rename(signature, unfold = false)
      }
    }

    Elaboration.modify(_.clear) {
      pruneType(signature, mask, Renaming.empty(variable)).flatMap(Conversion.evaluate)
    }
  }

  private def apply(bindings: Vector[Binding], mask: Mask, value: Value): Elaboration[Value] = (bindings, mask) match {
    case (_ +: bindings, false :: mask) =>
      // We ignore this binding, as it was pruned from the fresh meta-variable
      apply(bindings, mask, value)

    case (binding +: bindings, mask) =>
      for {
        value <- Conversion.apply(value, Argument.Default(binding.value, binding.idiom))
        // We use `drop` here, since it does not fail when `mask` is already empty. An empty mask means that all
        // remaining bindings must be applied to the meta-variable (we always fully apply meta-variables).
        value <- apply(bindings, mask.drop(1), value)
      } yield value
  }

}
