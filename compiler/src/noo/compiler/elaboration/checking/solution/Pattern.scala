package noo.compiler.elaboration.checking.solution

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.MetaVariable
import noo.compiler.evaluation.{Conversion, Level, Value}
import noo.compiler.syntax.common.Spine.:+
import noo.compiler.syntax.common.{Argument, Spine}

/**
 * A representation of the left-hand side of a meta-variable equation. That is, given a meta-variable `a?` that is
 * applied to a spine `x ... z` (where `x ... z` is a sequence of arbitrary expressions), and the solution `t`, we call
 * the left-hand side `a? x ... z` of the equation `a? x ... z = t` the pattern in this regard. This is in contrast to
 * Miller's so-called pattern fragment.
 */
sealed abstract class Pattern {

  /** The meta-variable of the pattern. */
  def variable: MetaVariable

  /** The arguments of the pattern. */
  def arguments: Spine[Value]

  /** Whether this pattern is a simple one (i.e., all arguments are variables). */
  def isSimple: Boolean

  /** Whether this pattern is a complex one (i.e., the arguments are arbitrary expressions). */
  def isComplex: Boolean

}

object Pattern {

  /**
   * A simple pattern whose arguments consist only of bound variables.
   */
  final class Simple private[Pattern] (val variable: MetaVariable, val variables: Spine[Level], duplicates: Set[Level]) extends Pattern {

    lazy val arguments: Spine[Value] = variables.map(Value.Application.Local(_, Spine.empty))

    override val isSimple: Boolean = true

    override val isComplex: Boolean = false

    /** Whether this pattern is linear (i.e., all variables are distinct). */
    val isLinear: Boolean = duplicates.isEmpty

    /** Whether the provided variable is unique for this pattern. */
    def isUnique(variable: Level): Boolean = !duplicates.contains(variable)

    /**
     * Create a mask for pruning on basis of the provided predicate.
     * @param predicate The predicate which decides whether a variable should be pruned or not.
     * @return A mask that can be used for pruning.
     */
    def mask(predicate: Level => Boolean): List[Boolean] =
      variables.toList.map(variable => predicate(variable.argument))

  }

  /**
   * A complex pattern whose arguments consist of arbitrary expressions.
   */
  final class Complex private[Pattern] (val variable: MetaVariable, val arguments: Spine[Value]) extends Pattern {

    override val isSimple: Boolean = false

    override val isComplex: Boolean = true

  }

  /**
   * Simplify the provided pattern. That is, if all arguments of the pattern are bound variables a simple pattern is
   * returned. Otherwise, a complex pattern is returned.
   * @param pattern The pattern that shall be simplified.
   * @return The simplified pattern.
   */
  def simplify(pattern: Pattern): Elaboration[Pattern] = pattern match {
    case simple: Simple =>
      Elaboration.pure(simple)

    case complex: Complex =>
      def from(arguments: Spine[Value]): Elaboration[Option[(Spine[Level], Set[Level])]] = arguments match {
        case arguments :+ Argument(argument, idiom) =>
          from(arguments).flatMap {
            case None =>
              Elaboration.pure(None)

            case Some((variables, duplicates)) =>
              Conversion.force(argument, unfold = true).map {
                case Value.Application.Local(level, Spine.empty) =>
                  if (variables.exists(_ == level)) {
                    Some((variables :+ Argument.Default(level, idiom), duplicates + level))
                  } else {
                    Some((variables :+ Argument.Default(level, idiom), duplicates))
                  }

                case _ =>
                  None
              }
          }

        case Spine.empty =>
          Elaboration.pure(Some((Spine.empty, Set.empty)))
      }

      from(complex.arguments).map {
        case Some((variables, duplicates)) =>
          new Simple(complex.variable, variables, duplicates)
        case None =>
          complex
      }
  }

  /**
   * Create a pattern from the given meta-variable and its arguments. If all arguments are bound variables, this method
   * returns a simple, otherwise a complex pattern.
   * @param variable The meta-variable of the pattern.
   * @param arguments The arguments of the pattern.
   * @return A simple pattern in case all arguments are bound variables, otherwise a complex pattern.
   */
  def from(variable: MetaVariable, arguments: Spine[Value]): Elaboration[Pattern] =
    simplify(new Complex(variable, arguments))

}
