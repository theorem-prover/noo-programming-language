package noo.compiler.elaboration.checking.solution

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.elaboration.context.{Binding, MetaVariable}
import noo.compiler.error.{EscapingVariable, RecursiveSolution}
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Level, Value}
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.common.Spine.:+
import noo.compiler.syntax.common.{Argument, Spine}
import noo.compiler.syntax.core.Term

import scala.collection.immutable.IntMap

/**
 * A class that represents a partial renaming for meta-variable solutions. That is, given a solution this class is
 * able to "move" it from the solution environment to the environment of the meta-variable by renaming the bound
 * variables of the solution. This operation can fail, however, when a bound variable in the solution environment
 * cannot be mapped (hence this is a partial renaming).
 * @param variable The meta-variable that shall be solved.
 * @param target The level of the environment of the meta-variable.
 * @param mapping The partial renaming from source levels (i.e., bound variables that reside in the solution environment)
 *                to target levels (i.e., bound variables that reside in the environment of the meta-variable). As an
 *                invariant `target` must always be equal or greater than the size of this mapping.
 */
final class Renaming private(
  val variable: MetaVariable,
  target: Level,
  mapping: IntMap[Level]
) {

  def rename(value: Value, unfold: Boolean): Elaboration[Term] = Conversion.force(value, unfold).flatMap {
    case Value.Type =>
      Elaboration.pure(Term.Type)

    case global: Value.Application.Global =>
      rename(Term.Variable.Global(global.declaration), global.arguments, unfold)

    case Value.Application.Local(source, spine) if mapping.isDefinedAt(source.value) =>
      val target = mapping(source.value)
      rename(Term.Variable.Local(target.toIndex(this.target)), spine, unfold)

    case Value.Application.Local(source, _) =>
      for {
        environment <- Elaboration.environment
        name = environment.get(source).name
        result <- Elaboration.fail(EscapingVariable(name) _)
      } yield result

    case Value.Application.Flexible(variable, _) if variable == this.variable =>
      Elaboration.fail(RecursiveSolution(variable.toString) _)

    case Value.Application.Flexible(variable, spine) =>
      for {
        pattern <- Pattern.from(variable, spine)
        result <- rename(pattern, unfold)
      } yield result

    case Value.Lambda(parameter, domain, body, idiom) =>
      for {
        body <- rename(parameter, domain, idiom, body, unfold)
        domain <- rename(domain, unfold)
      } yield Term.Lambda(parameter, domain, body, idiom)

    case Value.Pi(parameter, domain, body, idiom) =>
      for {
        body <- rename(parameter, domain, idiom, body, unfold)
        domain <- rename(domain, unfold)
      } yield Term.Pi(parameter, domain, body, idiom)
  }

  private def rename(term: Term, spine: Spine[Value], unfold: Boolean): Elaboration[Term] =
    spine match {
      case spine :+ argument =>
        for {
          term <- rename(term, spine, unfold)
          argument <- argument.mapM(rename(_, unfold))
        } yield Term.Application(term, argument)

      case Spine.empty =>
        Elaboration.pure(term)
    }

  private def rename(pattern: Pattern, unfold: Boolean): Elaboration[Term] = {
    pattern match {
      case simple: Pattern.Simple =>
        // A spine of the renamed variables (if a variable escapes its scope `None` is returned instead).
        val variables = simple.variables.map { variable =>
          if (mapping.isDefinedAt(variable.value)) {
            Some(mapping(variable.value).toIndex(target))
          } else {
            None
          }
        }

        for {
          // First we try to remove all variables which escape their scope from the meta-variable. The result is a fresh
          // meta-variable which does not depend on these escaping variables anymore.
          variable <- Pruning.prune(simple.variable, variables.toList.map(_.argument.isDefined))
          // Now we can apply the above meta-variable to the renamed variables.
          result = variables.foldLeft[Term](Term.Variable.Flexible(variable)) {
            case (function, Argument.Default(Some(index), idiom)) =>
              Term.Application(function, Argument.Default(Term.Variable.Local(index), idiom))
            case (function, Argument.Specific(name, Some(index))) =>
              Term.Application(function, Argument.Specific(name, Term.Variable.Local(index)))
            case (function, _) =>
              // In this case, the argument is `None` which means that this was a variable that escapes its scope. As a
              // result, it was pruned from the original meta-variable and hence must not be applied to the new, pruned
              // meta-variable.
              function
          }
        } yield result

      case complex: Pattern.Complex =>
        rename(Term.Variable.Flexible(complex.variable), complex.arguments, unfold)
    }
  }

  private def rename(parameter: String, domain: Type, idiom: Idiom, body: Value => Elaboration[Value], unfold: Boolean): Elaboration[Term] =
    bind(parameter, domain, idiom) { case (binding, renaming) =>
      body(binding.value).flatMap(renaming.rename(_, unfold))
    }

  /**
   * Introduce and map the specified parameter in the source and target environment. The resulting renaming and source
   * environment binding are passed to the continuation, which is executed within the new source environment.
   * @param parameter The parameter name.
   * @param domain The type of the parameter.
   * @param idiom The idiom of the parameter.
   * @param continuation The continuation that is executed in the new source environment.
   * @tparam A The result type of the continuation.
   * @return Whatever the continuation returns.
   */
  def bind[A](parameter: String, domain: Type, idiom: Idiom)(continuation: (Binding, Renaming) => Elaboration[A]): Elaboration[A] =
    Elaboration.bind(parameter, domain, idiom, Visible) { binding =>
      val renaming = new Renaming(this.variable, this.target + 1, this.mapping + (binding.level.value -> this.target))
      continuation(binding, renaming)
    }

}

object Renaming {

  /**
   * Creates an empty renaming for the specified meta-variable.
   * @param variable The meta-variable of the renaming.
   * @return
   */
  def empty(variable: MetaVariable): Renaming = new Renaming(variable, 0, IntMap.empty)

  /**
   * Create a partial renaming from the provided simple pattern.
   * @param pattern The simple pattern the meta-variable is applied to.
   * @return The partial renaming that allows to rename values such that they can be used in the meta-variable's
   *         solution environment.
   */
  def from(pattern: Pattern.Simple): Renaming = {
    // We initialize the mapping with all the variables in the pattern (i.e., they are renamed to their respective
    // de Brujin index).
    val mapping = IntMap(pattern.variables.toList.zipWithIndex.flatMap {
      case (Argument(source, _), target) if pattern.isUnique(source) =>
        // We only rename linear variables, which has the effect that renaming non-linear variables throws an error
        // stating that the respective variable escapes its scope. That is, if a solution does not depend on the non-
        // linear variables of the meta-variable, it is still accepted as solution.
        Some(source.value -> Level(target))
      case _ =>
        None
    }: _*)
    // The initial target size is the number of linear variables we renamed above
    new Renaming(pattern.variable, mapping.size, mapping)
  }

}
