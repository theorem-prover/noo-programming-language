package noo.compiler.elaboration.checking

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.checking.solution.{Pattern, Pruning, Solving}
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.error.TypeMismatch
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.syntax.common.Argument
import noo.compiler.syntax.core.PrettyPrinter

object Unification {

  def simplify(constraint: Constraint): Elaboration[Set[Constraint]] =
    Elaboration.withEnvironment(constraint.environment) {
      for {
        expected <- Conversion.force(constraint.expected, unfold = true)
        actual <- Conversion.force(constraint.actual, unfold = true)
        result <- (expected, actual) match {
          case (Value.Type, Value.Type) =>
            Elaboration.pure(Set.empty[Constraint])

          case (Value.Lambda(parameter, domain, body, idiom), actual) =>
            Elaboration.bind(parameter, domain, idiom, Visible) { binder =>
              for {
                expected <- body(binder.value)
                actual <- Conversion.apply(actual, Argument.Default(binder.value, idiom))
                constraint <- Constraint.from(expected, actual, constraint.justification)
                constraints <- simplify(constraint)
              } yield constraints
            }

          case (expect, Value.Lambda(parameter, domain, body, idiom)) =>
            Elaboration.bind(parameter, domain, idiom, Visible) { binder =>
              for {
                expected <- Conversion.apply(expect, Argument.Default(binder.value, idiom))
                actual <- body(binder.value)
                constraint <- Constraint.from(expected, actual, constraint.justification)
                constraints <- simplify(constraint)
              } yield constraints
            }

          case (Value.Pi(parameter, expectedDomain, expectedBody, expectedIdiom), Value.Pi(_, actualDomain, actualBody, actualIdiom)) if expectedIdiom == actualIdiom =>
            for {
              constraint <- Constraint.from(expectedDomain, actualDomain, constraint.justification)
              constraints <- simplify(constraint)
              constraints <- Elaboration.bind(parameter, expectedDomain, expectedIdiom, Visible) { binder =>
                for {
                  expected <- expectedBody(binder.value)
                  actual <- actualBody(binder.value)
                  constraint <- Constraint.from(expected, actual, constraint.justification)
                  constraints <- simplify(constraint).map(_ ++ constraints)
                } yield constraints
              }
            } yield constraints

          case (Value.Application.Local(expectedLevel, expectedSpine), Value.Application.Local(actualLevel, actualSpine)) if expectedLevel == actualLevel =>
            for {
              constraints <- Constraint.from(expectedSpine, actualSpine, constraint.justification)
              constraints <- constraints.map(simplify).sequence
            } yield constraints.flatten

          case (Value.Application.Global.Declaration(expectedDeclaration, expectedSpine), Value.Application.Global.Declaration(actualDeclaration, actualSpine)) if expectedDeclaration == actualDeclaration =>
            for {
              constraints <- Constraint.from(expectedSpine, actualSpine, constraint.justification)
              constraints <- constraints.map(simplify).sequence
            } yield constraints.flatten

          case (Value.Application.Global.Definition(expectedDefinition, expectedSpine), Value.Application.Global.Definition(actualDefinition, actualSpine)) if expectedDefinition == actualDefinition =>
            // If these definitions could be unfolded, `force` would have done so. Hence, we can only unify the arguments
            // in case this is the exact same definition.
            // TODO: Check for eta-equality of non-recursive negatively defined types. That is, although the definitions
            //       cannot be fully unfolded here, it might be the case that we can unfold them "far enough" to reach a
            //       negatively defined type. If this is the case, we can apply eta-conversion to check if both types
            //       are unifiable under all projections of the negatively defined type.
            for {
              constraints <- Constraint.from(expectedSpine, actualSpine, constraint.justification)
              constraints <- constraints.map(simplify).sequence
            } yield constraints.flatten

          case (Value.Application.Flexible(a, expectedSpine), Value.Application.Flexible(b, actualSpine)) if a == b =>
            for {
              environment <- Elaboration.environment
              intersection = Constraint.Intersection(a, expectedSpine, actualSpine, constraint.justification)(environment)
              constraints <- simplify(intersection)
            } yield constraints

          case (expected: Value.Application.Flexible, actual: Value.Application.Flexible) =>
            for {
              environment <- Elaboration.environment
              flexible = Constraint.Flexible(expected, actual, constraint.justification)(environment)
              constraints <- simplify(flexible)
            } yield constraints

          case (Value.Application.Flexible(a, spine), b) =>
            for {
              pattern <- Pattern.from(a, spine)
              _ <- Solving.solve(pattern, b, expectation = true, constraint.justification)
            } yield Set.empty[Constraint]

          case (a, Value.Application.Flexible(b, spine)) =>
            for {
              pattern <- Pattern.from(b, spine)
              _ <- Solving.solve(pattern, a, expectation = false, constraint.justification)
            } yield Set.empty[Constraint]

          case (expected, actual) =>
            for {
              expected <- PrettyPrinter.print(expected)
              actual <- PrettyPrinter.print(actual)
              result <- Elaboration.fail(TypeMismatch(expected, actual, Some(constraint.justification)) _)
            } yield result
        }
      } yield result
    }

  private def simplify(constraint: Constraint.Flexible): Elaboration[Set[Constraint]] =
    Elaboration.withEnvironment(constraint.environment) {
      // We try to solve the deeper meta-variable with the shallow one first, as the shallow one would require us to do less pruning in the future
      val (a, b) = if (constraint.actual.arguments.size > constraint.expected.arguments.size) (constraint.actual, constraint.expected)
                   else (constraint.expected, constraint.actual)

      for {
        pattern <- Pattern.from(a.variable, a.arguments)
        _ <- pattern match {
          case pattern: Pattern.Simple =>
            Solving.solve(pattern, b, expectation = true, constraint.justification)
          case _ =>
            // The first spine does not fall within Miller's pattern fragment, so we fall back to solve the shallow with the deeper meta-variable
            Pattern.from(b.variable, b.arguments).flatMap {
              case pattern: Pattern.Simple =>
                Solving.solve(pattern, a, expectation = false, constraint.justification)
              case _ =>
                // Both spines do not fall within Miller's pattern fragment, so we cannot solve the unification problem and fail here.
                for {
                  expected <- PrettyPrinter.print(constraint.expected)
                  actual <- PrettyPrinter.print(constraint.actual)
                  result <- Elaboration.fail(TypeMismatch(expected, actual, Some(constraint.justification)) _)
                } yield result
            }
        }
      } yield Set.empty[Constraint]
    }

  /**
   * Whenever a meta-variable `α? : Δ → τ` is applied to two spines `p : Δ` and `q : Δ` a general solution for `α?` can
   * only depend on the intersection between `p` and `q`. So, for example, if `a?` is yet unsolved and we encounter the
   * unification problem `a? x y z = a? h y i` then `a?` can be solved as `a? = \_ . \y . \_ => b? y` where `b?` is a
   * fresh meta-variable. That is, we prune the meta-variable `α?` by keeping only those variables that overlap.
   * @param intersection The intersection constraint.
   * @return A set of new constraints.
   */
  private def simplify(intersection: Constraint.Intersection): Elaboration[Set[Constraint]] =
    Elaboration.withEnvironment(intersection.environment) {
      for {
        expected <- Pattern.from(intersection.variable, intersection.expectedSpine)
        actual <- Pattern.from(intersection.variable, intersection.actualSpine)
        constraints <- (expected, actual) match {
          case (expected: Pattern.Simple, actual: Pattern.Simple) =>
            if (expected.variables.size != actual.variables.size) impossible // TODO: really?
            else {
              // Here, we compute the intersection of the two spines. Only those variables that occur in both spines
              // will not be pruned.
              val mask = expected.variables.toList.zip(actual.variables.toList).map {
                case (Argument(a, _), Argument(b, _)) => a == b
              }
              // Using the above mask we can prune the meta-variable such that future solutions can only depend on the
              // common variables.
              Pruning.prune(intersection.variable, mask).map(_ => Set.empty[Constraint])
            }
          case (_, _) =>
            // One of the spines is not a simple pattern, so we simplify the spines as usual
            for {
              constraints <- Constraint.from(intersection.expectedSpine, intersection.actualSpine, intersection.justification)
              constraints <- constraints.map(simplify).sequence
            } yield constraints.flatten
        }
      } yield constraints
    }

}
