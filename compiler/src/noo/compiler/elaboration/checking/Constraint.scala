package noo.compiler.elaboration.checking

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.{Environment, MetaVariable}
import noo.compiler.evaluation.Value
import noo.compiler.syntax.common.{Argument, Spine}
import noo.compiler.syntax.common.Spine.:+

sealed trait Constraint {

  def expected: Value

  def actual: Value

  def justification: String

  def environment: Environment

}

object Constraint {

  def from(expected: Value, actual: Value, justification: String): Elaboration[Constraint] =
    Elaboration.environment.map(Unify(expected, actual, justification))

  def from(expected: Spine[Value], actual: Spine[Value], justification: String): Elaboration[Set[Constraint]] =
    (expected, actual) match {
      case (expected :+ Argument(x, _), actual :+ Argument(y, _)) =>
        for {
          constraints <- from(expected, actual, justification)
          constraint <- from(x, y, justification)
        } yield constraints + constraint

      case (Spine.empty, Spine.empty) =>
        Elaboration.pure(Set.empty)
    }

  case class Unify(
    expected: Value,
    actual: Value,
    justification: String
  )(val environment: Environment) extends Constraint

  case class Flexible(
    expected: Value.Application.Flexible,
    actual: Value.Application.Flexible,
    justification: String
  )(val environment: Environment) extends Constraint

  case class Intersection(
    variable: MetaVariable,
    expectedSpine: Spine[Value],
    actualSpine: Spine[Value],
    justification: String
  )(val environment: Environment) extends Constraint {

    val expected: Value = Value.Application.Flexible(variable, expectedSpine)

    val actual: Value = Value.Application.Flexible(variable, actualSpine)

  }

}