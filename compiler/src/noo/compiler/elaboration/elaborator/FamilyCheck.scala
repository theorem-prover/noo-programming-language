package noo.compiler.elaboration.elaborator

import noo.compiler.common.impossible
import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.{Binding, Visibility}
import noo.compiler.error._
import noo.compiler.evaluation.{Conversion, Level}
import noo.compiler.modifier.Polarity
import noo.compiler.syntax.core.{Declaration, PrettyPrinter, Term}

import scala.annotation.tailrec

trait FamilyCheck { this: DeclarationElaborator =>

  // TODO: Exclude meta-variables (family, constructor and projection signatures must be complete when finally checking their validity)

  def checkFamilySignature(family: Declaration.Family, `type`: Term, polarity: Polarity, level: Level = 0): Elaboration[Unit] = {
    `type` match {
      case Term.Pi(_, _, _, _) if family.polarity.isNegative && level.value >= family.parameters.size =>
        // We do not allow indices for negative families at the moment at the moment
        Elaboration.fail(IllegalNegativeFamilySignature(family.qualifier) _)

      case Term.Pi(_, _, body, _) =>
        checkFamilySignature(family, body, polarity, level + 1)

      case Term.Type =>
        Elaboration.unit

      case _ =>
        for {
          typeString <- PrettyPrinter.print(`type`)
          result <- Elaboration.fail(IllegalFamilySignature(family.qualifier, polarity, typeString) _)
        } yield result
    }
  }

  def checkConstructorSignature(family: Declaration.Family, constructor: String, `type`: Term): Elaboration[Unit] = {
    def go(`type`: Term, parameters: List[Binding]): Elaboration[Unit] = `type` match {
      case Term.Pi(parameter, head, body, idiom) =>
        for {
          _ <- checkStrictPositivity(family, constructor, head)
          head <- Conversion.evaluate(head)
          _ <- Elaboration.bind(parameter, head, idiom, Visibility.Visible) {
            binder => go(body, binder :: parameters)
          }
        } yield ()
      case _ =>
        checkConstructorReturnType(family, parameters.reverse, constructor, `type`)
    }

    go(`type`, Nil)
  }


  @tailrec
  private def checkStrictPositivity(family: Declaration.Family, constructor: String, parameterType: Term): Elaboration[Unit] =
    parameterType match {
      case Term.Pi(_, head, _, _) if head.free.contains(family.qualifier) =>
        Elaboration.fail(StrictPositivity(family.qualifier, constructor, s"to the left of the arrow in the parameter type `${PrettyPrinter.print(parameterType)}`") _)

      case Term.Pi(_, _, body, _) =>
        checkStrictPositivity(family, constructor, body)

      case Term.Type | _: Term.Variable.Local | _: Term.Variable.Global =>
        Elaboration.unit

      case Term.Application(_, argument) if argument.argument.free.contains(family.qualifier) =>
        // The inductive family may not occur as an argument to a function
        Elaboration.fail(StrictPositivity(family.qualifier, constructor, s"as an argument in the parameter type `${PrettyPrinter.print(parameterType)}`") _)

      case Term.Application(function, _) =>
        checkStrictPositivity(family, constructor, function)

      case _ =>
        impossible
    }

  private def checkConstructorReturnType(family: Declaration.Family, parameters: List[Binding], constructor: String, returnType: Term): Elaboration[Unit] = {
    def check(`type`: Term): Elaboration[List[Binding]] = `type` match {
      case Term.Application(_, argument) if argument.argument.free.contains(family.qualifier) =>
        Elaboration.fail(StrictPositivity(family.qualifier, constructor, s"as an argument in the return type `${PrettyPrinter.print(returnType)}`") _)

      case Term.Application(function, argument) =>
        for {
          parameters <- check(function)
          // TODO: Check that any occurrence of the family type is applied to the parameters
          /*result <- parameters match {
            case parameter :: parameters =>
              for {
                argument <- Evaluation.evaluate(argument.argument)
                _ <- TypeCheck.unify(parameter.value, argument, "family parameter")
              } yield parameters

            case Nil =>
              Elaboration.pure(Nil)
          }*/
        } yield parameters

      case Term.Variable.Global(`family`) =>
        Elaboration.pure(parameters)

      case _ =>
        for {
          returnTypeString <- PrettyPrinter.print(returnType)
          result <- Elaboration.fail(IllegalConstructorSignature(family.qualifier, constructor, returnTypeString) _)
        } yield result
    }

    check(returnType).void
  }

  def checkProjectionSignature(family: Declaration.Family, projection: String, projectionType: Term): Elaboration[Unit] = {
    @tailrec
    def isFamilyApplication(term: Term): Boolean =
      term match {
        case Term.Variable.Global(`family`) => true
        // TODO: It is also important that the family is *fully* applied (maybe switch to values here)
        case Term.Application(function, _) => isFamilyApplication(function)
        case _ => false
      }

    @tailrec
    def check(`type`: Term): Elaboration[Unit] =
      `type` match {
        case Term.Pi(_, head, _, _) if isFamilyApplication(head) =>
          Elaboration.unit
        case Term.Pi(_, _, body, _) =>
          check(body)
        case _ =>
          Elaboration.fail(IllegalProjectionSignature(family.qualifier, projection) _)
      }

    check(projectionType)
  }

}
