package noo.compiler.elaboration.elaborator

import noo.compiler.elaboration.Elaboration
import noo.compiler.elaboration.context.Visibility
import noo.compiler.error.UnknownParameter
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.modifier.Idiom
import noo.compiler.syntax.core.Term

object Elaborator extends DeclarationElaborator {

  def introduceImplicitsUntil(needle: String, goal: Type)(continuation: Type => Elaboration[Term]): Elaboration[Term] = Conversion.force(goal, unfold = true).flatMap {
    case goal @ Value.Pi(`needle`, _, _, Idiom.Implicit) =>
      continuation(goal)

    case Value.Pi(parameter, head, body, Idiom.Implicit) =>
      for {
        body <- Elaboration.bind(parameter, head, Idiom.Implicit, Visibility.Visible) { binder =>
          for {
            goal <- body(binder.value)
            body <- introduceImplicitsUntil(needle, goal)(continuation)
          } yield body
        }
        head <- Conversion.reify(head, unfold = false)
      } yield Term.Lambda(parameter, head, body, Idiom.Implicit)

    case _ =>
      for {
        implicits <- Elaboration.context.map(_.environment.bindings.view.filter(_.isHidden).map(_.name))
        result <- Elaboration.fail(UnknownParameter(needle, implicits.toList) _)
      } yield result
  }

  def introduceImplicits(goal: Type)(continuation: Type => Elaboration[Term]): Elaboration[Term] = Conversion.force(goal, unfold = true).flatMap {
    case Value.Pi(parameter, head, body, Idiom.Implicit) =>
      for {
        body <- Elaboration.bind(parameter, head, Idiom.Implicit, Visibility.Visible) { binder =>
          for {
            goal <- body(binder.value)
            body <- introduceImplicits(goal)(continuation)
          } yield body
        }
        head <- Conversion.reify(head, unfold = false)
      } yield Term.Lambda(parameter, head, body, Idiom.Implicit)


    case goal =>
      continuation(goal)
  }

}
