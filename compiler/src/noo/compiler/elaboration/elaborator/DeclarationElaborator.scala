package noo.compiler.elaboration.elaborator

import noo.compiler.common.{StringExtension, impossible}
import noo.compiler.elaboration.Elaboration.IterableExtension1
import noo.compiler.elaboration.checking.TypeCheck
import noo.compiler.elaboration.context.Context
import noo.compiler.elaboration.context.Visibility.Visible
import noo.compiler.elaboration.{Elaboration, matching}
import noo.compiler.error.{DuplicateDeclaration, Error, IllegalScope}
import noo.compiler.evaluation.Conversion.Type
import noo.compiler.evaluation.{Conversion, Value}
import noo.compiler.modifier.Polarity.{Negative, Positive}
import noo.compiler.modifier.{Idiom, Modifier, Polarity, Visibility}
import noo.compiler.syntax.common.{Argument, Qualifier}
import noo.compiler.syntax.core.Namespace.Entry
import noo.compiler.syntax.core._
import noo.compiler.syntax.surface.Declaration.Import.Pattern
import noo.compiler.syntax.surface.Expression
import noo.compiler.syntax.{core, surface}

import scala.collection.mutable

trait DeclarationElaborator extends FamilyCheck { this: Elaborator.type =>

  def elaborate(declarations: List[surface.Declaration], defaultScope: core.Declaration with Namespace): Elaboration[Unit] = {
    (context: Context) =>
      val errors: mutable.ArrayBuffer[Error] = mutable.ArrayBuffer.empty

      do {
        context.namespace.reset()
        errors.clear()

        for (declaration <- declarations) {
          elaborate(declaration, defaultScope).elaborate(context) match {
            case Left(error) =>
              errors += error
            case _ =>
              ()
          }
        }
      } while (context.namespace.isDirty && errors.nonEmpty)

      if (errors.nonEmpty) {
        Left(errors.head)
      } else {
        Right(())
      }
  }

  def elaborate(declaration: surface.Declaration, defaultScope: core.Declaration with Namespace): Elaboration[Unit] =
    Elaboration.at(declaration.span) {
      declaration match {
        case surface.Declaration.Module(modifiers, qualifier, _, declarations) =>
          def declare(qualifier: Qualifier, scope: Declaration with Namespace): Elaboration[Declaration with Namespace] = qualifier match {
            case Qualifier.Empty =>
              Elaboration.namespace

            case Qualifier.Selection(base, member) =>
              declare(base, scope).flatMap { parent =>
                parent.get(member) match {
                  case Some(entry@Entry(module: core.Declaration.Module)) if module.qualifier == parent.qualifier + member =>
                    // This is a module that is a real child of this module, so we can weaken the visibility here
                    if (entry.scope.isChildOf(scope)) {
                      // The existing entry is more restricted, so we have to weaken it
                      val newEntry = module.self.link(member, scope)
                      parent.set(member, newEntry)
                      Elaboration.pure(module)
                    } else {
                      // The current scope is more restricted, so we leave it as it is
                      Elaboration.pure(module)
                    }

                  case Some(other) =>
                    Elaboration.fail(DuplicateDeclaration(core.Declaration.Module(member, Some(parent)).self.link(member, scope), other, parent) _)

                  case None =>
                    val module = core.Declaration.Module(member, Some(parent))
                    parent.declare(module.self.link(member, scope)).map(_.declaration)
                }
              }
          }

          for {
            scope <- getScopeBy(modifiers, defaultScope)
            module <- declare(qualifier, scope)
            _ = declaration.instance = Some(module)
            _ <- Elaboration.withNamespace(module) {
              elaborate(declarations, scope)
            }
          } yield ()

        case surface.Declaration.Axiom(modifiers, name, _, signature) =>
          Elaboration.when(declaration.instance.isEmpty) {
            for {
              parent <- Elaboration.namespace
              scope <- getScopeBy(modifiers, defaultScope)
              signatureValue <- TypeCheck.check(signature, Value.Type, "axiom signature").flatMap(Conversion.evaluate)
              parameters <- getParameterNames(signatureValue)
              axiom = core.Declaration.Axiom(name, signatureValue, parameters, Some(parent))
              _ <- parent.declare(axiom.self.link(name, scope))
              signatureString <- PrettyPrinter.print(signatureValue)
              _ = println(s"[info] assumed ${axiom.qualifier} : $signatureString")
            } yield declaration.instance = Some(axiom)
          }

        case surface.Declaration.Definition(modifiers, name, span, signature, Left(expression)) =>
          Elaboration.when(declaration.instance.isEmpty) {
            for {
              parent <- Elaboration.namespace
              scope <- getScopeBy(modifiers, defaultScope)
              signatureTerm <- TypeCheck.check(signature, Value.Type, "definition signature")
              signatureValue <- Conversion.evaluate(signatureTerm).flatMap(Conversion.reify(_, unfold = true)).flatMap(Conversion.evaluate)

              parameters <- getParameterNames(signatureValue)
              term <- TypeCheck.check(expression, signatureValue, "user provided signature")
              value <- Conversion.evaluate(term)
              definition = core.Declaration.Definition(name, signatureValue, parameters, Some(parent))
              _ = definition.tree = Some(CaseTree.Body(term))
              _ <- parent.declare(definition.self.link(name, scope))
              signatureString <- PrettyPrinter.print(signatureValue)
              termString <- PrettyPrinter.print(value)
              indent = 15 + definition.qualifier.toString.length + 1
              _ = println(s"[info] defined ${definition.qualifier} : $signatureString\n${("= " + termString).indentation(indent)}")
            } yield declaration.instance = Some(definition)
          }

        case surface.Declaration.Definition(modifiers, name, _, signature, Right(clauses)) =>
          Elaboration.when(declaration.instance.isEmpty) {
            val definitionElaboration: Elaboration[Declaration.Definition] = declaration.instance match {
              case Some(declaration) =>
                Elaboration.pure(declaration.asInstanceOf[Declaration.Definition])
              case None =>
                for {
                  parent <- Elaboration.namespace
                  scope <- getScopeBy(modifiers, defaultScope)
                  signatureValue <- TypeCheck.check(signature, Value.Type, "definition signature").flatMap(Conversion.evaluate)

                  parameters <- getParameterNames(signatureValue)

                  definition = core.Declaration.Definition(name, signatureValue, parameters, Some(parent))
                  _ <- parent.declare(definition.self.link(name, scope))
                  _ = declaration.instance = Some(definition)
                } yield definition
            }

            for {
              definition <- definitionElaboration
              term <- matching.Elaborator.elaborate(definition, clauses)
              _ = definition.tree = Some(term)
              signatureString <- PrettyPrinter.print(definition.signature)
              _ = println(s"[info] defined ${definition.qualifier} : $signatureString")
            } yield ()
          }

        case surface.Declaration.Family(modifiers, name, _, constants, signature, clauses) =>
          def go(parameters: List[(String, Argument[Expression])])(elaboration: => Elaboration[Term]): Elaboration[Term] = parameters match {
            case (parameter, argument) :: parameters =>
              for {
                signature <- TypeCheck.check(argument.argument, Value.Type, "type signature").flatMap(Conversion.evaluate)
                body <- Elaboration.bind(parameter, signature, argument.idiom, noo.compiler.elaboration.context.Visibility.Visible)(_ => go(parameters)(elaboration))
                signature <- Conversion.reify(signature, unfold = true)
              } yield Term.Pi(parameter, signature, body, argument.idiom)

            case Nil =>
              elaboration
          }

          val familyElaboration = if (declaration.instance.isEmpty) {
            for {
              parent <- Elaboration.namespace
              scope <- getScopeBy(modifiers, defaultScope)
              polarity = Modifier.of[Polarity](modifiers).getOrElse(Polarity.Positive)

              signatureValue <- go(constants) {
                TypeCheck.check(signature, Value.Type, "family signature")
              }.flatMap(Conversion.evaluate)
              (constants, indices) <- getParameterNames(signatureValue).map(_.splitAt(constants.size))

              family = core.Declaration.Family(name, constants, indices, signatureValue, polarity, Some(parent))
              signatureTerm <- Conversion.reify(signatureValue, unfold = true)

              _ <- atPositionOf(signature)(checkFamilySignature(family, signatureTerm, polarity))
              _ <- parent.declare(family.self.link(name, scope))
              _ = declaration.instance = Some(family)

              signatureString <- PrettyPrinter.print(signatureTerm)
              _ = println(s"[info] assumed $name : $signatureString")
            } yield family
          } else {
            Elaboration.pure(declaration.instance.get.asInstanceOf[core.Declaration.Family])
          }

          for {
            scope <- getScopeBy(modifiers, defaultScope)
            family <- familyElaboration
            _ <- Elaboration.withNamespace(family) {
              elaborate(clauses, scope)
            }
          } yield ()

        case surface.Declaration.Family.Clause(modifiers, name, _, signature) =>
          def go(parameters: List[Argument[String]], signature: Type)(elaboration: => Elaboration[Term]): Elaboration[Term] = parameters match {
            case Argument(parameter, _) :: parameters =>
              Conversion.force(signature, unfold = true).flatMap {
                case Value.Pi(_, head, body, idiom) =>
                  for {
                    body <- Elaboration.bind(parameter, head, idiom, noo.compiler.elaboration.context.Visibility.Visible) {
                      binder => body(binder.value).flatMap(go(parameters, _)(elaboration))
                    }
                    head <- Conversion.reify(head, unfold = true)
                  } yield Term.Pi(parameter, head, body, Idiom.Implicit) // Parameters can be decided by the instance so we always pass them implicit here

                case _ =>
                  impossible
              }

            case Nil =>
              elaboration
          }

          Elaboration.when(declaration.instance.isEmpty) {
            for {
              parent <- Elaboration.namespace
              scope <- getScopeBy(modifiers, defaultScope)

              family = parent.asInstanceOf[core.Declaration.Family]

              signatureValue <- go(family.parameters, family.signature) {
                TypeCheck.check(signature, Value.Type, "clause signature")
              }

              signatureValue <- Conversion.evaluate(signatureValue)
              signatureTerm <- Conversion.reify(signatureValue, unfold = true)
              // TODO: This reification is necessary, since there might be meta-variables that are already resolved and pruned in this way.
              //   The family checker fails if there are meta-variables, since it can't force them. In the future, it would be nice to work
              //   on values instead of terms for the family check.

              parameters <- getParameterNames(signatureValue)
              clause <- family.polarity match {
                case Positive =>
                  val clause = core.Declaration.Clause.Constructor(name, signatureValue, parameters, Some(family))
                  atPositionOf(signature)(checkConstructorSignature(family, name, signatureTerm)).map(_ => clause)
                case Negative =>
                  val clause = core.Declaration.Clause.Projection(name, signatureValue, parameters, Some(family))
                  atPositionOf(signature)(checkProjectionSignature(family, name, signatureTerm)).map(_ => clause)
              }

              _ <- parent.declare(clause.self.link(name, scope))
              signatureString <- PrettyPrinter.print(signatureTerm)
              _ = println(s"[info] assumed $name : $signatureString")
            } yield declaration.instance = Some(clause)
          }

        case surface.Declaration.Import(modifiers, pattern, _) =>
          for {
            parent <- Elaboration.namespace
            // We default to the parent as the scope (i.e., imports are private by default)
            scope <- getScopeBy(modifiers, parent)
            _ <- resolve(pattern, scope)
          } yield declaration.instance = None
      }
    }

  // TODO: Make public.
  private def atPositionOf[A](expression: Expression)(elaboration: => Elaboration[A]): Elaboration[A] = expression match {
    case Expression.Position(expression, range) =>
      Elaboration.modify(_ at range)(atPositionOf(expression)(elaboration))
    case _ =>
      elaboration
  }

  private def getParameterNames(signature: Type): Elaboration[List[Argument[String]]] = Conversion.force(signature, unfold = true).flatMap {
    case Value.Pi(parameter, head, body, idiom) =>
      Elaboration.bind(parameter, head, idiom, Visible) {
        binder => body(binder.value).flatMap(getParameterNames)
      }.map(Argument.Default(parameter, idiom) :: _)

    case _ =>
      Elaboration.pure(Nil)
  }

  private def getScopeBy(modifiers: List[Modifier], defaultScope: core.Declaration with Namespace): Elaboration[core.Declaration with Namespace] =
    Modifier.of[Visibility](modifiers).map {
      case Visibility.Public => Elaboration.pure(core.Declaration.Module.root)
      case Visibility.Private(scope) =>
        for {
          namespace <- Elaboration.namespace
          entry <- namespace.resolveOrFail(scope).flatMap(_.as[core.Declaration with Namespace]("namespace"))
          _ <- Elaboration.unless(namespace.isChildOf(entry.declaration)) {
            Elaboration.fail(IllegalScope(entry.declaration, namespace) _)
          }
        } yield entry.declaration
    }.getOrElse(Elaboration.pure(defaultScope))

  private def resolve(pattern: surface.Declaration.Import.Pattern, scope: core.Declaration with Namespace): Elaboration[Unit] = {
    def resolve(base: Qualifier, pattern: surface.Declaration.Import.Pattern): Elaboration[Set[Qualifier]] = pattern match {
      case Pattern.Wildcard =>
        // Whenever we hit a wildcard import we postpone its resolution until we have seen the whole pattern.
        // In this way, we achieve that only the "unspecified rest" is imported.
        Elaboration.pure(Set(base))

      case Pattern.Group(patterns) =>
        patterns.map(resolve(base, _)).sequence.map(_.flatten.toSet)

      case Pattern.Qualified(suffix, pattern) =>
        resolve(base ++ suffix, pattern)

      case Pattern.Synonym(suffix, synonym) =>
        for {
          namespace <- Elaboration.namespace
          entry <- namespace.resolveOrFail(base ++ suffix)
          _ <- namespace.declare(entry.link(synonym, scope))
        } yield Set.empty
    }

    resolve(Qualifier.Empty, pattern).flatMap(_.map { wildcard =>
      for {
        namespace <- Elaboration.namespace
        entry <- namespace.resolveOrFail(wildcard).flatMap(_.as[core.Declaration with Namespace]("namespace"))
        _ <- entry.declaration.map {
          // Wildcard patterns import any entry of the specified namespace if:
          // 1) The entry is actually visible from the current namespace (i.e., we could have imported it by its qualifier).
          //    This prevents visibility errors from occurring.
          // 2) The current namespace does not have a member that is named alike (i.e., wildcard imports do not shadow existing declarations).
          //    This prevents duplicate declaration errors from occurring.
          // 3) The associated declaration of the entry is not already present under a different name (i.e., we only import things that are not already there).
          //    This prevents odd behavior such as `import Math.(Nat as N, _)` where `Nat` would otherwise be available as `Nat` (due to the wildcard) and `N`.
          case entry if entry.isVisibleFrom(namespace) && !namespace.contains(entry.name) && !namespace.exists(_.declaration eq entry.declaration) =>
            namespace.declare(entry.link(entry.name, scope))
          case _ =>
            Elaboration.unit
        }.sequence
      } yield ()
    }.sequence.void)
  }

}
