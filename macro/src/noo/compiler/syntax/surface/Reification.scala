package noo.compiler.syntax.surface

import noo.compiler.input.{SourcePosition, SourceSpan}
import noo.compiler.modifier.{Idiom, Modifier, Polarity, Visibility}
import noo.compiler.syntax.common.Qualifier
import noo.compiler.syntax.surface.Declaration.Family

import scala.reflect.macros.blackbox

final class Reification(val context: blackbox.Context) {

  import context.universe._

  implicit def expressionLiftable: context.universe.Liftable[Expression] = {
    case _ => q"_root_.noo.compiler.syntax.surface.Expression.Hole"
  }

  implicit def modifierLiftable: context.universe.Liftable[Modifier] = {
    case Idiom.Explicit => q"_root_.noo.compiler.modifier.Idiom.Explicit"
    case Idiom.Implicit => q"_root_.noo.compiler.modifier.Idiom.Implicit"
    case Polarity.Negative => q"_root_.noo.compiler.modifier.Polarity.Negative"
    case Polarity.Positive => q"_root_.noo.compiler.modifier.Polarity.Positive"
    case Visibility.Public => q"_root_.noo.compiler.modifier.Visibility.Public"
    case Visibility.Private(scope) => q"_root_.noo.compiler.modifier.Visibility.Private($scope)"
  }

  implicit def qualifierLiftable: context.universe.Liftable[Qualifier] = {
    case Qualifier.Empty => q"_root_.noo.compiler.syntax.common.Qualifier.Empty"
    case Qualifier.Selection(base, member) => q"_root_.noo.compiler.syntax.common.Qualifier.Selection($base, $member)"
  }

  implicit def positionLiftable: context.universe.Liftable[SourcePosition] = {
    case SourcePosition.Undefined => q"_root_.noo.compiler.input.SourcePosition.Undefined"
    case SourcePosition.Location(line, character, _, offset) =>
      q"_root_.noo.compiler.input.SourcePosition.Location($line, $character, _root_.noo.compiler.input.InputSource.unknown, $offset)"
  }

  implicit def sourceSpanLiftable: context.universe.Liftable[SourceSpan] = {
    case SourceSpan.Undefined => q"_root_.noo.compiler.input.SourceSpan.Undefined"
    case SourceSpan.Between(p, b) => q"_root_.noo.compiler.input.SourceSpan.Between($p, $b)"
  }

  def reify(declaration: Declaration): Tree = declaration match {
    case Declaration.Module(modifiers, qualifier, span, declarations) => ???
    case Declaration.Axiom(modifiers, name, span, signature) => ???
    case Declaration.Definition(modifiers, name, span, signature, body) =>
      q"_root_.noo.compiler.syntax.surface.Declaration.Definition($modifiers, $name, $span, $signature, Right(Nil))"
    case Declaration.Family(modifiers, name, span, parameters, signature, clauses) =>
      q"_root_.noo.compiler.syntax.surface.Declaration.Family($modifiers, $name, $span, Nil, $signature, Nil)"
    case Family.Clause(modifiers, name, span, signature) => ???
    case Declaration.Import(modifiers, pattern, span) => ???
  }

}
