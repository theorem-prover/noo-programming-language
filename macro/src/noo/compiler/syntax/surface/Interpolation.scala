package noo.compiler.syntax.surface

import noo.compiler.elaboration.context.Context
import noo.compiler.input.{InputSource, SourceText}
import noo.compiler.syntax.surface.parser.Parser

import scala.reflect.macros.blackbox

object Interpolation {

  def declaration(context: blackbox.Context)(arguments: context.Expr[Any]*): context.Expr[Declaration] = {
    import context.universe._

    context.prefix.tree match {
      case Apply(_, List(Apply(_, rawParts))) =>
        // These are the parts of the string, split at the interpolated arguments
        val parts = rawParts.map {
          case Literal(Constant(const: String)) => const
        }

        // For the moment we do not allow any interpolation, so we just parse the string
        val input = parts.mkString
        val source = InputSource.from(SourceText.from(input), "string interpolation", None)
        val parser = new Parser(source)
        parser.parse(parser.declaration)(Context.empty) match {
          case Left(error) =>
            context.abort(context.enclosingPosition, error.kind)

          case Right(declaration) =>
            val tree: Tree = new Reification(context).reify(declaration).asInstanceOf[Tree]
            context.Expr(tree)
        }

      case _ =>
        context.abort(context.enclosingPosition, "illegal call to macro `declaration`")
    }
  }

}
