# The Noo Programming Language

Welcome to the official repository of the programming language Noo (pronunciation [nu​ː​]).
Noo is just like [Agda](https://wiki.portal.chalmers.se/agda/pmwiki.php) a ...
- ... **functional programming language** with dependent types.
- ... **proof assistant** for constructive mathematics.

For a quick example have a look at the [standard library](library). 
An overview of Noo's features is given in the next section [Overview](#overview). 
If you wonder about the motivation behind Noo, check out section [Motivation](#motivation). 
Contributions of any kind are more than welcome! 
Please see section [Contribution](#contribution) if you want to help out.

## Overview

### Surface Features

Here is a list of Noo's current and some of the planned features:
- [x] Inductive type families that feature ...
  - [x] parameters
  - [x] a positive style of definition
  - [x] dependent pattern matching
- [x] Co-inductive types that feature ...
  - [x] parameters
  - [x] a negative style of definition
  - [x] dependent co-pattern matching
- [x] First-class implicit function types
- [x] A module system that features ...
  - [x] fine-grained control over visibility
  - [x] submodules
  - [x] import patterns
  - [x] export using public imports
- [ ] Code generation
- [ ] Quantitative type theory
- [ ] Type classes
- [ ] Context-free syntax extensions (based on [Björn Lötters'](https://git.thm.de/bpfr67) latest research)

### Compiler Features

- [x] Normalization by evaluation
- [x] Control over unfolding
- [x] Higher-order pattern unification
  - [x] Pruning of meta-variables
  - [x] Intersection of the same meta-variable
  - [ ] Postponement of unification problems
- [x] Tactical (co-) pattern matching
- [x] Strict positivity check for inductive families
- [x] Name resolution and accessibility control
- [ ] LLVM backend
- [ ] ...

## Motivation

If you are already familiar with proof assistants and dependently typed programming languages you may ask at this point:
Why yet another dependently typed language?

### Noo as a Research Project
First and foremost, Noo is a research project with the aim to analyze and evaluate new language features and the architectural choices of compilers for dependently typed programming languages.
Even though there are already several of such languages, there are only few widely acknowledged standard solutions to these architectural choices.
This is in stark contrast to other programming paradigms, where we can rely on a long history of compiler engineering and resort to a huge number of well-proven solutions and tools.

### Noo as an Educational Project

Another essential reason why we develop Noo is a consequence of the first: 
We love dependent types and believe, that they have the potential to change the way how we think about software. 
They can help us to formalize knowledge in science in a verifiable way and allow us to think about programs in a more structured and robust way. 
We aim to provide an exemplary compiler and language architecture with Noo that can be used for educational purposes in the future.

### Noo as a New Programming Paradigm

Dependent types open a whole new world of possibilities: 
They are not only well-suited for theorem proving, but can be used for general-purpose programming as well (as emphasized by Agda and [Idris](https://www.idris-lang.org/))! 
For our day-to-day programming this means that we can use dependent types to express solutions in a more generic and more robust way than ever before in typed programming languages. 
[Recent research](https://www.idris-lang.org/drafts/sms.pdf) also shows that we can use dependent types to model state machines (e.g., internet protocols) in a type-safe way. 
With Noo we wish to discover new programming paradigms that can take advantage of this expressive power.

## Contribution

If you would like to contribute to Noo, please do not hesitate to get in touch either via the issue system or via mail. 
Of course, you can also fork this project and create a merge request to contribute bug fixes, documentation and other changes.
Contributions of any kind (including error reports) are more than welcome!

List of contributors:
- [Björn Lötters](https://git.thm.de/bpfr67)
- [Jan Hindges](https://git.thm.de/jfhn47)

